@istest
public class Test_PaymentConfirmed {
    public static testmethod void test_Payment(){
        try{
opportunity opp=new opportunity(name='opp',stagename='Closed Won',CloseDate=system.today()+2);
        insert opp;
    payment_details__c pd=new payment_details__c(name='test',Confirmation_status__c='confirmed',Opportunity__c=opp.Id);
        insert pd;
        
        opportunity opp1=new opportunity(name='opp',stagename='Closed Lost',CloseDate=system.today()+2);
        insert opp1;
    payment_details__c pd1=new payment_details__c(name='test',Confirmation_status__c='On-Hold',Opportunity__c=opp1.Id);
        insert pd1;
        opp1.stagename='Closed Won';
        update opp1;
    }
        catch(exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Please let the Payment Confirmed before Closed Won') ? true : false;
System.AssertEquals(expectedExceptionThrown, true);
        }
    }
}