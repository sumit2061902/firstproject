@isTest
public class Test_MatchingCampaign {
    public static Testmethod void  checklead(){
        campaign c=new campaign();
        c.Name='Marvel';
        insert c;
        
        lead l1 =new lead();
 		l1.LeadSource='Dup-Marvel';
        l1.LastName='Test';
        l1.Company='cts';
        insert l1;
        
              lead l12 =new lead();
 		l12.LeadSource='Web';
        l12.LastName='Test';
        l12.Company='cts';
        insert l12;
        string holder=l1.id;
        list<campaignmember>cm=[select id from campaignmember where leadid=:holder limit 1];
        system.assertEquals(1,cm.size());
    }
    
    
}