@restresource(urlmapping='/Accounts/*/contacts')
global class AccountManager {
    
    @HttpGet
    global static Account getAccount(){
        restrequest req=restcontext.request;
          String accId = req.requestURI.substringBetween('Accounts/', '/contacts');
         Account acc = [SELECT Id, Name, (SELECT Id, Name FROM Contacts) 
                       FROM Account WHERE Id = :accId];
        return acc;
    }
}