public with sharing class AsBuiltCaseSelector
{
    public static List<task> getAlltasks(Id basecaseid)
    {
        list<task> alltasks=new list<task>();
        alltasks=[select subject,status from task where whatid=:basecaseid ORDER BY status  LIMIT 100 OFFSET 3 ];
    return alltasks;
    }
}