public class ProjectsController { 
    
    public ProjectItem__c newProject { get; set; }
    
    public ProjectItem__c editProject { get; set; }

    public ProjectsController() {
        newProject = new ProjectItem__c();
    }
    
    public ProjectItem__c[] getProjects() {
        return [SELECT Name, Status__c, Weighting__c FROM ProjectItem__c WHERE OwnerId =:UserInfo.getUserId() 
                ORDER BY CreatedDate DESC];
    }
    
    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);   
    }
    
    public PageReference add() {
        try {
            INSERT newProject;
            // if successful, reset the new project for the next entry
            newProject = new ProjectItem__c();
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }
     
    public PageReference del() {
        try {
            String delid = getParam('delid');
            ProjectItem__c project = [SELECT Id FROM ProjectItem__c WHERE ID=:delid];
            DELETE project;
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }
    
    public PageReference edit() {
        String editid = getParam('editid');
        editProject = [SELECT Name, Status__c, Weighting__c FROM ProjectItem__c WHERE Id=:editid];
        return null;
    }
    
    public PageReference cancelEdit() {
        editProject = null;
        return null;
    }
    
    public PageReference saveEdit() {
        try {
            UPDATE editProject;
            editProject = null;
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        return null;
    }
        
}