public class AccControllerClass {
    public static void getParent(list<Merge_Request__c>records,Map<string,contact>contactmap){
        list<String>sourceIds=new list<String>();
        list<accountcontactrelation> acrupdate=new list<accountcontactrelation>();
        list<accountcontactrelation> acrSrcList= new list<accountcontactrelation>();
        Map<id,accountcontactrelation> acrTrgtList= new Map<id,accountcontactrelation>();
        
        for(Merge_Request__c mr:records) {
            sourceids.add(mr.Source_Number__c);
            system.debug('souSrceids>>>>'+sourceids+'::'+contactmap.get(mr.Source_Number__c));
            for(accountcontactrelation acr:contactmap.get(mr.Source_Number__c).accountcontactrelations) {
                acrSrcList.add(acr);
            }
            for(accountcontactrelation acr:contactmap.get(mr.Target_Number__c).accountcontactrelations) {
                acrTrgtList.put(acr.accountid,acr);
            }
            system.debug('acrSrcLiasdasdst>>>'+acrSrcList+':::::'+acrTrgtList);
            for(accountcontactrelation acr:acrSrcList) {
                if(acrTrgtList.containskey(acr.accountid)) {
                    acr.enddate=system.today();
                    acr.roles__c='Third Party';
                    acrupdate.add(acr);
                    System.debug('acrupdate>>>'+acrupdate);
                    
                }
                else {
                    accountcontactrelation acrnew= new accountcontactrelation();
                    acrnew.enddate=system.today()+10;
                    acrnew.accountid=acr.accountid;
                    acrnew.contactid=contactmap.get(mr.Target_number__c).id;
                    acrupdate.add(acrnew);
                    upsert acrupdate;
                    
                }
            }
        }
    } 
    
    public static void reparentUserRecord(List<merge_request__C>records,Map<string,contact>contactMap) {
        Map<id,id>contactSrcTrgtMap=new Map<id,id>();
        for(merge_request__c mr:records) {
            contactSrcTrgtMap.put(contactMap.get(mr.source_number__c).id,contactmap.get(mr.target_number__c).id);
        }
        
        List<task> taskobj=new list<task>([select id from task where whoid in :contactSrcTrgtMap.keyset()]);
        list<task> tsklist=new list<task>();
        for( task tsk:taskobj) {
            task t=new task();
            t.id=tsk.id;
            tsk.whoid=(contactSrcTrgtMap.get(tsk.whoid)!=null) ? contactSrcTrgtMap.get(tsk.whoId):tsk.whoid;
            tsklist.add(t);
        }
        if(tsklist.size()>0) {
            database.saveresult[] srlist=database.update(tsklist,false);
        }
    }   
}