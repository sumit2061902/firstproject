public class MixedDMLFuture {
    public static void useFutureMethod() {
        // First DML operation
        Account a = new Account(Name='Acme',active__c='Yes',SLA__c='Gold',SLASerialNumber__c='2212',AnnualRevenue=1200);
        insert a;
        System.debug('a>>>>>>>>>>'+a);
        // This next operation (insert a user with a role) 
        // can't be mixed with the previous insert unless 
        // it is within a future method. 
        // Call future method to insert a user with a role.
        Util.insertUserWithRole(
            'mruiz@awcomputinsaaom.com', 'mruiz', 
            'mruiz@awcomputingsaam.com', 'Ruiz');        
    }
}