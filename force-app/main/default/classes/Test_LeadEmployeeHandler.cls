@istest
public class Test_LeadEmployeeHandler {
    public static testmethod void  CheckCompany(){
        Company_Information__c CI=new Company_Information__c();
        ci.Name='Tata';
        ci.Inidvial_Corporate__c=true;
        ci.Annual_TurnOver__c=123000;
        ci.Employee_count__c=3000;
        Insert Ci;
        Lead l=new lead();
        l.Lastname='LeadTest';
        l.company='cts';
        l.status='Closed-converted';
        l.Company_name__c='Tata';
        insert l;
        
        
    }
    public static testmethod void  CheckCompany1(){
        Company_Information__c CI1=new Company_Information__c();
        ci1.Name='cst';
        ci1.Inidvial_Corporate__c=true;
        ci1.Employee_count__c=3000;
        Insert Ci1;
          ci1.Employee_count__c=null;
        update ci1;
        Lead l1=new lead();
        l1.Lastname='LeadTest';
        l1.company='cst';
        l1.status='Closed-converted';
        l1.Company_name__c='cst';
        insert l1;
     
        Lead l2=new lead();
        l2.Lastname='LeadTest';
        l2.company='cst';
        l2.status='Closed-converted';
        l2.Company_name__c='cssst';
        try{
        insert l2;
        }
        catch(exception e){
          boolean exceptionthrwon=e.getMessage().contains('This Company is Not associated previously,Please add the Company Information')?true:false;
            system.assertEquals(exceptionthrwon, true);
            
        }
    }
}