public class SiteTriggerHandler {
    public static void onAfterInsert(crm_site__c[] newsite){
        SiteTriggerHelper.createContactrRole(newsite);
    }
}