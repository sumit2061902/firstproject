public class UpdateAccountPhoneInfo {
public static void UpdateAcconContact(list<account>accList){
    Map<id,account>accmap=new map<id,account>();
    for(account acc:accList){
        if(acc.phone!=null || acc.phone!=''){
            accmap.put(acc.id,acc);
        }
    }
    System.debug('accmap>>>'+accmap);
    if(accmap.size()>0){
        Map<id,contact>conmap=new map<id,contact>([select phone,AccountId from contact where accountid in :accmap.keySet()]);
        list<contact>conlist=new list<contact>();
        System.debug('conmap>>>'+conmap);
        if(conmap.size()>0){
            for(contact con:conmap.values()){
                if(accmap.containsKey(con.AccountId)){
                    con.phone=accmap.get(con.AccountId).phone;
                    conlist.add(con);
                }
            }
            update conlist;
        }
    }
}

public static void PhoneDetails(list<account>accList){
    Map<string,list<account>> phoneNum=new Map<string,list<account>>();
    
    for(account acc:acclist){
        if(phoneNum.containsKey(acc.Phone)){
            phoneNum.get(acc.phone).add(acc);
        }
        else
        {
            phonenum.put(acc.Phone,new list<account>{acc});
        }
       
    }
    System.debug('phonenum::::::::::'+phonenum); 
    list<account>acname=new list<account>([select id from account where phone in :phonenum.keySet()]);
    map<id,phone_Details__c> Phdetails=new Map<id,phone_Details__c>([select id,Number_Of_Account_with_same_number__c,Name,Account_Name__c from phone_Details__c where name in:phoneNum.keyset()]);       
    list<phone_Details__c>phList=new list<phone_Details__c>();
    for(phone_Details__c ph:phdetails.values()){
        if(phoneNum.containskey(ph.name)){       
            list<account> acc=phonenum.get(ph.name);
            ph.Account_Name__c  =acc[0].Name;
            ph.Number_Of_Account_with_same_number__c=ph.Number_Of_Account_with_same_number__c+phoneNum.get(ph.name).size();
            phList.add(ph);
        }
    }
    System.debug('phList::::::::::'+phList);
    update phList;      
}

Public static void AccountHistoryInsert(list<account>acclist){
   List<AccountHistory__c>acchistory=new list<AccountHistory__c>();
    for(account acc:acclist){
        AccountHistory__c ah=new AccountHistory__c();
        ah.Name=acc.name;
        ah.accountid__c=acc.id;
        ah.Active__c=acc.Active__c;
        ah.AnnualRevenue__c=acc.AnnualRevenue;
        ah.SLA__c=acc.SLA__c;
        ah.SLASerialNumber__c=acc.SLASerialNumber__c;
        acchistory.add(ah);
    }
    System.debug('acchistory:::::'+acchistory);
    Insert acchistory;
}
 Public static void AccountHistoryUpdate(list<account>acclist,boolean isupdate,boolean isdelete,list<account>acclistold){
     Map<id,account> accid=new Map<id,account>();
      Map<id,account> accidsde=new Map<id,account>();
     List<AccountHistory__c>acchistory=new list<AccountHistory__c>();
     if(isupdate==true){
     for(account acc:acclist){
         accid.put(acc.id,acc);
         
     } 
      
     list<AccountHistory__c>acchisList=new list<AccountHistory__c>([select id,AccountID__c from AccountHistory__c where accountId__c in :accid.keyset()]);
 
     for(AccountHistory__c ah:acchisList){
         if(accid.containskey(ah.accountid__c)){
             ah.Name=accid.get(ah.accountid__c).name;
           
            ah.Active__c=accid.get(ah.accountid__c).Active__c;
            ah.AnnualRevenue__c=accid.get(ah.accountid__c).AnnualRevenue;
            ah.SLA__c=accid.get(ah.accountid__c).SLA__c;
            ah.SLASerialNumber__c=accid.get(ah.accountid__c).SLASerialNumber__c;
            acchistory.add(ah);
         }
     }
     update acchistory;
 
 }
 
      if(isdelete==true){
     for(account acc:acclistold){
         accidsde.put(acc.id,acc);
     } 
     
list<AccountHistory__c>acchisList1=new list<AccountHistory__c>([select id,AccountID__c from AccountHistory__c where accountId__c in :accidsde.keyset()]);
List<AccountHistory__c > acdelete=new list<AccountHistory__c >();
     if(acchisList1.size()>0){
     for(AccountHistory__c ac:acchisList1){
     if(accidsde.containskey(ac.accountid__c)){
     acdelete.add(ac);
     }
     }
     }
     delete acdelete;
     
     
 }
 }
}