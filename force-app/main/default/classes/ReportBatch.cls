global class ReportBatch implements Database.Batchable<Report>, Database.Stateful {
    
    global List<Report> reps = new List<Report>();
    global String repstring;
    global String str;
    global String filt;
    global String val;
    global ReportBatch(String t, String f, String a)
    {
       str = a;
       filt = f;
       val = t;    
    }
    
    global Iterable<Report> start(Database.BatchableContext bc) {
        return [select Id,Name,DeveloperName from Report];
    }
    
    global void execute(Database.BatchableContext BC, List<Report> scope) {
        for (Report report : scope) {
            try {
                Reports.ReportDescribeResult reportDescription = Reports.ReportManager.describeReport(report.Id);
                Reports.ReportMetadata reportMetadata = reportDescription.getReportMetadata();
                for (Reports.ReportFilter filter : reportMetadata.getReportFilters()) { 
                    if (filter.getColumn().containsignorecase(filt) && filter.getColumn() != val) {
                        reps.add(report);
                        repstring += (report.Id+' '+report.Name+' \n');
                    }
                }
            } catch (Exception e) {
                system.debug(e);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        system.debug(reps +'    '+reps.size());
        repstring = 'Here is the list of report' + repstring;
        sendMessage('reports',str,repstring);
    }
    
    public static void sendMessage(String subject, String addressess, Exception e) {
        String message = 'System tried to close when transactions were entered <br> <br> Error Message: '  + e.getMessage() + ' <br> Type: '  + e.getTypeName() + ' <br> Stack Trace: ' + e.getStackTraceString();
        sendMessage(subject, addressess, message);
    }
    
    public static void sendMessage(String subject, String addressess, String message) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{addressess});
        mail.setSubject(subject);
        mail.setHtmlBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
    }
    
}