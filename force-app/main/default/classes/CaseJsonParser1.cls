/*--------------------------------------------------
Class Name: CaseJsonParser1
CreateDate: 03/05/2018
Description:created to manage the inbound calls for creating cases.
Version: 1.0
---------------------------------------------------*/

public class CaseJsonParser1 {
    
    public  cls_case cse;
    
    public class cls_case {
        public string status;
        public string Origin;
        public string recTypeName;
        public string customerType;
        public string reasonForCompletingAIM;
        public string serviceId;
        public string enquiryType;
        public boolean nbnService;
        public boolean paCustomer;
        public string emailAddress;
        public boolean existingTelstraCustomer;
        public boolean callbackOnWeekned;
        public string companyType;
        public string changeModifyType;
        public string contactNumber;
        
    }
    
    Public static CaseJsonParser1 parse(string json){
        return (CaseJsonParser1) System.JSON.deserialize(json, CaseJsonParser1.class); 
    }
    
}