public class ACN_OrderLineItemTriggerHandler {
    public static void onAfterInsert(map<id,orderitem> orderLineMap){
        System.debug('called method onAfterInsert :::::::::::::::');
        concatenateDan(orderLineMap);
        
    }
    public static void concatenateDan(Map<ID,orderItem> orderLineMap){
        Map<id,order>orderMap= new Map<id,order>();
        Map<id,set<string>> danMap=new Map<id,set<string>>();
        set<id>OrderIDS=new set<id>();
        for(orderItem ot:orderLineMap.values()){
            IF(ot.OrderId!=null || ot.OrderId!=''){
                order od=new order();
                od.id=ot.orderid;
                od.DAN__C='';
                orderMap.put(od.id,od);
                OrderIDS.add(od.Id);
            }
        }
         System.debug('orderMap :::::::::::::::'+orderMap);
        System.debug('OrderIDS :::::::::::::::'+OrderIDS);
      List<OrderItem>  OrderItemList=[select orderid,dan__c from orderitem where orderid in:orderIDS];
        System.debug('OrderItemList :::::::::::::::'+OrderItemList);
        for(orderitem oi:orderItemList){
            if(!string.isEmpty(oi.DAN__C)){
                if(danMap.containsKey(oi.OrderId)){
                       System.debug('danMap :::::::::::::::'+danMap);
                    set<string> tset=danMap.get(oi.OrderId);
                    if(!tset.contains(oi.DAN__c)){
                        
                        orderMap.get(oi.OrderId).dan__C+=(';'+oi.DAN__c);
                        tset.add(oi.DAN__c);
                        danmap.put(oi.OrderId, tset);
                         System.debug('tset :::::::::::::::'+tset);
                          System.debug('danmap :::::::::::::::'+danmap);
                    }
                }
                else
                {
                    danmap.put(oi.OrderId, new set<string>{oi.DAN__c});
                    if(string.isBlank(ordermap.get(oi.OrderId).dan__C)){
                        	orderMap.get(oi.OrderId).dan__C=oi.DAN__c;
                        system.debug('orderMap>>>>'+orderMap);
                    }
                    else
                    {
                         system.debug('order ELSE>>>');
                        ordermap.get(oi.OrderId).dan__C+=(';'+oi.DAN__c);
                    }
                }
            }
        }
      update ordermap.values();  
    }
}