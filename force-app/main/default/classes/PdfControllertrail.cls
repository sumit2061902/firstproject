public  class PdfControllertrail
{
public string UserLocale {get;set;}
public string baseId{get;set;}
public string userEmpNumber{get;set;}
public Sym_Employee__c  record {get; set;}
public string passURL{get;set;}
string doc;

    PUBLIC PdfControllertrail(ApexPages.StandardController controller)
    {
String sfdcBaseURL= URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file='; 
   
       system.debug('sfdcBaseURL---'+sfdcBaseURL);
      record = (Sym_Employee__c )controller.getRecord();
     system.debug('record-->'+record);
     
     
              system.debug('baseId------'+baseId);
   userEmpNumber=[select Employee_ID__c from Sym_Employee__c where id=:record.id ]. Employee_ID__c ;

    system.debug('userEmpNumber------'+userEmpNumber);
    
   UserLocale=[select   LocaleSidKey from user where EmployeeNumber=:userEmpNumber ].LocaleSidKey ;
       doc = [select id from document where Name =:UserLocale].id;
   system.debug('UserLocale------'+UserLocale);
   if(userLocale=='en_US'||userLocale=='en_IN')
   {
      passURL=sfdcBaseURL+doc;
                
       system.debug('passURL---'+passURL);
   }
   
   if(userLocale=='Ja_JP')
   {
   passURL=sfdcBaseURL+doc;
   
   }
   
    }

}