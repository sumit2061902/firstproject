// Apex Controller for Contact List Lightning Component
public class ContactListController {
    
    @auraEnabled
    public static list<contact> getContactList() {
        list<contact> contactList=new List<contact>([select firstname,lastname,email,phone from contact limit 18]);
        return contactList;
    }
}