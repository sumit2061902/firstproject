public class DynamicAccountFieldsLister {
public DynamicAccountFieldsLister(ApexPages.StandardController controller) {
controller.addFields(myfields);
}
public List<String> myfields {
get {
if (myfields == null) {
myfields = new List<String>();
myfields.add('Industry');
myfields.add('AnnualRevenue');
myfields.add('BillingCity');

}
return myfields ;
}
private set;
}
}