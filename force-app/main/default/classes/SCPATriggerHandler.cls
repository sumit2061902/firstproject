public class SCPATriggerHandler
    {
        public static void  UpdateStatus(List<Sales_Compensation_Plan__C> triggernew)
        {    
            system.debug('calling trigger ti update Status');
            system.debug('triggernew----'+triggernew);
            list<Sales_Compensation_Plan__C> sales=new list<Sales_Compensation_Plan__C>([select id,status__C from Sales_Compensation_Plan__C where id in :triggernew]);
            
                 for(Sales_Compensation_Plan__C sc:sales) 
                 
                     {
                         system.debug('Inside For');
                          sc.status__c='Accept';
                            
                     }  
              }
        public static void scparecordshare(List<Sales_Compensation_Plan__C> triggernew)
        {
              System.debug('calling Trigger with '+triggernew); 
        
                    List<Sales_Compensation_Plan__Share> scpaShares = new List<Sales_Compensation_Plan__Share>();
                    
                    List<Sales_Compensation_Plan__C> SalesPlanList = new List<Sales_Compensation_Plan__C>();
                    
                    SalesPlanList = [Select Id,Name, Employee_Name__c,OwnerID from Sales_Compensation_Plan__C where id IN: triggernew];
                    
                    Set<String> empIdSet = new Set<String>();
                    List<ID> shareIdsToDelete = new List<ID>();
                    
                    for(Sales_Compensation_Plan__C salesPlan : SalesPlanList ){
                    
                        empIdSet.add(salesPlan.OwnerID);
                    }
                    
                    System.debug('Employee Name--'+empIdSet); 
                     List<User>symEmployee = new List<User>();        
                     symEmployee = [SELECT Name, ManagerID, Manager.Name from User where ID IN :empIdSet];
        
                    /** if userid and managerid is not null */
                    for(User symemp : symEmployee  ){
                        
                        /** For each of the SCPA records being inserted, do the following: **/
                           for(Sales_Compensation_Plan__c eachrecord : triggernew){
                            
                                if(symemp.name !=null && symemp.Manager !=null && eachrecord.status__c!='Draft'){
                                                        
                                Sales_Compensation_Plan__Share scparecord = new Sales_Compensation_Plan__Share();
                            
                            /** Populate the Sales_Compensation_Plan__Share record with the ID of the record to be shared. **/
                                scparecord.ParentId = eachrecord.Id;
                            
                            /** Then, set the ID of user or group being granted access. In this case,
                              * we're setting the managerid from ManagerID__c field. ManagerID__c is a 
                              * formula field capturing immediate Manager userid.
                            **/
                                System.debug('Sharing with Manager'+symemp.Manager.Name); 
                                scparecord.UserOrGroupId = symemp.ManagerID ;
                                scparecord.AccessLevel = 'Read';
                                scparecord.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                                
                            /** Add the new Share record to the list of new Share records. **/
                                scpaShares.add(scparecord);
                                }    
                               if(eachrecord.Geography__c=='AMS'){
                                     /*Sharing with AMS Admin */ 
                                    Sales_Compensation_Plan__Share scparecord = new Sales_Compensation_Plan__Share();
                                    scparecord.ParentId = eachrecord.Id;
                                    scparecord.UserOrGroupId ='00G28000000xZ6x';
                                    System.debug('Sharing with group--'+scparecord.UserOrGroupId+'--');
                                    scparecord.AccessLevel = 'Edit';
                                    scparecord.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                                    scpaShares.add(scparecord);
                                                                      
                                }
                               if(eachrecord.Geography__c=='APJ'){
                                     /*Sharing with AMS Admin */ 
                                    Sales_Compensation_Plan__Share scparecord = new Sales_Compensation_Plan__Share();
                                    scparecord.ParentId = eachrecord.Id;
                                    //scparecord.UserOrGroupId = symemp.ManagerID ;
                                   scparecord.UserOrGroupId ='00G28000000xZDv';
                                    System.debug('Sharing with group--'+scparecord.UserOrGroupId+'--');
                                    scparecord.AccessLevel = 'Edit';
                                    scparecord.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                                    scpaShares.add(scparecord);
                                                                      
                                }                               
                                
 
                        }
                    }   
                    /** Insert all of the newly created Share records and capture save result **/
                     Database.SaveResult[] scpaShareInsertResult = Database.insert(scpaShares,false);
         
        }
        
        public static void scparecordshareUpdate(List<Sales_Compensation_Plan__C> triggerupdate, Map<ID,Sales_Compensation_Plan__C> oldMap)
        {
                List<Sales_Compensation_Plan__c> updatelist = new List<Sales_Compensation_Plan__c>();
                for(Sales_Compensation_Plan__c scpa : triggerupdate){
                    
                        Sales_Compensation_Plan__c scpaObj = oldMap.get(scpa.id);
                        System.debug('scpa.geography__c--'+scpa.geography__c+' scpaObj.geography__c--'+scpaObj.geography__c);
                        System.debug('scpa.Manager--'+scpa.Employee_Name__r.managerID +' scpaObj.Manager--'+scpaObj.Employee_Name__r.managerID);
                        if((scpa.geography__c != scpaObj.geography__c) ||(scpa.Employee_Name__r.managerID != scpaObj.Employee_Name__r.managerID))
                                {
                                    updatelist.add(scpa);
                                }
                }
        
                    List<Sales_Compensation_Plan__Share> scpaShares = new List<Sales_Compensation_Plan__Share>();
                    
                    List<Sales_Compensation_Plan__C> SalesPlanList = new List<Sales_Compensation_Plan__C>();
                    
                    SalesPlanList = [Select Id,Name, Employee_Name__c,OwnerID from Sales_Compensation_Plan__C where id IN: updatelist];
                    
                    Set<String> empIdSet = new Set<String>();
                    List<ID> shareIdsToDelete = new List<ID>();
                    
                    for(Sales_Compensation_Plan__C salesPlan : SalesPlanList ){
                    
                        empIdSet.add(salesPlan.OwnerID);
                    }
                    
                    System.debug('Employee Name--'+empIdSet); 
                     List<User>symEmployee = new List<User>();        
                     symEmployee = [SELECT Name, ManagerID, Manager.Name from User where ID IN :empIdSet];
        
                    /** if userid and managerid is not null */
                    for(User symemp : symEmployee  ){
                        
                        /** For each of the SCPA records being inserted, do the following: **/
                           for(Sales_Compensation_Plan__c eachrecord : updatelist){
                            
                                if(symemp.name !=null && symemp.Manager !=null && eachrecord.status__c!='Draft'){
                                                        
                                Sales_Compensation_Plan__Share scparecord1 = new Sales_Compensation_Plan__Share();
                            
                            /** Populate the Sales_Compensation_Plan__Share record with the ID of the record to be shared. **/
                                scparecord1.ParentId = eachrecord.Id;
                            
                            /** Then, set the ID of user or group being granted access. In this case,
                              * we're setting the managerid from ManagerID__c field. ManagerID__c is a 
                              * formula field capturing immediate Manager userid.
                            **/
                                System.debug('Sharing with Manager'+symemp.Manager.Name); 
                                scparecord1.UserOrGroupId = symemp.ManagerID ;
                                scparecord1.AccessLevel = 'Read';
                                scparecord1.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                             /** Add the new Share record to the list of new Share records. **/
                                scpaShares.add(scparecord1);

                            }
                            // if the record was for different GEO and Geo gets changed -- delete the existing share
                                for(Sales_Compensation_Plan__c eachrecord1 : updatelist) {
                                    shareIdsToDelete.add(eachrecord1.Id);   
                                    System.debug('shareIdsToDelete--'+shareIdsToDelete);            
                                }
                                   
                               if(eachrecord.Geography__c=='AMS'){
                                     /*Sharing with AMS Admin */ 
                                    Sales_Compensation_Plan__Share scparecord1 = new Sales_Compensation_Plan__Share();
                                    scparecord1.ParentId = eachrecord.Id;
                                    scparecord1.UserOrGroupId ='00G28000000xZ6x';
                                    System.debug('Sharing with group AMS--'+scparecord1.UserOrGroupId+'--');
                                    scparecord1.AccessLevel = 'Edit';
                                    scparecord1.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                                    scpaShares.add(scparecord1);
                                                                      
                                }
                               if(eachrecord.Geography__c=='APJ'){
                                     /*Sharing with AMS Admin */ 
                                    Sales_Compensation_Plan__Share scparecord1 = new Sales_Compensation_Plan__Share();
                                    scparecord1.ParentId = eachrecord.Id;
                                   // scparecord.UserOrGroupId = symemp.ManagerID ;
                                    scparecord1.UserOrGroupId ='00G28000000xZDv';
                                    System.debug('Sharing with group APJ--'+scparecord1.UserOrGroupId+'--');
                                    scparecord1.AccessLevel = 'Edit';
                                    scparecord1.RowCause = Schema.Sales_Compensation_Plan__Share.RowCause.SCPA_Record_Sharing__c;
                                    scpaShares.add(scparecord1);
                                                                      
                                }                                    
        
                        }
                    }   
                    // if GEO/Manager gets changed for scpa records -- delete the existing share
                    if (!updatelist.isEmpty())
                    {
                        System.debug('Deleting Records with parent Id--'+shareIdsToDelete);
                        delete [select id from Sales_Compensation_Plan__Share where parentID IN :shareIdsToDelete and RowCause = 'SCPA_Record_Sharing__c' ];
                    }   
                    /** Insert all of the newly created Share records and capture save result **/
                     Database.SaveResult[] scpaShareInsertResult = Database.insert(scpaShares,false);
         
        }
        
    }