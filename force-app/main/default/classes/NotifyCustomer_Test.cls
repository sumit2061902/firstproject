@isTEst		
public class NotifyCustomer_Test {

    public static testmethod void Notifypass(){
                    String json1  = NotifyCustJson.customerWithAddressPositive();// "cscrmExternalID":"Demo001010"

         RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            //request.requestUri = 'https://cs6.salesforce.com/services/apexrest/NotifyCustomer/'; //this needs to change so not to hardcode URI
            request.httpMethod = 'POST';
            request.addHeader('Content-Type', 'application/json');
            request.requestBody = Blob.valueOf(json1);
            RestContext.request = request;
            RestContext.response = response;

      NotifyCust.upsertCustomerRecords();
    }
      public  static testMethod void AccountWithValidationFail() {

            String json2  = NotifyCustJson.customerWithAddressPositive();// "cscrmExternalID":"Demo001010"
            //system.Debug('json ' + json2);
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestUri = 'https://cs6.salesforce.com/services/apexrest/NotifyCustomer/'; //this needs to change so not to hardcode URI
            request.httpMethod = 'POST';
            request.addHeader('Content-Type', 'application/json');
            request.requestBody = Blob.valueOf(json2);
            RestContext.request = request;
            RestContext.response = response;
            
            NotifyCust.upsertCustomerRecords();
           
         
           
    }
}