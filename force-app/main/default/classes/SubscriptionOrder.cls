public class SubscriptionOrder {
    
    Public static void subOrderGen(list<csord_Subscription__c>SubOrd){
        Set<Id> OrdId=new Set<Id>();
        Set<Id> OrdSubId=new Set<Id>();
        map<Id, list<csord_Subscription__c>> ordToSub = new map<Id, list<csord_Subscription__c>>();
        for(csord_Subscription__c csub:subord){
            OrdId.add(csub.csord_Order_Request__c);
            OrdSubId.add(csub.CS_Order__c);
        }
        System.debug('OrdId:::::'+OrdId);
        System.debug('OrdSubId:::::'+OrdSubId);
        List<CS_Order__c> csOrdList=new list<CS_Order__c>([select id,Name from CS_Order__c where 
                                                           csord_Order_Request__c in:OrdId]);
        List<csord_Subscription__c> CsSubList=new List<csord_Subscription__c>([select id,name,CS_Order__c from csord_Subscription__c where CS_Order__c in:ordSubid]);
        System.debug('csOrdList:::::'+csOrdList);
        System.debug('CsSubList:::::'+CsSubList);
        for(id ordids:ordid){
            list<csord_Subscription__c> relSubList = new list<csord_Subscription__c>();
            for(csord_Subscription__c csub:CsSubList){
                if(csub.CS_Order__c==ordids){
                    relSubList.add(csub);
                }
            }
            ordToSub.put(ordIds, relSubList);
            
        }
        System.debug('ordToSub:::::'+ordToSub);
    }
}