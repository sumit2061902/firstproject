public class SiteTriggerHelper {
    public static void createContactrRole(crm_site__C[] newsite){
        List<site_contact_role__c> sitecon=new list<site_contact_role__c>();
        for(crm_site__C cs:newsite){
            if(cs.Primary_Contact__c!=null){
                sitecon.add(createSCR(cs.Primary_Contact__c,cs.id,cs.name));
            }
            if(cs.Secondary_Contact__c!=null){
                sitecon.add(createSCR(cs.Primary_Contact__c,cs.id,cs.name));
            }
            
        }
        if(sitecon.size()>0){
            insert sitecon;
        }
    }
    public static Site_Contact_Role__c createSCR(ID contactId,ID stId,string name){
        Site_Contact_Role__c scr = new Site_Contact_Role__c();
        scr.name=name;
        return scr;
    }

}