public with sharing class ctrlCompetitor { 



    public static Integer flag=0; 

 public Competitor__c cc{get;set;} 
    public Competitor__c cc1=new Competitor__c(); 
    public List<Competitor__c> listComptr=new List<Competitor__c>();  
    public ctrlCompetitor(){}

    //extension invoked to pre-populate the Name parameter
    public ctrlCompetitor(ApexPages.StandardController stdController) {  
     try{
       cc=new Competitor__c();      
       cc = (Competitor__c)stdController.getRecord(); 
       if(ApexPages.currentPage().getParameters().get('Name')!=null)
         cc.Name=ApexPages.currentPage().getParameters().get('Name'); 
     }
     catch(Exception e){System.debug(e.getMessage());}
    }

   //cache all the primary competitors for the particular opportunity
   public void init(){      
    listComptr=[SELECT Id,Name,Primary_Competitor__c from Competitor__c where 
                                 Opportunity__r.Id =:ApexPages.currentPage().getParameters().get('retURL').substring(1,16) AND Primary_Competitor__c=true];     
   }

   //method to uncheck the primary competitor for other competitor records     
   @RemoteAction
   public static void checkPrimaryComp(String oppId){              
    Map<Id,Competitor__c> compMap=new Map<Id,Competitor__c>([SELECT Id,Name,Primary_Competitor__c from Competitor__c where Opportunity__r.Id =: 
                                         oppId.substring(1,16)]);     
     for(Competitor__c comp : compMap.values()){         
        comp.Primary_Competitor__c=false;}       
     update compMap.values();      
   }

   //invoked to check for the first primary competitor
   @RemoteAction
   public static String checkFirstPrimaryComp(String oppId){
   List<Competitor__c> listTemp=[SELECT Id,Name,Primary_Competitor__c from Competitor__c where 
                                 Opportunity__r.Id =:oppId.substring(1,16) AND Primary_Competitor__c=true];
       if(listTemp.size()==0){return 'true';}
       else{return 'false';}   

   }

   //method invoked on update of Competitor records
   public void onUpdatePrimaryComp(){
   try{
     if(Trigger.IsUpdate && !Trigger.IsInsert && flag==0){
     flag++;
     SET<Id> oppIds=new SET<Id>();          
     for(Sobject sobj: Trigger.new){
        Competitor__c cc1=(Competitor__c)sobj;
        oppIds.add(cc1.Opportunity__c);             
     }       
     List<Competitor__c> listTemp=[SELECT Id,Name,Primary_Competitor__c,Opportunity__r.Id from Competitor__c where Opportunity__r.Id in : oppIds AND
                               Primary_Competitor__c=true] ;        
     if(listTemp.size()!=0){
         for(Sobject sobj: Trigger.new){
             Competitor__c cc1=(Competitor__c)sobj;

             if(cc1.Primary_Competitor__c==true){
                 for(Integer i=0;i<listTemp.size();i++){
                     if(listTemp.get(i).Opportunity__r.Id == cc1.Opportunity__c){
                       listTemp.get(i).Primary_Competitor__c=false;}
                 }
             }
          }       
       update listTemp;}     
     }}
     catch(Exception e){System.debug('==Exception Message==='+e.getMessage());}
   }

    //method invoked to save and return to the original Opportunity record Page
    public PageReference Save(){

      Competitor__c comp=new Competitor__c();
      //List<Competitor__c> checklist=new List<Competitor__c>();
      //checklist=[select Competitor_Machine__C,Competitor_Machine_Description__c from Competitor__c];

      if (comp.Competitor_Machine__C == 'other' && comp.Competitor_Machine_Description__c == null)
      {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'If other is selected please enter a description');//Same old drama 
ApexPages.addMessage(myMsg);
          // Return null so the page won't redirect
        return null;
      }
      try{
      if(ApexPages.currentPage().getParameters().get('Name')!=null)
      {

      insert cc;
      }
      else 
      update cc;
      }
      catch(Exception e){}        
      return new PageReference('/'+ApexPages.currentPage().getParameters().get('retURL').substring(1,16));
    }

    //method is invoked on click of CANCEL button
    public PageReference Cancel(){
    try{
    if(ApexPages.currentPage().getParameters().get('Name')==null){
        if(listComptr.get(0).primary_Competitor__c==false){
            listComptr.get(0).primary_Competitor__c=true;
        }
    }
      update listComptr;
    }
    catch(Exception e){
      System.debug('==Exception Message==='+e.getMessage());
    }
     return new PageReference('/'+ApexPages.currentPage().getParameters().get('retURL').substring(1,16));

    }



}