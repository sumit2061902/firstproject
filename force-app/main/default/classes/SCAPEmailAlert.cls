/**
* This class called from scheduler
* responsible for sending email notification to Sales Rep
* as well as their Managers and so on.
**/
global class SCAPEmailAlert implements Database.Batchable<sObject>{
 
     String query = ' SELECT Name, GeoGraphy__c,ownerID,Employee_Id__c,Reminder_Notification__c,employee_Name__c, employee_Name__r.name,Accepted_By__c, status__c FROM Sales_Compensation_Plan__c where status__c=\'Pending\'';

                    global SCAPEmailAlert(){
                               
                    }
                    global Database.QueryLocator start(Database.BatchableContext BC){
                       
                         return Database.getQueryLocator(query);
                    }
      
                    
                    global void execute(Database.BatchableContext BC, List<Sales_Compensation_Plan__c>scope){
                    
                                     
                                         
                                        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
                                          List<String> sendTo = new List<String>();
                                                List<String> sendTo1 = new List<String>();
                                        
                                        List <String> allEmployeeIds = new List<String>() ;    
                                       
                                        for(Sales_Compensation_Plan__c item : scope){             
                                            allEmployeeIds.add(item.employee_Name__c);
                                        }
             
                                      
                                        List<User>empList = [Select Name,EmployeeNumber,Email,manager.email from User where ID IN :allEmployeeIds ];
                        
                                       string acc;
                                        Map <string,string> mapEmpEmail = new Map<string,string>(); 
                                        Map <string,string> mapEmpMgrEmail = new Map<string,string>();  
                                          
                                        for(User s: empList ){
                                            mapEmpEmail.put(s.ID , s.Email);
                                            mapEmpMgrEmail.put(mapEmpEmail.get(s.ID), s.manager.email);
                                        }

                                    for(Sales_Compensation_Plan__c s: scope){
                                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
              
                                        if(System.today() == s.Accepted_By__c){ 
                                        System.debug('Entering into First IF');
                                     
                                            
                                                string empEmail = mapEmpEmail.get(s.Employee_Name__C);                                            
                                                System.debug('Emp Email Id--'+mapEmpEmail);
                                                System.debug('Emp manager Email Id--'+mapEmpMgrEmail.get(empEmail));
                                                
                                             
                                                sendTo.add(empEmail);
                                              
                                                email.setToAddresses(sendTo);
                                                for(OrgWideEmailAddress owa : [select id, Address,DisplayName  from OrgWideEmailAddress]) {

                                                   if(owa.DisplayName.contains(s.GeoGraphy__c)) 
                                                    email.setOrgWideEmailAddressId(owa.id);

                                                  }               
                                                  

                                                email.setSubject('URGENT - RESPONSE REQUIRED: Sales Representation Letter');
                                               
                                                String body = 'Dear ' + s.Employee_Name__r.Name + ', ';
                                                body += 'This is a reminder notice that the Sales Representation Letter sent to you is now past due.'+'\n';
                                                body += 'This was due on  Our records show that you have not responded to the letter. For your reference,';
                                                body += 'the original email communication is attached below. You can access the representation letter by';
                                                body += 'clicking on the internal representation letter link on the attached email below. Please note that a response is mandatory.';
                                                body += 'Also, please be aware that your Target Manager is also receiving notice that your response has not yet been received.'; 
                                                body += 'This process is now standard and considered best practice in most US owned corporations where CEO and CFO who are publicly';
                                                body += 'accountable have to confirm to the governing authorities that they have at least asked the right questions and gained declarations from the relevant staff.';
                                                body +=     'Please follow the link, read the associated letter, and sign it electronically by clicking on the appropriate exception box.';
                                                body += 'If you have any questions or need assistance, please contact: rep-letter-admin@symantec.com.';

                                                email.setHtmlBody(body);
                                               allMails.add(email);
                                               
System.debug('You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');                                                
                                             
                                                
                                        }
                                     
                                      if(s.Accepted_By__c!=Date.today() && math.MOD(Date.today().daysBetween(s.Accepted_By__c),7) ==0 && s.Reminder_Notification__c!=false) 
                                        {
                                       
                                      
                                                string empEmail = mapEmpEmail.get(s.Employee_Name__C);                                            
                                               
                                                
                                             
                                               sendTo.add(empEmail);
                                               
                                              if(mapEmpMgrEmail.get(empEmail)!=null)
                                               {
                                                sendTo1.add(mapEmpMgrEmail.get(empEmail));
                                                 email.setCcAddresses(sendTo1);
                                                }
                                                
                                              
                                               email.setToAddresses(sendTo);
                                                 email.setCcAddresses(sendTo1);
                                               
                                              
                                           
                                              
                                             
                                                   
                                                for(OrgWideEmailAddress owa : [select id, Address,DisplayName  from OrgWideEmailAddress]) {

                                                    if(owa.DisplayName.contains(s.GeoGraphy__c)) 
                                                     email.setOrgWideEmailAddressId(owa.id);

                                                  }                                                 
                                                email.setSubject('URGENT - RESPONSE REQUIRED: Sales Representation Letter');
                                                String body = 'Dear ' + s.Employee_Name__r.name + ', ';
                                                body += 'This is a reminder notice that the Sales Representation Letter sent to you is now past due.'+'\n';
                                            
                                                body += 'This was due on  Our records show that you have not responded to the letter.For your reference,'+'\n';
                                                body += 'the original email communication is attached below. You can access the representation letter by';
                                                body += 'clicking on the internal representation letter link on the attached email below. <br><br>Please note that a response is mandatory.'+'\n';
                                                body += 'Also, please be aware that your Target Manager is also receiving notice that your response has not yet been received.'; 
                                                body += 'This process is now standard and considered best practice in most US owned corporations where CEO and CFO who are publicly';
                                                body += 'accountable have to confirm to the governing authorities that they have at least asked the right questions and gained declarations from the relevant staff.';
                                                body +=     'Please follow the link, read the associated letter, and sign it electronically by clicking on the appropriate exception box.';
                                                body += 'If you have any questions or need assistance, please contact: rep-letter-admin@symantec.com.';

                                                email.setHtmlBody(body);
                                                allMails.add(email);
                                               
System.debug('You have made ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');                                                
                                                 
                                       
                                                                   
                                        }
                                       sendTo.clear();
                                         sendTo1.clear();
                                    }
                                   
                                    if(!allMails.isEmpty())
                                    system.debug('allMails--->'+allMails);
                                    Messaging.sendEmail(allMails);
                                    
                         }
     
                    global void finish(Database.BatchableContext BC){
                              
                    }
    }