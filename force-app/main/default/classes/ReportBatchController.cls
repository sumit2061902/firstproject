public class ReportBatchController {
    
    public ReportBatchController(){}
    
    public String theTerms {
        get;set;
    }
        public String theFilter {
        get;set;
    }
        public String theAddresses {
        get;set;
    }
    
    public ReportBatchController(String a, String b, String c){
        this.theTerms = a;
        this.theFilter = b;
        this.theAddresses = c;
    }
    
    public PageReference action(){
    database.executebatch(new ReportBatch(theTerms, theFilter, theAddresses), 10000);
    return null;
    }
}