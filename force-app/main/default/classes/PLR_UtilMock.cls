@istest
global class PLR_UtilMock implements HttpCalloutMock {
    
    private Integer statusCode;
    
    public PLR_UtilMock(Integer statusCode) {
        this.statusCode = statusCode;
    }
 public PLR_UtilMock() {
        
    }    
    global HttpResponse respond(HttpRequest req) {
        
        HttpResponse res = new HttpResponse();
        HttpResponse res1 = new HttpResponse();
        
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        
        String endPoint = req.getEndpoint();
        System.debug('Endpoint: ' + endPoint);
        String reqBody = req.getBody();
        System.debug('Request Body: ' + reqBody);
        String xmlBody = '<?xml version="1.0" encoding="UTF-8"?>' +
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf="urn:fault.partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '	<soapenv:Body xmlns="http://schemas.xmlsoap.org/soap/envelope/">' +
            '		<loginResponse xmlns="urn:partner.soap.sforce.com">' +
            '			<result xmlns="urn:partner.soap.sforce.com">' +
            '				<sessionId xmlns="urn:partner.soap.sforce.com">sfdcSessionId</sessionId>' +
            '			</result>' +
            '		</loginResponse>' +
            '	</soapenv:Body>' +
            '</soapenv:Envelope>';
        
        res.setBody(xmlBody);  
        // res.setStatusCode(200); 
        
        
        
        // res1.setBody(xmlBody);
        // res1.setStatusCode(201);
        res.setStatusCode(statusCode);
        return res;	
        
    }
    
   /* public HttpResponse respond1(HttpRequest request) {
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings');
        throw e;
    }*/
}