public class CustomerSendEmailtoCon {
    
    public void sendemailcon(list<customerinfo__c> custList){
        EmailTemplate et=[Select id from EmailTemplate where name=:'Sales: New Customer Email'];
        //list of emails
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        list<id> conlist=new list<id>();
        for(customerinfo__c cust:custList){
            conlist.add(cust.Contact__c);
        }
        list<string> Emailist=new list<string>();
        map<id,contact> conmap=new map<id,contact>([select id,email from contact where id in :conlist ]);
        System.debug('conmap>>>'+conmap);
        for(customerinfo__c cust:custList){
            for(contact con:conmap.values()){
                if(con.Email!=null){
                    Emailist.add(con.Email);
                    if(conmap.containsKey(cust.Contact__c)){
                        System.Debug('Inside Contact>>>>');
                        cust.Contact_email__c=conmap.get(cust.Contact__c).email;
                        /* Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                            singleMail.setTargetObjectId(con.id);
                            singleMail.setTemplateId(et.Id);
                            singleMail.setSaveAsActivity(false);
                            System.debug('singleMail>>>'+singleMail);
                            emails.add(singleMail);*/
                    }
                }
            }
        }
        for(customerinfo__c cust:custList){
            Emailist.add(conmap.get(cust.Contact__c).email);
        }
        map<id,contact> conl=new map<id,contact>([select id,email from contact where email in :Emailist ]);
        for(customerinfo__c cust:custList){
            integer count=0;
            for(contact c:conl.values()){
                if(conl.get(cust.Contact__c).email!=null){
                    if(conl.containsKey(cust.Contact__c)){
                        if(conl.get(cust.Contact__c).email==c.Email){
                            count++;  
                        }
                    }
                }
                cust.ContactEmailCount__c=count;
                if(count>=5){
                    cust.contact__C.adderror('The Email--'+conl.get(cust.Contact__c).email+'--is aready associated with more then 5 contacts.');
                }
            }
        }
        // System.debug('emails>>>>'+emails);
        // Messaging.sendEmail(emails);
    }
}