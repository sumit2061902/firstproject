/**************************************************************************************************************
Page Name   ::      SupportCloudCaseListPageController
Purpose     ::      This class redirects the user to the Case Detail Page once the user updates the owner in 
                    the list view of the class
Created Date::      Feb 2011
Created By  ::      Santoshi Mishra
Modified History: 
                 :Balasaheb Wani
                 Issue # 1842, 
                 Exception handling at line for list index 
                 : Issue -1875 new method called retrievesCloudRecordTypeIdsForCaseAccept at Line 100
*************************************************************************************************************/
public with sharing class SupportCloudCaseListPageController {
    
    ApexPages.StandardSetController listcont;   
    public List <Case> caseToBeUpdated;
    Case detailCaseObj;
    public String selectedUser {get;set;}
    public String txtOwnersName{get;set;}
    public String txtOwnersId{get;set;}
    public Boolean  sendMail {get;set;}
    public Boolean renderError{get;set;}
    public List <String> selectedListId = new List<String>();
    public Boolean acceptFlag {get;set;}
    public Boolean acceptFlagForDetailPage {get;set;}
    public Boolean errorUpdateFlag {get;set;}
    public String errorUpdateMsg {get;set;}
    public String listViewFilterId {get;set;}
        
    
/*********************************************************************************************************
Purpose     ::  Controller to bind the pagereference with the extension class..
**********************************************************************************************************/         
    public SupportCloudCaseListPageController(ApexPages.StandardSetController controller)  
    {          
        listcont =controller;   
        acceptFlag = false; 
        acceptFlagForDetailPage=false; 
        caseToBeUpdated = listcont.getSelected(); // The selected record in the list view is assigned. 
        listViewFilterId = listcont.getfilterId();// The list view id from where the page is redirected is stored here.
        if(caseToBeUpdated==null ||(caseToBeUpdated!=null && caseToBeUpdated.isempty()) )
        {
            //Throws error if no record is selected in the list view and ChangeOwner button is clicked. 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Please select atleast one case..');
            ApexPages.addMessage(myMsg);
            renderError = true;
            
        }
        else
        { 
            renderError = false;        
            for(Case c:caseToBeUpdated)
            {
                selectedListId.add(c.Id);
                
               
            }
           // update(caseToBeUpdated);
        }               
    }    
      
}