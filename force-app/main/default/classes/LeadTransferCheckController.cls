@RestResource(urlMapping='/cantransferrecord/*')
global without sharing class LeadTransferCheckController{
    @httpGet
            global static void getLeadInfoForTransferCheck() {
                list<lead> lstLead = new list<lead>();
                restrequest req=restContext.request;
                Map<String,string> jsonheaderelement=new map<string,string>();
                
                for(String param: RestContext.request.params.keySet()){
                jsonheaderelement.put(param, RestContext.request.params.get(param));                
            }
                system.debug('jsonheaderelement:::::'+jsonheaderelement);
                system.debug('jsonheaderelement Keyset():::::'+jsonheaderelement.keyset());
                system.debug('jsonheaderelement.values():::::'+jsonheaderelement.values());
                    //lstLead = getLeadInfo(jsonHeaderElement);   
  					system.debug('lstLead:::::'+lstLead);
                 System.debug('validate(jsonheaderelement::::'+validate(jsonheaderelement));
                validate(jsonheaderelement);
                 String leadQueryString ='';
               leadQueryString = fetchLeadDetails(jsonheaderelement); 
                System.debug('leadQueryString:::'+leadQueryString);
            }     

    public static boolean validate(Map<string,string>validate){
       System.debug('Enter into Validate function');
        boolean validateflag=true;
        if(validate.get('id')==null || string.isBlank(validate.get('id'))){
            System.debug('Enter into Validate function');
            validateflag=false;
        }
        return validateflag;
    }
     public static list<lead> getLeadInfo(Map<string,string> req){
            
        List<Lead> leads = new List<Lead>();
        String leadQueryString ='';

        leadQueryString = fetchLeadDetails(req); 
        if(leadQueryString != null){
            leads = database.query(leadQueryString );
        }
          system.debug('leads:::'+leads);       
        return leads;
        
    }
    public static String fetchLeadDetails(Map<string,string> mapHeaderValues){
        
       string userinfo=userinfo.getuserID();
        user userecord;
        string querystring='select'+' ';
        userecord=[select id from user where id =:userinfo];
        map<string,schema.SObjectField>leadfields=schema.SObjectType.lead.fields.getmap();
        System.debug('leadfields::'+leadfields);
         System.debug('leadfields::'+leadfields.keyset());
         System.debug('leadfields::'+leadfields.values());
        for(string fieldname:leadfields.keyset()){
            querystring+=fieldname+',';
        }
        querystring=querystring.removeEnd(',');
        querystring=querystring+' '+'from lead limit 1';
        System.debug('querystring::'+querystring);
        return querystring;
    }
}