public class RoleUtil {

    /********************* Properties used by getRootNodeOfUserTree function - starts **********************/
    // map to hold roles with Id as the key
    private static Map <Id, UserRole> roleUsersMap;
    private static Map <Id,Sym_employee__c> roleUsersList;
    private static List <Sym_employee__c>roleUsersChildList;

    // map to hold child roles with parentRoleId as the key
    private static Map <Id, List<UserRole>> parentChildRoleMap;
    private static Map <Id, List<Sym_employee__c>> parentChildRoleMap1;

    // List holds all subordinates
    private static List<Sym_employee__c> allSubordinates {get; set;}
        
    // Global JSON generator
    private static JSONGenerator gen {get; set;}

    /********************* Properties used by getRootNodeOfUserTree function - ends **********************/
    
    
    /********************* Properties used by getSObjectTypeById function - starts ********************* */
    // map to hold global describe data
    private static Map<String,Schema.SObjectType> gd;
    
    // map to store objects and their prefixes
    private static Map<String, String> keyPrefixMap;

    // to hold set of all sObject prefixes
    private static Set<String> keyPrefixSet;
    /********************* Properties used by getSObjectTypeById function - ends **********************/
    
    /* // initialize helper data */ 
    static {
        // initialize helper data for getSObjectTypeById function
        init1();
        
        // initialize helper data for getRootNodeOfUserTree function
        init2();
    }
    
    /* // init1 starts <to initialise helper data> */
    private static void init1() {
        // get all objects from the org
        gd = Schema.getGlobalDescribe();
        
        // to store objects and their prefixes
        keyPrefixMap = new Map<String, String>{};
        
        //get the object prefix in IDs
        keyPrefixSet = gd.keySet();
        
        // fill up the prefixes map
        for(String sObj : keyPrefixSet) {
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
            String tempName = r.getName();
            String tempPrefix = r.getKeyPrefix();
            keyPrefixMap.put(tempPrefix, tempName);
        }
    }
    /* // init1 ends */

    /* // init2 starts <to initialise helper data> */
    private static void init2() {
        
        // Create a blank list
        allSubordinates = new List<sym_employee__c>();
        
        // Get role to users mapping in a map with key as role id
        roleUsersMap = new Map<Id, UserRole>([select Id, Name, parentRoleId, (select id, name from users) from UserRole order by parentRoleId]);        
        
        roleUsersList = new Map< ID, sym_employee__c>([select Id, Name from sym_employee__c where manager__c = null]);
        roleUsersChildList = new List<sym_employee__c>([select Id, Name from sym_employee__c where manager__c IN : roleUsersList.keySet()]);
        
        System.debug('roleUsersMap--'+roleUsersMap);
        System.debug('roleUsersList--'+roleUsersList);
        System.debug('roleUsersChildList--'+roleUsersChildList);
        
        // populate parent role - child roles map
        parentChildRoleMap = new Map <Id, List<UserRole>>(); 
        parentChildRoleMap1 = new Map<ID,List<sym_employee__c>>(); 	
        
        for(sym_employee__c symemp :roleUsersList.values()){
        		parentChildRoleMap1.put(symemp.ID,roleUsersChildList);
			}
        System.debug('parentChildRoleMap1--'+parentChildRoleMap1);
        
/*        for (UserRole r : roleUsersMap.values()) {
            List<UserRole> tempList;
            if (!parentChildRoleMap.containsKey(r.parentRoleId)){
                tempList = new List<UserRole>();
                tempList.Add(r);
                parentChildRoleMap.put(r.parentRoleId, tempList);
            }
            else {
                tempList = (List<UserRole>)parentChildRoleMap.get(r.parentRoleId);
                tempList.add(r);
                parentChildRoleMap.put(r.parentRoleId, tempList);
            }
        } */
		// For SLR 
	
     
      for (sym_employee__c r : roleUsersList.values()) {
            List<sym_employee__c> tempList;
            if (!parentChildRoleMap1.containsKey(r.Id)){
                tempList = new List<sym_employee__c>();
                tempList.Add(r);
                parentChildRoleMap1.put(r.Id, tempList);
                System.debug('Should not be here');
            }
            else {
                
                tempList = (List<sym_employee__c>)parentChildRoleMap1.get(r.Id);
                tempList.add(r);
                System.debug('Child for Parent--'+tempList);
                parentChildRoleMap1.put(r.Id, tempList);
            }
        } 
       
    } 
    /* // init2 ends */

    /* // public method to get the starting node of the RoleTree along with user list */
    public static RoleNodeWrapper getRootNodeOfUserTree (Id userOrRoleId) {
        return createNode(userOrRoleId);
    }
    
    /* // createNode starts */
    private static RoleNodeWrapper createNode(Id objId) {
       RoleNodeWrapper n = new RoleNodeWrapper();
        System.debug('objId--'+objId);
 /*         
        Id roleId;
        if (isRole(objId)) {
            roleId = objId; */
            System.debug('objId1111---'+objId);
        	System.debug('objId222---'+objId);
        	System.debug('parentChildRoleMap1.get(objId)---'+parentChildRoleMap1.get(objId));
            if (parentChildRoleMap1.get(objId)!=null) {
                
                for(sym_employee__c s :parentChildRoleMap1.get(objId)){
                    
               		    n.myUsers1 = parentChildRoleMap1.get(objId);
                    
                		allSubordinates.addAll(n.myUsers1);
                		n.hasUsers = true;
                }
 
            }
       /* } */
        else { 
            List<sym_employee__c> tempUsrList = new List<sym_employee__c>();
            sym_employee__c tempUser = [Select Id, Name from sym_employee__c where Id =: objId];
            System.debug('tempUser--'+tempUser);
             tempUsrList.add(tempUser);
             n.myUsers1 = tempUsrList;
       //     roleId = tempUser.UserRoleId;
        }
        //n.myRoleId = roleId;
        //n.myRoleName = roleUsersMap.get(roleId).Name;
        System.debug('parentChildRoleMap1.get(objId)---'+parentChildRoleMap1.get(objId));
        //n.myRoleName = parentChildRoleMap1.get(objId).Name;
        if(parentChildRoleMap1.get(objId)!=null){
        for(sym_employee__c p:parentChildRoleMap1.get(objId)){
            n.myParentRoleId = p.name;
        }
       } 
        if (parentChildRoleMap1.containsKey(objId)){
            n.hasChildren = true;
            n.isLeafNode = false;
            List<RoleNodeWrapper> lst = new List<RoleNodeWrapper>();
            for (sym_employee__c r : parentChildRoleMap1.get(objId)) {
                lst.add(createNode(r.Id));
            }           
            n.myChildNodes = lst;
        }
        else {
            n.isLeafNode = true;
            n.hasChildren = false;
        } 
        System.debug('--------Here---'+n);
        return n;
    }
    
    public static List<Sym_employee__c> getAllSubordinates(Id userId){
        createNode(userId);
        return allSubordinates;
    }
    
    public static String getTreeJSON(Id userOrRoleId) {
        gen = JSON.createGenerator(true);
        RoleNodeWrapper node = createNode(userOrRoleId);
        gen.writeStartArray();
        System.debug('--node---'+node);
            convertNodeToJSON(node);
        gen.writeEndArray();
        return gen.getAsString();
    }
    
    private static void convertNodeToJSON(RoleNodeWrapper objRNW){
        gen.writeStartObject();
        
          for(sym_employee__c myuser :objRNW.myUsers1)
          {
              System.debug('objRNW--'+myuser.Name);
              gen.writeStringField('title', myuser.Name);
          }
          
            
          // gen.writeStringField('title', 'Adeep');
          // gen.writeStringField('key', objRNW.myRoleId);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
            gen.writeBooleanField('isFolder', true);
         /*   if (objRNW.hasUsers || objRNW.hasChildren)
            {
                gen.writeFieldName('children');
                gen.writeStartArray();
                    if (objRNW.hasUsers)
                    {
                        for (User u : objRNW.myUsers)
                        {
                            gen.writeStartObject();
                                gen.writeStringField('title', u.Name);
                                gen.writeStringField('key', u.Id);
                            gen.WriteEndObject();
                        }
                    }
                    if (objRNW.hasChildren)
                    {
                        for (RoleNodeWrapper r : objRNW.myChildNodes)
                        {
                            convertNodeToJSON(r);
                        }
                    }
                gen.writeEndArray();
            } */
        gen.writeEndObject();
    }
    
    /* // general utility function to get the SObjectType of the Id passed as the argument, to be used in conjunction with */ 
    public static String getSObjectTypeById(Id objectId) {
        String tPrefix = objectId;
        tPrefix = tPrefix.subString(0,3);
        
        //get the object type now
        String objectType = keyPrefixMap.get(tPrefix);
        return objectType;
    }
    /* // utility function getSObjectTypeById ends */
    
    /* // check the object type of objId using the utility function getSObjectTypeById and return 'true' if it's of Role type */
    public static Boolean isRole (Id objId) {
        if (getSObjectTypeById(objId) == String.valueOf(UserRole.sObjectType)) {
            return true;
        }
        else if (getSObjectTypeById(objId) == String.valueOf(User.sObjectType)) {
            return false;
        } 
        return false;
    }
    /* // isRole ends */
    
    public class RoleNodeWrapper {
    
        // Role info properties - begin
        public String myRoleName {get; set;}
        
        public Id myRoleId {get; set;}
        
        public String myParentRoleId {get; set;}
        // Role info properties - end
        
        
        // Node children identifier properties - begin
        public Boolean hasChildren {get; set;}
        
        public Boolean isLeafNode {get; set;}
        
        public Boolean hasUsers {get; set;}
        // Node children identifier properties - end
        
        
        // Node children properties - begin
        public List<User> myUsers {get; set;}
        public List<Sym_employee__c> myUsers1 {get; set;}
    
        public List<RoleNodeWrapper> myChildNodes {get; set;}
        // Node children properties - end   
        
        public RoleNodeWrapper(){
            hasUsers = false;
            hasChildren = false;
        }
    }   
    
    @isTest
    static void testRoleUtil() {
        
        /*
        // test the output in system debug with role Id
        Id roleId = '00E90000000pMaP';
        RoleNodeWrapper startNodeWithRoleId = RoleUtil.getRootNodeOfUserTree(roleId);
        String strJsonWithRoleId = JSON.serialize(startNodeWithRoleId);
        system.debug(strJsonWithRoleId);
        */
        //system.debug('****************************************************');

        // now test the output in system debug with userId
        Id userId = UserInfo.getUserId() ;
        /*
        RoleNodeWrapper startNodeWithUserId = RoleUtil.getRootNodeOfUserTree(userId);
        String strJsonWithUserId = JSON.serialize(startNodeWithUserId);
        system.debug(strJsonWithUserId);
        */
        
        // test whether all subordinates get added
        //Id userId = '005900000011xZv';
        String str = RoleUtil.getTreeJSON(userId);
        //List<User> tmpUsrList = RoleUtil.getAllSubordinates('00E90000000pMaP');
        //system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%tmpUsrList:' + tmpUsrList);
   }
     
 }