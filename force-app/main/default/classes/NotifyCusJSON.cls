public class NotifyCusJSON {

   public cls_account Account;
    
    public class cls_account {     
		public string SendDateTime;
        public string ABN;
        public string CustomerId;
        public string cscrmExternalID;
        public string AccountUUID;
        public string FirstName;
        public string LastName;
        public string Phone;
        public string fax ;
        public string email;
        public string birthDate;
        public string Title;
        public string Type ;
        public string website;
       
   }
      public static NotifyCusJSON parse(String json){
        return (NotifyCusJSON) System.JSON.deserialize(json, NotifyCusJSON.class);
    }
}