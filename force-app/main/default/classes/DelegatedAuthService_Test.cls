/***********************************************
@Author: Yasdeep Sana
@name: DelegatedAuthServiceTest
@CreateDate:15/Nov/2018
@Description: Test class for DelegatedAuthService Class
@Version: 1.0
************************************************/
@isTest 
public class DelegatedAuthService_Test implements HttpCalloutMock {
    
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        return resp;
    }
    
    static testMethod void testDoPost(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        DelegatedAuthService_Test fakeResponse = new DelegatedAuthService_Test();
        
        
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        String ItemsInsert= '<node><Body xmlns=\'http://schemas.xmlsoap.org/soap/envelope/\' >'+
                    '<Authenticate xmlns=\'urn:authentication.soap.sforce.com\' >'+
                    '<username>username</username>'+
                    '<password>password</password>'+
                    '<sourceIp>sourceIp</sourceIp>'+
                    '</Authenticate>'+
                    '</Body></node>';
        
        req.requestBody = Blob.valueof(ItemsInsert);
        
        req.requestURI = '/services/apexrest/delegatedauth/';  
        System.assertEquals('/services/apexrest/delegatedauth/', req.requestURI);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res; 
        Test.startTest();
           DelegatedAuthService  results = new DelegatedAuthService();  
            DelegatedAuthService.doPost();
        Test.stopTest();
        

    }     
}