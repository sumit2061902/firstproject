@isTest
public class Test_ConTrigger {
    
    public static testmethod void checkCon(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System administrator']; 
       User u2 = new User(Alias = 'standt', Email='standareeduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standssdssasdsarduser@testorg.com');
        User u1 = new User(Alias = 'standt', Email='standareeduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, managerid=u2.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='standasdsarduser@testorg.com');
        User u = new User(Alias = 'standt', Email='standaregggeduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,managerid=u1.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standasdsarduser@testorg.com');
        insert u;
        System.runAs(u) {
           
            List<contact> contacts=new List<contact>();
            Map<id,contact>conmap=new map<id,contact>();
            Account acc=new account(name='TestAccount',AnnualRevenue=2121,Active__c='Yes',SLASerialNumber__c='200000',SLA__c='Gold');
            insert acc;
            contacts.add(new contact(lastname='Test',accountid=acc.id,primary_contact__C=true));      
            contacts.add(new contact(lastname='Test',accountid=acc.id,primary_contact__C=true));
            contact cons1=new contact(ownerid=u.id,lastname='Test',accountid=acc.id,primary_contact__C=true,Country_ISO_Code__c='US',Postal_Code__c='12345 1234');
            insert cons1;
            conmap.put(cons1.Id,cons1);
            System.debug('conmap>>>>'+conmap);
           
                ConControllertrigger con=new ConControllertrigger();
                 ConControllertrigger.firstRun = true;
                update cons1;
                con.ValidatePrimarycontacts(contacts);
                con.populateManager(contacts);
                con.BillProfile(contacts,conmap) ;
                con.ContactDeletePer(contacts, conmap);
            
            
        }
    }  
    
}