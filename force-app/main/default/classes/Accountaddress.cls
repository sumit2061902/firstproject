public class Accountaddress {
    public static void updateaddress(list<account> acclist) {
        Map<id,account> accmap=new map<id,account>();
        for(account acc:acclist){
            accmap.put(acc.id,acc);
        }
        list<contact>conupdate=new list<contact>();
        for(contact con:[select id,accountid,MailingCity,MailingCountry,MailingPostalCode,MailingState from contact where accountid in:accmap.keySet()]){
            account acc=accmap.get(con.accountid); 
            con.MailingCity=acc.BillingCity;
            con.MailingCountry=acc.BillingCountry;
            con.Mailingstreet=acc.Billingstreet;
            conupdate.add(con);
        }
        System.debug('conupdate>>>>'+conupdate);
        update conupdate;
        
    }
    Public static void AccshareChk(list<account>acclist,map<id,account>accoldmap){
        set<string>zips=new set<string>();
        set<id>changedacc=new set<id>();
        for(account acc:acclist){
            zips.add(acc.BillingPostalCode);
            changedacc.add(acc.id);
        }
        list<accountshare> shares=[select id from accountshare where accountid in:changedacc and RowCause='Manual'] ;
        System.debug('shares>>>>'+shares);
        delete shares;
        Map<string,territory__c> terrmap=new Map<string,Territory__C>();
        list<territory__C> territory=([select id,zip__c,(select id,user__c from territory_members__r)from territory__c where zip__c in :zips]);
        System.debug('territory>>>>'+territory);
        for(territory__c terr:territory){
            terrmap.put(terr.zip__c,terr);
        }
        System.debug('terrmap>>>>'+terrmap);
        list<accountshare>shareslist=new list<accountshare>();
        for(account a:acclist){
            territory__c terr=terrmap.get(a.BillingPostalCode);
            if(terr!=null){
                System.debug('terr>>>>'+terr);
                for(Territory_Members__c tm:terr.Territory_Members__r){
                    if(tm.user__c!=a.ownerid){
                        accountshare accshare=new accountshare();
                        accshare.accountid=a.id;
                        accshare.UserOrGroupId=tm.user__C;
                        accshare.AccountAccessLevel='Edit';
                        accshare.OpportunityAccessLevel='Edit';
                        shareslist.add(accshare);
                    }
                }
            }
            
        }
        System.debug('shareslist>>>>'+shareslist);
        insert shareslist;
        
    } 
}