public class AssignmentStatusController {
    public list<master_checkList__c> masterchklst{get;set;}
      public List<MasterTaskWrapper> masterTasks {get; set;}
    	public boolean chk{get;set;}    
    public pageReference getMasterTasks()
    {
        
        masterchklst=[select id,name from master_checkList__c];
    	masterTasks = new List<MasterTaskWrapper>();
        for(master_checkList__c mChkList:masterchklst)
        {
              masterTasks.add(new MasterTaskWrapper(mChkList));
            
        }
        
        return null;
    
    }
     public PageReference back() {  
      
       PageReference pageRef = new PageReference('/apex/WelcomeToSFDCFact');
        pageRef.setRedirect(true);
    return pageRef;        
    }
   
    public class MasterTaskWrapper{
         public Master_Checklist__c tsk {get; set;}
         public MasterTaskWrapper(Master_Checklist__c t)
         {
             tsk = t;
         }
    }
        

}