global  class DataMask implements database.batchable<sObject>,database.stateful {string query1;
    string query='';
    string str1='';
integer AP= 0;
string val;
    public DataMask(string query,string str) {
        string query1 = query;
        string str1 = str;

    }
    global string changeValues(string value,string param,integer counter) {
        string OriginalValue;
        OriginalValue=value;
        val = '';
        if(OriginalValue !=null || OriginalValue!='') {
            val = OriginalValue.replace(value,param);
            val = val + counter;
        }
        return val;
    }
    global database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query1);
    }
    global void execute(Database.BatchableContext bc,List<sobject>scope) {
        
        if(str1 == 'Account') {
            for(sobject so:scope) {
                account acc = (account)so;
                acc.Email__c = 'Test'+AP+'@test.com';
                acc.CID__c = changeValues(string.ValueOf(acc.CID__c),'CID',AP);
            }
            update scope;
        }
    }
    global void finish(Database.BatchableContext bc) {

    }
}
