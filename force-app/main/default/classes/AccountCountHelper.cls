public class AccountCountHelper {
    
    public static void agrCountIncr(list<id> accIdList){
        Set<ID> accSet=new Set<ID>();
        List<Account> accList = new List<Account>();
        Map<ID,Decimal> accountMapper=new Map<ID,Decimal>();
         Map<ID,Decimal> accCountMap = new Map<ID,Decimal>();
        System.debug('accIdList>>>>'+accIdList);
        for(integer i=0;i<accIdList.size();i++){
            accSet.add(accIdList[i]);
            System.debug('accSet>>>>'+accSet);
        }
        
        for(ID id1:accSet){
            
            integer count=0;
            
            for(ID id2:accIdList){
                
                if(id1==id2){
                    
                    count++;
                }
            }
           accountMapper.put(id1,count) ;
            
        }
        for(Account[] aList:[select Id,Participant_Count__c from account where id in :accSet order by ID]) {
            for(account a:aList){
                accCountMap.put(a.id,(a.Participant_Count__c+accountMapper.get(a.Id)));
                 System.debug('accCountMap>>>>'+accCountMap);
            }
        }
        for(ID id:accSet)       {
            Account acc=new Account(ID=id,Participant_Count__c=(accCountMap.get(id)!=null)?accCountMap.get(id):0);
                                    accList.add(acc);
        }
        if(!accList.isEmpty()){
             System.debug('<<<<<accList>>>>'+accList);
            database.update(acclist,false);
        }
    }
}