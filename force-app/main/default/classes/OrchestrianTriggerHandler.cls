public class OrchestrianTriggerHandler {
    public void OnafterInsert(List<CSPOFA_Orchestration_Process__c> newRecords){
        List<string>Parentrec=new list<string>();
        for(CSPOFA_Orchestration_Process__c csp:newrecords){
            Parentrec.add(csp.Parent_Orchestration_Process__c);
        }
        Map<id,Parent_Orchestration_Process__c> newMap=new map<id,Parent_Orchestration_Process__c>([select name from Parent_Orchestration_Process__c
                                                                                                  where id in:Parentrec]);
        for(CSPOFA_Orchestration_Process__c cs:newrecords){
           Parent_Orchestration_Process__c p=newmap.get(cs.Parent_Orchestration_Process__c);
            p.name=cs.name+p.name;
        }
        update newmap.values();
        
    }
    public  void onAfterUpdate(List<CSPOFA_Orchestration_Process__c> oldrecords,
                              Map<Id,CSPOFA_Orchestration_Process__C>OldMap,
                              List<CSPOFA_Orchestration_Process__c>newrecords,
                              Map<Id,CSPOFA_Orchestration_Process__C>NewMap){
        orchestrianHelper.rollUpCancellation(oldrecords,OldMap,newrecords,NewMap);
    }

}