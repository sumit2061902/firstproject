/************************************************************************************
* Name             : LeadSearchHelper
* Description      : This class contains common methods that will use in all the lead search classes.
                     OWD for Lead object is Public read and write so no need to use any CRUD permission for SOQL and DML operation
* Created By       : Accenture Offshore
* Created Date     : 19/May/2017
*************************************************************************************
* Date         Change-Request          Modified By                    Description
*************************************************************************************
*
*************************************************************************************/
public without sharing class LeadSearchHelper {
    
    //***************************************************************************************************
    // Method Name: createErrorResponse
    // Description: This method is for responding with an error response 
    // Params: RestResponse,Integer,string 
    // Return Type:Void   
    //****************************************************************************************************
    public static void createErrorResponse(RestResponse response, Integer statusCode, string message){
        RestContext.response.addHeader('Content-Type', 'text/xml');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize( message));
        RestContext.response.statusCode=statusCode; 
    }
    
    //***************************************************************************************************
    // Method Name: createSuccessResponseWithRecords
    // Description: This method is for responding with an success response including lead data
    // Params: RestResponse,Integer, List of leads
    // Return Type:Void   
    //****************************************************************************************************
    public static void createSuccessResponseWithRecords(RestResponse response, Integer statusCode, List<Lead> leadsList){
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(leadsList));
        RestContext.response.statusCode=statusCode; 
    }
    
    //***************************************************************************************************
    // Method Name: createExceptionLog
    // Description: This method is for creating an exception log record 
    // Params: String errorMessage, string errorCode, string businessCode 
    // Return Type:Void   
    //****************************************************************************************************
    public static void createExceptionLog(String errorMessage, string errorCode, string businessCode,string statusValue,String correlationIddd){//, String jsonPayload
        Exception_Log__c ex1 = new Exception_Log__c();        
       // ex1.Source_Name__c = LeadCustomerSearchController.class.getName();
        ex1.Destination_System__c = 'SFDC';
        ex1.End_Point_URL__c = '/services/apexrest/LeadSearch';
       
        insert ex1; 
    }
    
}