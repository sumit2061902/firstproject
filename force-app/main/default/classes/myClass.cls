public class myClass {
   public myClass() {
       values = new List<String>();
       values.add('Before On Click');
   }
    
   public static List<String> values {get; set;}
   
   public static void onClick() {
       values = new List<String>();
       values.add('After On Click');
   }
}