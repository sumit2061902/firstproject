public class ServeyCal {
    public static void createServeyNote(List<sev_form__c>sevReclist,Map<id,sev_form__c>oldSevmap){
        List<string> modifiedInfo=new list<string>();
        list<case>childCases=new list<case>();
        integer fieldCounter=0;
          Group OwnerGrp=[select Id from Group where Name = 'SMB' and Type = 'Queue' LIMIT 1];
        for(Sev_form__c sevRec:sevRecList){
            if(sevRec.Last_Day_of_Work__c<>oldSevmap.get(sevRec.id).Last_Day_of_Work__c){
                String fieldstr='Last day of work is changed from '+(oldSevmap.get(sevRec.id).Last_Day_of_Work__c).format()+'to'+(sevRec.Last_Day_of_Work__c).format();
                modifiedInfo.add(fieldstr);
                fieldCounter++;
            }
            
            if(sevRec.eid__c<>oldSevmap.get(sevRec.id).eid__c){
                String fieldstr='Eid is changed from '+oldSevmap.get(sevRec.id).eid__c+'to'+sevRec.eid__c;
                modifiedinfo.add(fieldstr);
                fieldcounter++;
            }
            if(sevRec.Site_Location__c<>oldSevmap.get(sevRec.id).Site_Location__c){
                String fieldstr='Eid is changed from '+oldSevmap.get(sevRec.id).Site_Location__c+'to'+sevRec.Site_Location__c;
                modifiedinfo.add(fieldstr);
                fieldcounter++;
            }
    
        User u=[select eid__c,id,FederationIdentifier from user where id =:userinfo.getuserid() limit 1];
        string eidss;
        string descr;
        list<contact>c2=new list<contact>();
        if(u.FederationIdentifier<>null){
            eidss=u.FederationIdentifier.substring(0,7);
             }
            for(string strvar:modifiedinfo){
                descr+=strvar+'\n'+'\n';
            }
    
        if(fieldcounter>=2){
case cs=new case(description=descr,subject=sevrec.Name,ownerid=OwnerGrp.id,RecordTypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Sales rep').getRecordTypeId());
   childCases.add(cs);
        }
    
}
    database.insert(childCases)   ;
    }
}