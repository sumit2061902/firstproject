Public with sharing class SendemailController{
   Public SendemailController(){
   }
  
 Public void sendEmailFunction(){
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   String[] toAddresses = new String[] {'sumit.rajaram.bhagat@accenture.com'}; 
  // String[] ccAddresses = new String[] {''};
   mail.setToAddresses(toAddresses);
  // mail.setCcAddresses(ccAddresses);
  // mail.setReplyTo('myemail@mail.com');
   mail.setSenderDisplayName('My Name');
   mail.setSubject('Testing email through apex');
   mail.setBccSender(false);
   mail.setUseSignature(false);
  // mail.setPlainTextBody('This is test email body. This mail is being sent from apex code');
  
  String body = '<html><body>Dear,Trent Flack \n';
                                                    body += 'Reminder: Your ompensation Plan Details are  available for your review and acceptance.<br><br>';
                                                    body += 'This was due on  Our records show that you have not responded to the Plan.Please click on the link below to the Sales Compensation Acceptance Tool to review your plan details<br> '; 
                                                
                                                    body += 'When you accept the sales plan, You are deem to have read, understood and agreed to the contentand terms and conditions of the sales plan.<br>';
                                                    body += 'Please note, if you choose to decline your plan';
                                                    body += 'the Global Sales Compensation team will be notified of your decline and will work with your manager and Sales Finance to address any concernsto final resolution.<br> '; 
                                                    body += 'If any aspect of your Compensation Plan changes during the fiscal year, you may be required to review and re-accept.<br>';
                                                    body += 'Should you have additional questions regarding your compensation plan details, please contact your manager.<br>Thank You</html></body>';

   mail.setHtmlBody(body);
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 } 

}