public class CaseResponseGenerator {

    public string Status;
    public CaseResponseGenerator(string inputStr)
    {
        this.Status = inputStr;
    }
    
    public static string GenratePayload(string inputJson)
    {
        CaseResponseGenerator caseObj = new CaseResponseGenerator(inputJson);
        return Json.serialize(caseObj);
        
    }
}