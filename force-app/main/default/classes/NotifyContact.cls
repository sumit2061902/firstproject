@RestResource(urlMapping='/NotifyContact/*')
global without sharing class NotifyContact{     
 
    Static String requiredFields = '';
    static Savepoint sp;
     static String sourceAPI='NotifyContact';
     static String correlationId;
 
    /**   
    * @MethodName: UpsertContactRecords
    * @Arguments: Null
    @Description: This method is for Parsing Json string ,Validating Json String and performing 
                   DML operations using below methods.    
    **/
    @HTTPPOST
    global static void UpsertContactRecords(){
        Integer retryCount;
        //String correlationId;
        String operation; 
        String contactId; 
        String primaryAccId;      
        String jsonResponse = '';
        String primaryTCMUUID;
        String sContactUUID ='';
System.debug('Inside method::::::');
        
        Map<String, String> tcmIDToAccId = new Map<String, String>(); 
        //String primaryAccUUId;
        String contactType;
        Integer numAcc = 0;
        RestRequest req;
        RestResponse res;      
        SET<String> addressIds = new SET<String>();
        SET<String> accountIds = new SET<String>();        

        List<Contact> contList2;
        List<Contact> contList;

        NotifyContactJSON.cls_Contact csReq =  new NotifyContactJSON.cls_Contact();
        Contact conRec;//for insert scenarios       
        try{  
            //lastUpdated                    
            DateTime sendDateTime;        
            req = RestContext.request;
            res = RestContext.response;   
           
            
                    
            string jsonVal=req.requestBody.toString();
            NotifyContactJSON obj = NotifyContactJSON.parse(jsonVal);   
			System.debug('jsonVal::::::'+jsonVal);
                csReq = obj.Contact;    
                contactId = csReq.cscrmExternalID;//TCM UU id used to uniquely identify a contact
                sContactUUID =csReq.ContactUUID;//ScSE-1551
 
                   System.debug('csReq::::::'+csReq);    
                    //contList=[SELECT id,name ,ContactUUID__c, Send_Date_Time__c, TCMUUID__c , FirstName, MiddleName__c,        
                                        //LastName,Title,BirthDate,Email,Fax ,Phone FROM Contact WHERE TCMUUID__c =: contactId Limit 1];
                           
                    //get if there is an existing Contact Record with the ContactUUID coming in the Payload            
                    contList2=[SELECT id,name ,TCMUUID__c , FirstName,         
                                        LastName,Title,BirthDate,Email,Fax ,Phone FROM Contact WHERE TCMUUID__c =: contactId Limit 1 ];
     				System.debug('contList2::::'+contList2);
                    if(!contList2.isEmpty()){
                        //system.debug('here in 137');
                        conRec = contList2[0]; 
                        operation ='Update'; 
                    }
            else{
                        if(contList2.isEmpty()){
                            conRec = New Contact();
                            operation = 'Insert';                  
                        }
                        else{//if it is an update then assign the first contact in the list to the object record
                            conRec = contList2[0]; 
                            operation = 'Update';                
                        }
                    }
                                       
                        mapContactJSON(conRec, csReq); //transform json to contact record    
                        try{    
                            If(operation =='Insert'){     
                                System.debug('inside Insert::::');
                                insert conRec;     
                            }
                            else
                            {
                                update conRec;                           
                            }
                        }
                        catch (Exception e) {
                                              } 

                      
    }
        catch(Exception e)	{
            
        }
	}
    /**   
    * @MethodName: mapContactJSON
    * @Arguments: Contact Object, Contact JSON object
    @Description: This method is for mapping json to SFDC contact object
    
    **/
    public static void mapContactJSON(Contact conRec, NotifyContactJSON.cls_Contact ContReq) {
                                
        conRec.FirstName = ContReq.FirstName;
        conRec.LastName = ContReq.LastName;
        conRec.Title = ContReq.Title;
        conRec.Salutation = ContReq.Salutation;
        conRec.TCMUUID__c = ContReq.cscrmExternalID;
        conRec.Birthdate =  ContReq.birthDate != null && ContReq.birthDate != '' ? Date.valueof(ContReq.birthDate):null;                
        conRec.Email= ContReq.Email ;
        conRec.MobilePhone = ContReq.MobilePhone;
        conRec.Phone = ContReq.phone;    
        conRec.OtherPhone= ContReq.OtherPhone;
        conRec.HomePhone = ContReq.HomePhone;
    }   
}