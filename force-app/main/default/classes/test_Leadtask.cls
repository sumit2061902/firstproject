@istest
public class test_Leadtask {

    public testmethod static void testLead(){
        list<task>lstask=new list<task>();
        lead l=new lead(status__c='Open',Status='Open - Not Contacted',lastname='testLead',company='cts');
        insert l;
        for(integer i=0;i<6;i++){
            lstask.add(new task(whoid=l.Id,subject='call'));
        }
        insert lstask;
        update l;
        system.assertEquals(l.Description,'More that 15 tasks are assigned');
    }
}