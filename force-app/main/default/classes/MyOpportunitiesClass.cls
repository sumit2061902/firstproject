public class MyOpportunitiesClass
  { 
public List<Opportunity> OpportunityRecords{get;set;}
  public String Stage {get;set;}
  public String recordId;
 public List<SelectOption> getStages()
  {
  List<SelectOption> options = new List<SelectOption>();
  // options.add(new SelectOption('None','--- None ---')); 
options.add(new SelectOption('Qualification','Qualification'));
  options.add(new SelectOption('Closed Won','Closed Won'));
  options.add(new SelectOption('Closed Lost','Closed Lost'));
  options.add(new SelectOption('Negotiation/Review','Negotiation/Review')); 
return options;
  } 

public MyOpportunitiesClass(ApexPages.StandardController controller) 
{
  Id user_id = UserInfo.getUserid(); 
OpportunityRecords = new List<Opportunity>();
  OpportunityRecords = [select name,account.name,CloseDate,Probability from Opportunity where StageName = 'Qualification' and ownerID=:user_id ];
 }
 public void RefreshTable(){
  Id user_id = UserInfo.getUserid(); 
OpportunityRecords = new List<Opportunity>();
  if(Stage == 'Qualification')
  OpportunityRecords = [select name,account.name,CloseDate,Probability from Opportunity where StageName = 'Qualification' and ownerID=:user_id ];
  else if(Stage == 'Closed Won')
  OpportunityRecords = [select name,account.name,CloseDate,Probability from Opportunity where StageName = 'Closed Won' and ownerID=:user_id ];
  else if(Stage == 'Closed Lost')
  OpportunityRecords = [select name,account.name,CloseDate,Probability from Opportunity where StageName = 'Closed Lost' and ownerID=:user_id ];
  else if(Stage == 'Negotiation/Review') 
OpportunityRecords = [select name,account.name,CloseDate,Probability from Opportunity where StageName = 'Negotiation/Review' and ownerID=:user_id ];
 } 
}