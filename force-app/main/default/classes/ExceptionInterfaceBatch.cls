global class ExceptionInterfaceBatch  implements Database.batchable<sobject>,database.stateful {

    global database.querylocator start(database.batchableContext bc ) {
        datetime dt=datetime.now();
        ErrorHandlerPurge_Parameter__mdt ep=[select unit__c,value__c from ErrorHandlerPurge_Parameter__mdt];
        if(ep.unit__c == 'hours'){
            dt=dt.addhours(integer.valueof(ep.value__c));
        }
        if(ep.unit__c == 'days'){
            dt=dt.adddays(integer.valueof(ep.value__c));
        }
        string query = 'select id from exception_log__c where createddate < :dt';
        return database.getquerylocator(query);
    }
    global void execute(database.batchableContext bc,list<exception_log__c>scope){
        list<exception_log__c> exlist = new list<exception_log__c>();
        for(exception_log__c ex:scope) {
            if(string.valueof(ex.id).startsWith('a1g')){
                exlist.add(ex);
            }
        }
        database.deleteresult[] rs=database.delete(exlist,false);
        for(database.deleteresult dr:rs){
            if(dr.success){
                system.debug('Successfully Deleted Records'+dr.getId());
            }
            else {
                for(database.error error:dr.getErrors()) {
                    system.debug(error.getstatusCode()+'::'+error.getMessage());
                }
            }

        }
    }
    global void finish(database.batchablecontext bc){
         AsyncApexJob job  = [SELECT Id, Status, NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE Id =:bc.getJobId()];

    }
}