Global Class UpdateAccountStartDate implements Database.Batchable<sobject>{
	String Query;
   	 global database.QueryLocator start(database.BatchableContext BC){
        
        Query='Select id,name,start_date__c,Customer_Status__c FROM Account where start_date__c=null';
         system.debug('Query1----'+Query);
        return database.getQueryLocator(Query);
          
    }
    global 	void execute(database.BatchableContext BC,list<Account> acc){
        system.debug('Query2----'+Query);
        for(Account ac:acc)
        {
         
                ac.start_date__c=system.today();
           		system.debug('ac----'+ac);
            update ac;
        }
    }
    global void finish(database.BatchableContext BC)    {
        
    }
    
}