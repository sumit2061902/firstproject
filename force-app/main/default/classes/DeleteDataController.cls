public class DeleteDataController {
    public String objectName1{get;set;} // input text1 value  from vf
  public list<selectoption> nameOfObjects{get;set;}
  public map<string,schema.SobjectType> mapOfTokens{get;set;}
  public List<String> listToHoldNames{get;set;}
  public string selectedObject{get;set;}
  
  public Sobject objectName{get;set;}
  public List<Sobject> objectNames{get;set;}
  public List<Sobject> objects{get;set;}
  public List<String> fields{get;set;}
  public String query{get;set;}
  public List<string> newNameOfFields{get;set;}
   public String ModifiedDate{get;set;}
    public String recordCount{get;set;}
  /*public DeleteDataController(){

  mapOfTokens=Schema.getGlobalDescribe();
  listToHoldNames= new list<string>();
  nameOfObjects= new list<selectoption>();
  listToHoldNames.addAll(mapOfTokens.keyset());
  listToHoldNames.sort();
  nameOfObjects.add(new selectoption('None','--None--'));
  for(string s:listToHoldNames){
  nameOfObjects.add(new selectoption(s,s));
  newNameOfFields= new List<String>();
  }
  fields = new  List<String>();
  objectNames=new List<Sobject>();
  }
  */
    public PageReference getDeleteRecords(){
        DeleteRecords.createQuery(objectName,ModifiedDate,recordCount);
        PageReference pg = new PageReference('/apex/ThanksPage');
        pg.setRedirect(true);
        return pg;
    }
}