public class AccountTriggerHelper {
    public static void accountbeforeInsert(List<account>accountlist){
        AccountTriggerHelper.generateAccountUUID(accountlist);
    }
    public static void accountbeforeUpdate(List<account>accountlist){
        accountTriggerHelper.generateBusinessKey(accountList);
    }
    
    public static list<account> generateBusinessKey(List<account>accountlist){
         List<account> updatedAccountList = new List<account>();
        for(account acc:accountlist){
            if(acc.Factory__c=='CMT1'){
                if(!string.isEmpty(acc.Id)){
                acc.SLASerialNumber__c=acc.id;
                    }
                }
            else
            {
             String generateConsKey=''   ;
                String FirstNamestr='';
                if(acc.Factory__c!=null){
                    if(acc.Factory__c.equalsIgnoreCase(accountutility.CustomerTypeResdential)){
                        generateConsKey=accountutility.CustomerTypeResdential;
                    }
                        else if(acc.Factory__c.equalsIgnoreCase(accountutility.CustomerTypeSoleTrader)){
                        generateConsKey=accountutility.CustomerTypeSoleTrader;
                            
                        }
                          
                    }
              
                if(acc.Status__c != null){
                            generateConsKey += string.ValueOf(acc.Status__c) ;
                        }
                acc.testReadonly__c= generateConsKey.trim() ; 
                 }
            updatedAccountList.add(acc);
           
            }
          return updatedAccountList;
        
        }
        
    
    
    
    
    public static void generateAccountUUID(List<account>accountlist){
        for(account acc:accountlist){
            if(acc.CID__c!=null || acc.CID__c!=''){
                acc.CID__c=CC_UUID.newUUID();
            }
            
        }
    }
}