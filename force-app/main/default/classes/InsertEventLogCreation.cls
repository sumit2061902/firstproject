public class InsertEventLogCreation {
    
    public static void createEventLog(List<account>acclist){
        List<account>aclist=new list<account>();
        aclist=[select id ,Name from account where id in :acclist];
        List<Event_Log__c> eventloglist= new list<Event_Log__c>();
        for(account acc:aclist){
        Event_Log__c ev=new Event_Log__c();
        ev.name=acc.name;
        ev.Status__c='Cancelled';
        ev.Correlation_Id__c='X000X';
        eventloglist.add(ev);
        }
        insert eventloglist;
    }
    
    
}