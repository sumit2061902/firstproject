/*
*/
public with sharing class SolarAsBuiltCaseWizardController1 {
    public List<Task> allTasks {get;set;}
    public List<Task> newAndOpenTasks {get;set;}
     public String CloseCaseID{get; set;}
public list<task>  bookList {get;set;}
 public Boolean disableInput {get; set;}
    //Params
    String baseCaseId {get;set;} //The Case that launched this Popup
    
    /*Constructor*/
    public SolarAsBuiltCaseWizardController1(ApexPages.StandardController stdcon){
        prepareData();
    }
    
    private void prepareData(){
        baseCaseID = ApexPages.currentPage().getParameters().get('caseId');
        
        
        if(!String.isBlank(baseCaseID)){
            allTasks = [ SELECT Subject, Type, Status, CreatedById, 
                                CreatedDate, LastModifiedById, 
                                LastModifiedDate from Task where 
                                WhatId = :baseCaseID ];
            newAndOpenTasks = [ SELECT Subject, Type, Status, CreatedById, 
                                CreatedDate, LastModifiedById, 
                                LastModifiedDate from Task where 
                                WhatId = :baseCaseID and Status != :'Completed' ];
        }
    }
    
    public Boolean getHasAllTasks(){
        return allTasks.size()>0;
    }
     public void enableCloseDateInput(){
        disableInput = false;
        }
    public Boolean getHasExistingTasks(){
        return newAndOpenTasks.size()>0;
    }
    
    public void processLinkClick()
    {
    
    task t=[select status from task where id=:CloseCaseID];
    System.debug('===Task ID=='+t.status);
    if(t.status!='completed')
    {
    t.status='completed';
    update t;
    }
     
    }
       
}