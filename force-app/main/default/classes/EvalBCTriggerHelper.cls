public class EvalBCTriggerHelper {
    string iOldUserCount;
    string iNewUserCount;
    long AdditionUser;
    public void  BU_Event(List<Eval__C>EvalNew,List<Eval__C>EvalOld,Map<Id,Eval__c>EvalNewMap,Map<Id,Eval__c>EvalOldMap){
        UpdateAdditionalTrialUsers(EvalNew,EvalOld,EvalNewMap,EvalOldMap);
        BeforeInsertUpdateEval(EvalNew, EvalOld, EvalNewMap, EvalOldMap, True, False);
    }
    
    public void UpdateAdditionalTrialUsers(List<Eval__C>EvalNew,List<Eval__C>EvalOld,Map<Id,Eval__c>EvalNewMap,Map<Id,Eval__c>EvalOldMap){
        try{
            
            for(eval__c eval:EvalNew){
                for(eval__c evalld:evalOld){
                    iOldUserCount=evalld.Participating_Trial_Users__c;
                    iNewUserCount=eval.Participating_Trial_Users__c;
                    //AdditionUser=iOldUserCount+iNewUserCount;
                }
                //if(AdditionUser>0){
                    eval.Additional_Trial_Users__c=iNewUserCount;
                //}
                
                
            }
        }
        catch(System.DMLException e){
                     System.debug('Error : '+e);
                }
    }
    
    public void BeforeInsertUpdateEval(List<Eval__C>EvalNew,List<Eval__C>EvalOld,Map<Id,Eval__c>EvalNewMap,Map<Id,Eval__c>EvalOldMap,Boolean IsUpdate, Boolean IsInsert){
        list<id> usrID=new list<id>();
        
        for(Eval__C eval:evalnew){
            usrId.add(eval.sales_engineer__C);
        }
        list<user>seUsrLst=[select id ,managerid from user where id in:usrId];
        Map<id,id>UserMap=new map<id,id>();
        for(user u:seUsrLst){
            UserMap.put(u.id,u.managerid);
        }
        
        for(Eval__c ev:evalnew){
            ev.SE_Manager__c=usermap.get(ev.sales_Engineer__C);
        }
        
    }
        }