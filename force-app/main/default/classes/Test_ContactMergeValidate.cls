@isTest
public class Test_ContactMergeValidate {
    
    public static testmethod void validateMergeRecords(){
        Merge_request__c mr=new Merge_request__c();
        mr.Name='test Merge';
        mr.Merge_Status__c='New';
        mr.Source_Number__c='111001';
        mr.Target_Number__c='40440110';
        insert mr;
        
        contact con = new contact();
        con.lastname='test contact';
        con.TCMUUID__c='11201001';
        con.Merge_Status__c='New';
        insert con;
       
        list<merge_request__c> mrgList= new list<merge_Request__c>([SELECT Source_Number__c,Target_Number__c,Type__c,SFDC_Status__c,Batch_Order__c,Merge_Status__c,SFDC_Error__c,SFDC_Error_Code__c,Batch_Name__c from Merge_Request__c ]);
       ContactMergeValidate.validateMergeRecords(mrgList);
        
    }
      public static testmethod void validateMergeRecords1(){
        Merge_request__c mr=new Merge_request__c();
        mr.Name='test Merge';
        mr.Merge_Status__c='New';
        mr.Target_Number__c='40440110';
        insert mr;
        
        contact con = new contact();
        con.lastname='test contact';
        con.TCMUUID__c='40440110';
        con.Merge_Status__c='New';
        insert con;
       
        list<merge_request__c> mrgList= new list<merge_Request__c>([SELECT Source_Number__c,Target_Number__c,Type__c,SFDC_Status__c,Batch_Order__c,Merge_Status__c,SFDC_Error__c,SFDC_Error_Code__c,Batch_Name__c from Merge_Request__c ]);
       ContactMergeValidate.validateMergeRecords(mrgList);
      }
          public static testmethod void validateMergeRecords2(){
        Merge_request__c mr=new Merge_request__c();
        mr.Name='test Merge';
        mr.Merge_Status__c='New';
 mr.Source_Number__c='11201001';
        insert mr;
        
        contact con = new contact();
        con.lastname='test contact';
        con.TCMUUID__c='11201001';
        con.Merge_Status__c='New';
        insert con;
       
        list<merge_request__c> mrgList= new list<merge_Request__c>([SELECT Source_Number__c,Target_Number__c,Type__c,SFDC_Status__c,Batch_Order__c,Merge_Status__c,SFDC_Error__c,SFDC_Error_Code__c,Batch_Name__c from Merge_Request__c ]);
       ContactMergeValidate.validateMergeRecords(mrgList);
      }
        public static testmethod void validateMergeRecords3(){
        Merge_request__c mr=new Merge_request__c();
        mr.Name='test Merge';
        mr.Merge_Status__c='Merged';
 mr.Source_Number__c='11201001';
        insert mr;
        
        contact con = new contact();
        con.lastname='test contact';
        con.TCMUUID__c='11201001';
        con.Merge_Status__c='Merged';
        insert con;
       
        list<merge_request__c> mrgList= new list<merge_Request__c>([SELECT Source_Number__c,Target_Number__c,Type__c,SFDC_Status__c,Batch_Order__c,Merge_Status__c,SFDC_Error__c,SFDC_Error_Code__c,Batch_Name__c from Merge_Request__c ]);
       ContactMergeValidate.validateMergeRecords(mrgList);
      }
}