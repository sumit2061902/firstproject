global class DataStorageNotification implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return null;
    }
    global void execute(Database.BatchableContext bc, List<sobject> records){
      DataLimitController dc =new DataLimitController();
        dc.getDataLimit(records);
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}