@restResource(urlmapping='/NotifyCustomer/*')
global without sharing class NotifyCust {
    static String requiredFields = '';    
    static Boolean validateFlag = false;
    static Savepoint sp; 
    static String correlationId;
    @HttpPost
    global static void upsertCustomerRecords(){
        
        RestRequest req;
        RestResponse res;
        String sendDate = ''; 
        String customerId ='';
        String operation; 
        DateTime sendDateTime=null; 
        String jsonResponse = '';
        // String correlationId;
        Integer retryCount;
        String mStatus='' ;
        String  updExternalId='';
        String isExternalId ='';
        String corelId = '';
        //String trklogID='';
        String isSourceSystem='';
        String sresponsevent='';
        List<Account> accList; 
        
        try{
            Account accountRec=new account();
            req=restcontext.request;
            res=restcontext.response;
            res.addHeader('CONTENT_TYPE', 'APPLICATION_JSON');
            
            System.debug('req:::'+req);
            System.debug('res:::'+res);
            
            string jsonVal=req.requestBody.tostring();
            NotifyCustomerJSON obj=NotifyCustomerJSON.parse(jsonval);
            System.debug('jsonVal:::'+jsonVal);
            System.debug('obj:::'+obj);
            if(validate(obj, correlationId)){
                validateFlag = false; //static flag set to false
                
                /*******Send Date Time Section Starts*******/
                String reqSendDate = obj.Account.SendDateTime;
                System.debug('reqSendDate:::'+reqSendDate);
                reqSendDate='"'+reqSendDate+'"';
                DateTime reqSendDateTime= (DateTime)JSON.deserialize(reqSendDate,DateTime.class);
                System.debug('reqSendDateTime:::'+reqSendDateTime);
               
                NotifyCustomerJSON.cls_account ncJson = new NotifyCustomerJSON.cls_account();
                ncJson = obj.Account;  
                System.debug('ncJson '+ncJson);
                customerId = ncJson.CustomerId;
                isExternalId = ncJson.cscrmExternalID;
                isSourceSystem = ncJson.SourceSystem;
                
                accList = [select id, Name,Customer_Id__c,Send_Date_Time__c,Title__c,Firstname__c,Lastname__c,Birthdate__c,Phone,Fax,Email__c,Website,
                          Preferred_Communication_Mode__c,Merge_Status__c,csm_External_ID__c
                          from Account where Customer_ID__c =: customerId LIMIT 1]; 
                
                System.debug('acclist::::'+acclist.size());
                
                List<NotifyCustomerJSON.cls_AccountAddressRelationship> addressJunctionList =  ncJson.AccountAddressRelationship;

                Map<String,List<NotifyCustomerJSON.cls_AccountAddressRelationship>> addressMap = new Map<String,List<NotifyCustomerJSON.cls_AccountAddressRelationship>>();

                addressMap.put(ncJson.CustomerId,addressJunctionList); 

                Map<String,String> accountAdressMap = new Map<String,String>();

                set<String> accountAddressSet = new set<String>();
                set<String> addressSet = new set<String>();
                
                       /***Send Date Time Starts****/
                if(!accList.isEmpty()){
                    if(accList[0].Send_Date_Time__c !=null && accList[0].Send_Date_Time__c != ''){
                        System.debug('Inside If of send date:::');
                        sendDate = String.valueof(accList[0].Send_Date_Time__c);  
                         
                        sendDate='"'+sendDate+'"';                                         
                        sendDateTime=(DateTime)JSON.deserialize(sendDate,DateTime.class); 
                        mStatus = accList[0].Merge_Status__c;
                        updExternalId = accList[0].csm_External_ID__c; 
                        System.debug('sendDate:::'+sendDate);
                        System.debug('sendDateTime:::'+sendDateTime);
                        System.debug('mStatus:::'+mStatus);
                        System.debug('updExternalId:::'+updExternalId);
                        
                        
                    }
                    

                }     /***Send Date Time End****/ 
                 System.debug('After If call:::');  
                 if(sendDateTime<reqSendDateTime || sendDateTime==null){
                    try{
                        if(accList!=null && accList.size()>0)
                        {
                           
                            accountRec = accList.get(0);
                            operation ='Update'; 
                     System.debug('accountRec::::'+accountRec);
                            System.debug('operation::::'+operation);
                            mapAccountJSON(accountRec,ncJson); //calling method for mapping account 
                        }
                        else
                        {
                            accountRec = new Account();  
                            operation = NBNConstants.INSERTOperation; 
                            mapAccountJSON(accountRec,ncJson);
                        }
                        If(operation == NBNConstants.INSERTOperation){
						System.debug('operation::::'+operation);
                            insert accountRec;  
                        }
                        else
                        { 
                            System.debug('Inside Else for Updation');
                            update accountRec; 
                        }
					}
                     catch(Exception e){
                         
                     }
 }
 }
            
            }
        
        catch(Exception e){
            
        }
        
    }
    public static Boolean validate(NotifyCustomerJSON validate, String correlationId){        
        System.debug('+++++++++++');
        List<NotifyCustomerJSON.cls_AccountAddressRelationship> listAddressRec = new List<NotifyCustomerJSON.cls_AccountAddressRelationship>();
        listAddressRec = validate.Account.AccountAddressRelationship; 
        
        if(validate.Account.CustomerId.trim() !=null && validate.Account.CustomerId.trim() != NBNConstants.VALUE_BLANK){
            validateFlag=true;
        }
        else
        {
            validateFlag=false;
            requiredFields = NBNConstants.CUSTOMER_ID;
        }        
        
        If(String.isBlank(validate.Account.SendDateTime)){
            validateFlag=false;
            requiredFields = requiredFields +'Send Date Time;';
        }
        
        if(String.isBlank(validate.Account.LastName)){
            validateFlag=false;
            requiredFields = requiredFields + '	Last Name;' + ';';
        }
        
        //if(validateFlag==true){
        //listAddressRec.addAll(validate.Account.AccountAddressRelationship);
        if(!listAddressRec.isEmpty()){
            Integer listAddressRecSize = listAddressRec.size();
            for(integer i = 0;i<listAddressRecSize;i++){                    
                if(String.isBlank(listAddressRec[i].cscrmExternalAddressId)){                          
                    validateFlag= false;
                    requiredFields = requiredFields + 'Address Id;';
                }
            }
        }
        //}Four_Houndred        
        System.debug('ValidateFlag:::'+ValidateFlag);
        return validateFlag;
    }
     public static void mapAccountJSON(Account accRec2, NotifyCustomerJSON.cls_account csReq2) {
     System.debug('Calling method :::::');
             accRec2.Lastname__c = csReq2.lastname;
             accRec2.Name = csReq2.firstName + NBNConstants.VALUE_BLANK + csReq2.lastname;
             accRec2.Customer_Id__c = csReq2.CustomerId; 
             accRec2.Title__c  = csReq2.title;  
             accRec2.Firstname__c = csReq2.firstName;             
         accRec2.csm_External_ID__c = csReq2.cscrmExternalID;
             accRec2.Send_Date_Time__c = csReq2.SendDateTime;
             accRec2.Phone  = csReq2.Phone; 
             accRec2.Fax  = csReq2.fax; 
             //System.debug('Email'+csReq2.email);
             accRec2.Email__c = csReq2.email; 
             accRec2.Website= csReq2.website;  
 
             accRec2.Preferred_Communication_Mode__c = csReq2.PreferredCommunicationMode;
             accRec2.Merge_Status__c = csReq2.MergeStatus;//Added by Mounika for US:SCSE-723
             //accRec2.TCMUUID__c = csReq2.cscrmExternalID;//Replaced cscrm__External_ID__c with TCMUUID__c 
       
         }
   
}