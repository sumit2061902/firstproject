public class PLR_Util {
	
	private static Map<ID, User> mapUserProfile;
	public static string AUDIT_LOG_STATUS_STARTED = 'Started';
	public static string AUDIT_LOG_STATUS_FAILED = 'Failed';
	public static string AUDIT_LOG_STATUS_SUCCESS = 'Success';
	
	/*
	* This method is to get session id for api callout
	* 
	*/
	public static string getSessionId() {
		try {
			HttpRequest req = new HttpRequest();
			req.setMethod('POST');   
			req.setTimeout(60000);
			req.setEndpoint('callout:PLRMetadataFetch/services/Soap/u/31.0');
			req.setHeader('Content-Type', 'text/xml;charset=UTF-8');
			req.setHeader('SOAPAction', '""');
            req.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + '{!$Credential.UserName}' + '</username><password>' + '{!$Credential.Password}' + '</password></login></Body></Envelope>');
 
            HttpResponse res = new Http().send(req);
			System.debug('Response code: ' + res.getStatusCode());
            
			if(res.getStatusCode() != 200){
				Dom.Document responseDocument = res.getBodyDocument();
				Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
				Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/'); // soapenv:Body 
				Dom.Xmlnode faultElm = bodyElm.getChildElement('Fault', 'http://schemas.xmlsoap.org/soap/envelope/'); // soapenv:Fault
                System.debug(faultElm);
                return null;
            } else {
                Dom.Document responseDocument = res.getBodyDocument();
                Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
                Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/'); // soapenv:Body 
                Dom.Xmlnode loginResponseElm = bodyElm.getChildElement('loginResponse','urn:partner.soap.sforce.com'); // loginResponse
                Dom.Xmlnode resultElm = loginResponseElm.getChildElement('result','urn:partner.soap.sforce.com'); // result
                Dom.Xmlnode sessionIdElm = resultElm.getChildElement('sessionId', 'urn:partner.soap.sforce.com'); // sessionId
                System.debug('TSYKS+++++'+sessionIdElm.getText());
                return sessionIdElm.getText();
            }
		}
		catch(Exception e) {
			System.debug('Exception: ' + e.getMessage());
			return null;
		}
	}
    public static Boolean checkSobjectColumn(string sobjectName, string columnName) {
		
		Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		if(schemaMap.containsKey(sobjectName)) {
			Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sobjectName).getDescribe().fields.getMap();
            system.debug('SCEHMA:::'+fieldMap.containsKey(columnName));
			return fieldMap.containsKey(columnName);
		}
		else {
			return false;
		}
	}
    	/*
	* This method is to convert string date into DateTime type and return GMt dateTime
	* 
	*/ 
	public static DateTime getGmtTime(string strdt) {
		String dt = strdt.substringbefore('T');
		string tm = strdt.substringBetween('T','Z');
		string[] dts=dt.split('-');
		string[] tms=tm.split(':');
		Date myDate = Date.newInstance(Integer.valueOf(dts[0]),Integer.valueOf(dts[1]), Integer.valueOf(dts[2]));
		Time myTime = Time.newInstance(Integer.valueOf(tms[0]), Integer.valueOf(tms[1]), Integer.valueOf(tms[2].substringbefore('.')), Integer.valueOf(tms[2].substringafter('.')));
		System.debug('DATE:::::::::'+DateTime.newInstancegmt(myDate, myTime));
        return DateTime.newInstancegmt(myDate, myTime);
		
	}
	
}