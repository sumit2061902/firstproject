/**
* This class called from scheduler
* responsible for sending email notification to Sales Rep
* as well as their Managers and so on.
**/
global class ExampleBatchClass implements Database.Batchable<sObject>{
 
     String query = ' SELECT Name, ownerID,Employee_Id__c,employee_Name__c,Accepted_By__c,status__c FROM Sales_Compensation_Plan__c where status__c=\'Pending\'';

                    global ExampleBatchClass(){
                               
                    }
                    global Database.QueryLocator start(Database.BatchableContext BC){
                         /** Return SCPA records for processing  **/
                         return Database.getQueryLocator(query);
                    }
      
                    /** Execute each SCPA records and send emails **/
                    global void execute(Database.BatchableContext BC, List<Sales_Compensation_Plan__c>scope){
                    
                                        System.debug('SCPA records for processing--'+scope.size());
                                        List <String> allEmployeeIds = new List<String>() ;    
                                        /**Extracting Employee Ids for each SCPA  **/
                                        for(Sales_Compensation_Plan__c item : scope){             
                                            allEmployeeIds.add(item.Employee_Id__c);
                                        }
             
                                        System.debug('No of SCPA Employee Ids--'+allEmployeeIds.size());
                                        List<Sym_Employee__c>symEmpList = [Select Name,Employee_ID__C,Manager_Up__c from Sym_Employee__c where Employee_ID__C IN :allEmployeeIds and Manager_Up__c!= null];
                        
                                        System.debug('No of Sym Employee for SCPA EmployeeIds--'+symEmpList.size());
                                        
                                        /**Map for each Sym employee and their managers  **/                            
                                        Map <string,string> empManagers = new Map<string,string>();    
                                        for(Sym_Employee__c s: symEmpList ){
                                            System.debug('Manager Ups---'+s.Manager_Up__c);
                                            empManagers.put(s.Employee_ID__C, s.Manager_Up__c );
                                        }
                            
                                        /**List of all managers names**/
                                        list<string> managerNames = new list<string>();
                                        system.debug('empManagers.values()--'+empManagers.values());
                                        for(string s: empManagers.values()){
                                            managerNames = s.split(',');
                                        }
                        
                                        System.debug('All managers Names--'+managerNames );
                                        /** List of managers details based on their names **/
                                        List<Sym_Employee__c> managerList = new List<Sym_Employee__c>();                        
                                        managerList = [Select Id, email__c,name from Sym_Employee__c where name IN: managerNames];
                        
                                        map<string, string> mapManagers = new map<string, string>();
                                        for(Sym_Employee__c s: managerList ){
                                            System.debug('Manager Name--'+s.name+'---Manage Email--'+s.email__c);
                                            mapManagers.put(s.name, s.email__c);
                                        }
                                        /** Maps for sending no escalation no of levels **/
                                        map<Sales_Compensation_Plan__c, String> SCPAManagerMap1 = new Map<Sales_Compensation_Plan__c, String>();
                                        map<Sales_Compensation_Plan__c, String> SCPAManagerMap2 = new Map<Sales_Compensation_Plan__c, String>();
                                        map<Sales_Compensation_Plan__c, String> SCPAManagerMap3 = new Map<Sales_Compensation_Plan__c, String>();

                                    for(Sales_Compensation_Plan__c s: scope){
                
                                        if(System.today() > s.Accepted_By__c){
                                            List<String> lstManagers = new List<String>();
                                            string managers = empManagers.get(s.Employee_ID__C);
                                            lstManagers = managers.split(',');
                                            System.debug('Manager Size--'+lstManagers.size());
                                            if(lstManagers.size()>=1){
                                                    String m = lstManagers[0];
                                                    System.debug('First Manager--'+m);
                                                    SCPAManagerMap1.put(s, mapManagers.get(m));  
                                                    System.debug('Email of First Manager--'+SCPAManagerMap1.get(s));
                                                    if(SCPAManagerMap1.get(s)!=null){
                                                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                                            email.setToAddresses(new String[] {SCPAManagerMap1.get(s)});
                                                        /** email.setSaveAsActivity(false); 
                                                            email.setTargetObjectId(s.OwnerId);
                                                            email.setTemplateId(00X28000000X4lm);
                                                            email.setWhatId(item.Id); **/
                                                            email.setSaveAsActivity(false); 
                                                            email.setTargetObjectId(s.OwnerId);
                                                            email.setTemplateId('00X28000000r94H');
                                                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                                                      }      
                                             }
                                        
                                        }
                                        if(System.today() > s.Accepted_By__c+1){
                                            List<String> lstManagers = new List<String>();
                                            string managers = empManagers.get(s.Employee_ID__C);
                                            lstManagers = managers.split(',');
                                            if(lstManagers.size()>=2){
                                                    String m = lstManagers[1];
                                                    System.debug('Second Manager--'+m);
                                                    SCPAManagerMap2.put(s, mapManagers.get(m));  
                                                    System.debug('Email of Second Manager--'+SCPAManagerMap2.get(s));
                                                    
                                                    if(SCPAManagerMap2.get(s)!=null){
                                                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                                            email.setToAddresses(new String[] {SCPAManagerMap2.get(s)});
                                                        /** email.setSaveAsActivity(false);
                                                            email.setTargetObjectId(item.OwnerId);
                                                            email.setTemplateId(emailTemplate.Id);
                                                            email.setWhatId(item.Id); **/
                                                            email.setSaveAsActivity(false); 
                                                            email.setTargetObjectId(s.OwnerId);                        
                                                            email.setTemplateId('00X28000000r94H');
                                                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                                                      }
                                           }           
                                        }
                                                                    
                                        if(System.today() > s.Accepted_By__c+2){
                                            List<String> lstManagers = new List<String>();
                                            string managers = empManagers.get(s.Employee_ID__C);
                                            lstManagers = managers.split(',');
                                            if(lstManagers.size() >=3){
                                                String m = lstManagers[2];
                                                System.debug('Third Manager--'+m);
                                                SCPAManagerMap3.put(s, mapManagers.get(m));  
                                                System.debug('Email of Third Manager--'+SCPAManagerMap3.get(s));
                                                 if(SCPAManagerMap3.get(s)!=null){
                                                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                                        email.setToAddresses(new String[] {SCPAManagerMap3.get(s)});
                                                        /**email.setSaveAsActivity(false);
                                                        email.setTargetObjectId(item.OwnerId);
                                                        email.setTemplateId(emailTemplate.Id);
                                                        email.setWhatId(item.Id); **/
                                                        email.setSaveAsActivity(false); 
                                                        email.setTargetObjectId(s.OwnerId);                        
                                                        email.setTemplateId('00X28000000r94H');                        
                                                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                                                  }
                                           }                  
                                        }  
                                       
                                    }
                    }
     
                    global void finish(Database.BatchableContext BC){
                                // Logic to be Executed at finish
                    }
    }