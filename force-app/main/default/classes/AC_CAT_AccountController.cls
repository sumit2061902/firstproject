global with sharing class AC_CAT_AccountController {
    public static map<string,Object> retrieveAccount(string accId,string conId) {
        try {
        List<account> accList = new List<account>([select id,name from account where id=:accId]);
        account acc;
        if(accList.size()>0) {
            acc = accList[0];
        }
        List<contact> conList = new list<contact>([select id,lastname,Birthdate from contact where id=:conId]);
        contact con;
        if(conlist.size()>0) {
            con=conList[0];
        }
        map<string,Object>inputMap = new map<string,Object>();
        if(acc != null){
            inputMap.put('AccountId',acc.Id);
            inputMap.put('Name',acc.Name);
        }
        else
            if(con!= null) {
                datetime bd=con.Birthdate;
                list<string> strlist =new list<string>();
                strList.add('11/11/2011');
                strList.add('11/11/2013');
                strList.add('11/11/2015');
                inputMap.put('ContactName',con.LastName);
                inputMap.put('Birthdate',con.Birthdate);
                inputMap.put('Birthdate',bd.format('dd/MM/yyyy'));
                inputMap.put('Birthdate','11/11/2011');
            }
          return inputMap;
            
        }
        catch(exception ex) {
             System.debug('Error has occured-->'+ex);
            throw ex;
        }
            
    }
}