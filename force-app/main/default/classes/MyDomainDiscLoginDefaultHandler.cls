/*******************************************************
@name: MyDomainDiscLoginDefaultHandler
@CreateDate: 21/05/2019
@Description: This class is used to redirect login flow based on permissions
@Version: 1.0
@reference: 
*******************************************************/
public with sharing class MyDomainDiscLoginDefaultHandler implements Auth.MyDomainLoginDiscoveryHandler {
    public PageReference login(String identifier, String startUrl, Map<String, String> requestAttributes) {
        if (identifier != null) {
            //Query user by Fedaration ID  
            List<User> users = new list<user>([SELECT Id,profile.name,username,FederationIdentifier FROM User WHERE FederationIdentifier = :identifier  AND IsActive = TRUE]);
            
            //Query user by Username and filter with System administrator.
            List<User> SysAdminUsers = new list<user>([SELECT Id,profile.name,Username,FederationIdentifier FROM User WHERE Username = :identifier AND IsActive = TRUE and Profile.Name='System Administrator']);
            
            //If multiple records found with same identifier
            if(users.size()>1 || SysAdminUsers.size()>1) {
                throw new Auth.LoginDiscoveryException('Multiple Users found with Same UserName/FederationIdentifier');
            }
            if(users.size() == 1) {
                if(users[0].profile.name.equals(Label.PIMS_Partner_Profile)) {
                    System.debug('Inside telstra Retail partner permission>>>>>');
                    return getSsoRedirect(Label.PIMS_Partner_Login,users[0], startUrl, requestAttributes); //call sso flow 
                }
                else if(doesRunningUserHavePermission(Label.Telstra_Administrator,users)){
                    System.debug('Inside telstra administrtaor permission>>>>>');
                    return getSsoRedirect(Label.Telstra_RSA_Account_01,users[0], startUrl, requestAttributes); //call sso flow 
                }
                else {
                System.debug('inside else>>>>>>');
                    return getSsoRedirect(Label.Telstra_Account_01,users[0], startUrl, requestAttributes); //call sso flow 
                }
            }
            else if(SysAdminUsers.size()==1) {
                    
                    return normalLogin(SysAdminUsers[0],startUrl, requestAttributes);
                    
            }
            else {
                    throw new Auth.LoginDiscoveryException('No unique user found.'); 
            }  
        }
        throw new Auth.DiscoveryCustomErrorException('Invalid Identifier');
    }
    
   /*******************************************************
    @MethodName: getSsoRedirect
    @Arguments: list,String,map
    @Description: This method is used redirecting specific SSO
    *******************************************************/
    private PageReference getSsoRedirect(String DeveloperName,User user, String startUrl, Map<String, String> requestAttributes) {
        // You can look up if the user should log in with SAML or an Auth Provider and return the URL to initialize SSO. For example:
        SamlSsoConfig SSO = [select Id from SamlSsoConfig where developerName=:developerName limit 1];
        String ssoUrl = Auth.AuthConfiguration.getSamlSsoUrl(requestAttributes.get('MyDomainURL'), startUrl, SSO.Id);
        System.debug('requestAttributes>>>>>'+requestAttributes);
        System.debug('SSO>>>>>'+SSO);
        System.debug('ssoUrl >>>>>'+ssoUrl );
        if(ssoUrl!=null || ssoUrl!='') {
            return new PageReference(ssoUrl);
        }
        return null;
    }
    
    /*******************************************************
    @MethodName: normalLogin
    @Arguments: list,String,map
    @Description: This method is used redirecting to Normal login Flow
    *******************************************************/
    private PageReference normalLogin(User user, String startUrl, Map<String, String> requestAttributes) {
        return Auth.SessionManagement.finishLoginDiscovery(Auth.LoginDiscoveryMethod.password, user.Id);    
    }
    
    /*******************************************************
    @MethodName: doesRunningUserHavePermission
    @Arguments: CustomPermission name and User List
    @Description: This method is used for checking user is having custom permission assigned
    *******************************************************/
    public static Boolean doesRunningUserHavePermission(String CustomPermissionName,list <user>users) {
        Set<Id> accessiblePermissionIds = new Set<Id>();
        for (SetupEntityAccess access : [SELECT SetupEntityId FROM SetupEntityAccess
                           WHERE SetupEntityType = 'CustomPermission' AND ParentId IN (
                             SELECT PermissionSetId FROM PermissionSetAssignment
                             WHERE Assignee.FederationIdentifier =:users[0].FederationIdentifier)])
            
        accessiblePermissionIds.add(access.SetupEntityId);
        return 0 < [SELECT count() FROM CustomPermission WHERE Id IN :accessiblePermissionIds AND DeveloperName =: CustomPermissionName];
    }
}