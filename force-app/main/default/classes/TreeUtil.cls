public class TreeUtil {
    // Global JSON generator
    private static JSONGenerator gen {get; set;}
    
    /* // initialize helper data */ 
    static {
        // initialize helper data for getSObjectTypeById function
        init1();
    }
    private static List<Sym_Employee__C> empList;
    private static map<ID, List<Sym_Employee__C>> empMap;
    private static map<ID,Sym_Employee__C> empMgrMap;
    
     /* // init1 starts <to initialise helper data> */
    private static void init1() {
        
        empList= new List<Sym_Employee__C>([select Id, Name from sym_employee__c]);
        
        empMgrMap = new map<ID,Sym_Employee__C>([select Id, Name,name_empId_grade__c,manager__c from sym_employee__c ]);  
        empMap = new map<ID,List<Sym_Employee__C>>();
       
        for(Sym_Employee__C emp: empMgrMap.values())
        {
            
            if(empMap.containsKey(emp.manager__c))
            {
                 empMap.get(emp.manager__c).add(emp);
            }
            else{
                empMap.put(emp.manager__c,new List<Sym_Employee__C>{emp});
            }
        }   
        
        System.debug('EmpMAP--'+empMap);
        System.debug('EmpMAPKeys--'+empMap.keySet());
        System.debug('EmpMAPValues--'+empMap.values());
 
    }
   
    /* // createNode starts */
    private static EmployeeNodeWrapper createNode(Id objId) {
        
        System.debug('Tree for Emp--'+objId);
        
        EmployeeNodeWrapper n = new EmployeeNodeWrapper();
       // Sym_Employee__C temEmp = [Select Id, Name from sym_employee__c where Id =: objId];
        //Sym_Employee__C temEmp = empMgrMap.get(objId);
         // Setting Up top Manager ID
        Sym_Employee__C temEmp = empMgrMap.get(objId);
        n.empName=temEmp.name_empId_grade__c;
        n.empId=temEmp.Id;      
        n.myChildNodes=new List<EmployeeNodeWrapper>();
        
        //List<Sym_Employee__C> lst= new List<Sym_Employee__C>([select Id, Name from sym_employee__c where manager__c =: objId]);
        List<Sym_Employee__C> lst= new List<Sym_Employee__C>(empMap.get(objId));
        
        for(Sym_Employee__C e :lst){
           EmployeeNodeWrapper cn= createNode(e.Id);
            n.myChildNodes.Add(cn);
        }
            
        return n;
    }
    public static String getTreeJSON(Id empId) {
        gen = JSON.createGenerator(true);
        
       // ID topID = topMgrId(empId);
       // System.debug('Top Id---'+topID);
        
       EmployeeNodeWrapper node = createNode(empId);
       //EmployeeNodeWrapper node = createNode(topID);
        gen.writeStartArray();
            convertNodeToJSON(node);
        gen.writeEndArray();
        return gen.getAsString();
    }
    
    private static void convertNodeToJSON(EmployeeNodeWrapper objRNW){
        gen.writeStartObject();
            gen.writeStringField('title', objRNW.empName);
            gen.writeStringField('key', objRNW.empId);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
          gen.writeBooleanField('isFolder', True);
            //gen.writeStringField('icon', null);
            if (true)
            {
                gen.writeFieldName('children');
                gen.writeStartArray();
                        for (EmployeeNodeWrapper r : objRNW.myChildNodes)
                        {
                            convertNodeToJSON(r);
                        }
                    
                gen.writeEndArray();
            }

        gen.writeEndObject();
    }
    
      public class EmployeeNodeWrapper {
    
        // Role info properties - begin
        public String empName {get; set;}
        
        public Id empId {get; set;}
        
        public String managerID {get; set;}
        
        // Node children identifier properties - begin
        public Boolean hasChildren {get; set;}
        
        public Boolean isLeafNode {get; set;}
                        
        public List<EmployeeNodeWrapper> myChildNodes {get; set;}
        // Node children properties - end   
        
        public EmployeeNodeWrapper(){
            hasChildren = false;
        }
    }

 public static String topMgrId(ID objId){
                
                String TempId = objId;
     
                System.debug('empMgrMap.size()--'+empMgrMap.size());    
                for(Integer i= 0; i<=empMgrMap.size();i++){             
                
                            for(ID Id:empMgrMap.keySet())
                                {
                                    System.debug('Id--'+Id +' objId--  '+objId);
                                    if(Id==objId && empMgrMap.get(Id).manager__c!=null)
                                    {
                                        objId = empMgrMap.get(Id).manager__c ;
                                        break;
                                    }   
                                }
                }
             return objId ;
            }
}