public without sharing class PaginationWithFilter
{   
    public String soql;
         
    public String nam {get;set;}
    public Integer age1 {get;set;}         
    public Integer age2 {get;set;}
    public String option {get;set;}
    public Boolean age2bool {get;set;}   
    
    public Boolean fetchCalled;
   
    public integer totalRecs {get;set;}
    private integer index = 0;
    private integer blockSize = 5;       
   
    public PaginationWithFilter()
    {
        age2bool = false;   
        totalRecs = [select count() from Member__c];        
        fetchCalled = false;
    }  
   
    public void fetch()
    {          
        soql = 'SELECT Name, Age__c FROM Member__c';
        if(option == '>')
        {
        soql += ' WHERE Age__c > ' + String.ValueOf(age1);
        }
        else if(option == '<')
        {
        soql += ' WHERE Age__c < ' + String.ValueOf(age1);
        }
        else if(option == '=')
        {
        soql += ' WHERE Age__c = ' + String.ValueOf(age1); 
        }
        else
        {
        soql += ' WHERE Age__c > ' + String.ValueOf(age1) + ' AND ' + 'Age__c < ' + String.ValueOf(age2);
        }
        if(nam != '')
        {
        soql += ' AND Name LIKE  \'%' + nam + '%\'';
        }       
        
        List<Member__c> tempMemb = Database.Query(soql);
        totalRecs = tempMemb.size();   
        
       // soql += ' LIMIT ' + blockSize + ' OFFSET ' + index;  
        
        fetchCalled = true;                         
    }        
    
    public List<Member__c> getMemb()
    {       
        List<Member__c> membs; 
        if(fetchCalled)
        {
            fetch();
        }   
        else
        {
            soql = 'SELECT Name, Age__c FROM Member__c LIMIT : blockSize OFFSET : index';
        }           
        membs = Database.Query(soql);        
        System.debug('Values are ' + membs);
        return membs;        
    }    
   
    public void dispAge2()
    {
        if(option == 'between')
        age2bool = true;
        else
        age2bool = false;
    }                  
   
    public void beginning()
    {
        index = 0;
    }
  
    public void previous()
    {
        index = index - blockSize;
    }
  
    public void next()
    {
        index = index + blockSize;
    }

    public void end()
    {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }      
  
    public boolean getprev()
    {
        if(index == 0)
        return true;
        else
        return false;
    }
  
    public boolean getnxt()
    {
        if((index + blockSize) >= totalRecs)
        return true;
        else
        return false;
    }     
}