/**********************************
Created by:sumit Bhagat
Date:5/9/2017
Purpose:assign Lead Owner as a queue based on company Size
**********************************/

public class Leadhandler {
    
     private static boolean run = true;
    public void LeadQueueAssignment(list<lead>LeadNew,list<lead>Leadold,Map<id,Lead>NewMap,Map<id,Lead>OldMap){
        set<id>SalesterrIds=new set<id>();
        for(Lead l:leadNew){  
            SalesterrIds.add(l.Sales_Territory__c);  
        }
        System.debug('SalesterrIds>>>'+SalesterrIds);
        Map<id,Sales_Territory__c> SalesTerrMap=new Map<id,Sales_Territory__c>([select name,Queue__c from Sales_Territory__c where
                                                                                id in:SalesterrIds]);
        List<String>QueueList=new List<String>();
        for(Sales_Territory__c st:SalesTerrMap.values())
        {
            // string Qu=st.Queue__c;
            //qu=Qu.replace('_', ' ');
            //System.debug('Qu<<>>'+qu);
            QueueList.add(st.Queue__c);
        }
        System.debug('QueueList>>>>'+QueueList);
        System.debug('SalesTerrMap>>>'+SalesTerrMap);
        if(SalesTerrMap.size()>0){
            Map<id,group>Queuemap=new Map<id,group>([select Id,name from Group where Type ='Queue'and Name in:QueueList ]) ;
            Map<string,id>qMap=new map<string,id>();
            for(group g:Queuemap.values()){
                qMap.put(g.Name,g.Id);
            }
            System.debug('Queuemap>>>>'+Queuemap);
            for(lead l:Leadnew){
                if(l.Company_size__c>=1000){
                    for(Sales_Territory__c sa:SalesTerrMap.values()){
                        System.debug('l.Sales_Territory__c>>'+l.Sales_Territory__c);
                        if(l.Sales_Territory__c.equals(SalesTerrMap.get(l.Sales_Territory__c).id)){
                            System.debug('inside If>>>'+salesterrmap.get(l.Sales_Territory__c).id);
                            l.OwnerId=qmap.get(sa.queue__c);
                            break;
                        }
                    } 
                }
                else
                {
                    l.OwnerId=Label.SMB;
                }
            }
        } 
    }
    
    public static void UpdateLeadStatus(List<lead>leadList){
  if(run){
                run = false;
        List<lead> NewLead=new list<lead>();
        for(lead l:leadList){
            if(l.status__c=='Open'){
                NewLead.add(l);
            }
        }
        System.debug('NewLead>>>>>>>'+NewLead);
        list<lead>lstleadupdate=new list<lead>();
     Map<id,list<task>> taskMap=new Map<id,List<task>>();
        for(task t:[select id,subject,whoid,whatid from task where whoid in:Newlead]){
            if(!taskmap.containsKey(t.whoid)){
                taskmap.put(t.whoid,new list<task>{t});
            }
            else{
                taskmap.get(t.whoid).add(t);
                    }
        }
        
        for(id Leadid:taskmap.keySet()){
            if(taskmap.get(leadid).size()>5){
              lstleadupdate.add(new lead(id=leadid,Description='More that 15 tasks are assigned'));  
            }
            
        }
        update lstleadupdate;
  } 
    }
}