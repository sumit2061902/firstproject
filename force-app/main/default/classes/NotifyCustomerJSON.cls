public class NotifyCustomerJSON {

    public cls_account Account;
    
    public class cls_account {
        
        public string SendDateTime;
        public string ABN;
        public string CustomerId;
        public string CIDN;
        public string cscrmExternalID;
        public string AccountUUID;
        public string FirstName;
        public string LastName;
        public string Phone;
        public string fax ;
        public string email;
        public string birthDate;
        public string Title;
        public string Type ;
        public string ActualRetiredDate;
        public string BusinessUnitCode;
        public string BusinessUniqueKey;
        public string ConcessionAuthority;
        public string ConcessionNumber;
        public string ConcessionType;
        public string AustralianDriversLicenseNumber;
        public string AustralianDriversLicenseIssueState;
        public string website;
        public string LanguagePreference;
        public string ManagedIndicator;
        public string CustomerType;
        public string MarketSegmentCode;
        public string PreferredCommunicationMode;
        public string RetiredUserId;
        public string RetirementReason;
        public string ServicingOrganisationalUnit;
        public string PriorityAssist;
        public string Createddate;
        public string LastUpdatedDate;
        public string AccountNumber;
        public string RevenueOrganisationalUnit;
        public string onlineFlag;
        public string CustomerPotentialRevenue;
        public string GeographicLocationCode;
        public string SourceSystem;
        public string PaymentType;
        public string PaymentStatus;
        public string ValidConcessionFLag;
        public string LastConcessionValidatedDate;
        public string BypassConcessionValidation;
        public string ConcessionValidationResultOverride;
        public string ConcessionValidationCustomerConsent;
        public string ConcessionValidationConsentRequestDate;
        public string TradingAsNames;
        public string EffectiveRetiredDate;
        public string Sic;
        public string MergeStatus;//Added by Mounika for US:SCSE-723

        public cls_AccountAddressRelationship[] AccountAddressRelationship;
     }   
     
  Public class cls_AccountAddressRelationship {
        public String name;    //
        public String Status;
        public String cscrmAddressType;
        public String cscrmBuildingname;
        public String cscrmCountry;
        public String StreetNumberEndSuffix;
        public String cscrmLotNumberStart;
        public String cscrmExternalAddressId;
        public String cscrmZipPostalCode;
        public String cscrmPrivateStreetName;
        public String PrivateStreetType;
        public String cscrmIsPrimary;
        public String cscrmPropertyName;
        public String cscrmPlanNumber;
        public String StreetNumberStartSuffix;
        public String cscrmStateProvince;
        public String cscrmStreetNumberEnd;
        public String cscrmStreetNumberStart;
        public String cscrmStreetType;
        public String StreetTypeSuffix;
        public String cscrmBuildingNumber;
        public String cscrmBuildingType;
        public String cscrmLevelNumber;
        public String cscrmLevelType;
        public String cscrmLotNumberEnd;
        public String cscrmDescriptor;
        public String cscrmInternationalPostcode;
        public String cscrmInternationalState;
        public String cscrmCity;
        public String cscrmStreetName;
        public String AddressLine1;
        public String AddressLine2;
        public String AddressLine3;
        public String cscrmDPId; 
        public String cscrmExchange;

    }   
       
    public static NotifyCustomerJSON parse(String json){
        return (NotifyCustomerJSON) System.JSON.deserialize(json, NotifyCustomerJSON.class);
    }

}