public class TestPageController 
{
public TestPageController(ApexPages.StandardController controller)
{

}
public TestPageController(ApexPages.StandardSetController controller)
{

}
    public String getObjType() 
    {
        return String.valueOf(Account.sObjectType);
    }
    
    public RoleUtil.RoleNodeWrapper getRootNodeOfTree(Id roleOrUserId) 
    {
        return RoleUtil.getRootNodeOfUserTree(roleOrUserId);
    }

    public String getJsonString() 
    {
        String str = null;
        str = RoleUtil.getTreeJSON('00E90000000pMaP');
        return str; 
    }
    
 
    public String selectedValues {get; set;}
    
   public void saveRecord()
    {
       system.debug('selectedValues---------'+selectedValues);     
       List<String>Ids = selectedValues.split(',');
       List<Sym_employee__c>lstEmp = new List<Sym_employee__c>([select name,manager__c,Employee_ID__c from Sym_employee__c where ID in:Ids]);
       
        Set<String>empId = new Set<String>();
        for(Sym_employee__c eid:lstEmp ){
            empId.add(eid.Employee_ID__c); 
        }
        
        
        List<Assignment_and_Responce__c> empExistingRecords = new List<Assignment_and_Responce__c>();
        empExistingRecords  = [select Id, Name,Employee_ID__c from Assignment_and_Responce__c where Employee_ID__c IN :empId] ;
        
        System.debug('Selected Sym Employee--'+ lstEmp.size());      
        System.debug('Existing Assignment Records'+ empExistingRecords.size());      
        
       
       list<Assignment_and_Responce__c>lstAssignment = new list<Assignment_and_Responce__c>();
       
       for(Sym_employee__c sym:lstEmp){       
          boolean found = false ;
         for(Assignment_and_Responce__c responseId:empExistingRecords){ 
                
                  if((sym.Employee_ID__c).equals(responseId.Employee_ID__c))
                   {
                       System.debug('Already Existing Employee--'+sym.name);
                       found = true;
                   }
                   
          } 
           if(!found)
           {
                   System.debug('Employee Not avaialable--'+sym.name);
                   Assignment_and_Responce__c assignmeny = new Assignment_and_Responce__c();
                   assignmeny.name = sym.name;
                   assignmeny.Employee_ID__c=sym.Employee_ID__c;
                   assignmeny.ID__c=sym.Employee_ID__c;           
                   lstAssignment.add(assignmeny);

           }
           
       }
       
       insert lstAssignment;
       ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Records are inserted successfully.');
            ApexPages.addMessage(pageMsg);
    }
    
   /*  
    public String getJsonString() {
        String str = null;
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartArray();
            gen.writeStartObject();
                gen.writeStringField('title', 'Item1');
                gen.writeStringField('key', 'I1');
            gen.writeEndObject();
            //{title: "Folder 2", isFolder: true, key: "F2",
            gen.writeStartObject();
                gen.writeStringField('title', 'Folder 2');
                gen.writeStringField('key', 'F 2');
                gen.writeBooleanField('isFolder', true);
                gen.writeFieldName('children');
                gen.writeStartArray();
                    //{title: "Sub-item 2.1", key: "I2.1"},
                    gen.writeStartObject();
                        gen.writeStringField('title', 'Sub Item 2.1');
                        gen.writeStringField('key', 'I 2.1');
                    gen.writeEndObject();
                    gen.writeStartObject();
                        gen.writeStringField('title', 'Sub Item 2.2');
                        gen.writeStringField('key', 'I 2.2');
                    gen.writeEndObject();
                gen.writeEndArray();
            gen.writeEndObject();
            //{title: "Item 3"}
            gen.writeStartObject();
                gen.writeStringField('title', 'Item 3');
                gen.writeStringField('key', 'I 3');
            gen.writeEndObject();
        gen.writeEndArray();
        str = gen.getAsString();
        return str; 
    }
*/  
}