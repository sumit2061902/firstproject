public class ObjectOutputq {

  public List<ObjectWrapper> getObjects(){

    List<ObjectWrapper> objList = new List<ObjectWrapper>();

        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

    for(Schema.SObjectType objType : globalDescribe.values()) { 

        // Describe each object and walk through each field
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

        ObjectWrapper thisObj = new ObjectWrapper();

        thisObj.objApiName = objDescribe.getName();
        thisObj.objPrefix = objDescribe.getKeyPrefix();
        thisObj.objLabel = objDescribe.getLabel();

        objList.add(thisObj);
    }
    return objList;
  }

  public class ObjectWrapper {

    public String objPrefix {get;set;}
    public String objApiName {get;set;}
    public String objLabel {get;set;}

  }

}