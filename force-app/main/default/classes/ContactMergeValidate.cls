public class ContactMergeValidate {
    public static string batchname;
    public static set<string> validateMergeRecords(List<merge_request__c>ContactmergeRecords){
        set<string > srcnm=new set<string>();
        map<string,contact> tcmid=new map<string,contact>();
        list<merge_request__c>mergeList=new list<merge_request__c>();
        list<merge_request__c>updatemergelist=new list<merge_request__c>();
        for(merge_request__c cm:ContactmergeRecords){
            batchname=cm.batch_name__c;
            srcnm.add(cm.source_number__C);
            srcnm.add(cm.target_number__c);
            mergeList.add(cm);
        }
        
        for(contact con:[select id,tcmUUID__c,Contact_Status__c,Merge_Status__c from contact where tcmUUID__c in:srcnm]){
            tcmid.put(con.tcmUUID__C,con);
        }
        system.debug('tcmid Keyset:::'+tcmid.keyset());
        system.debug('tcmid values:::'+tcmid.values());
        if(tcmid.size()>0){
            
        for(merge_request__c mr:mergeList){
            
      
            
            if(string.isBlank(mr.Source_Number__c)){
                mr.SFDC_Status__c='MERGE_REQ_SFDC_STATUS_FAILED_VALIDATION';
                mr.SFDC_Error_Code__c='501';
                mr.SFDC_Error__c='SOURCE_CONTACT_BLANK_OR_NULL';
                updatemergelist.add(mr);
            }
            else if(string.isBlank(mr.Target_Number__c)){
                mr.SFDC_Status__c='MERGE_REQ_SFDC_STATUS_FAILED_VALIDATION';
                mr.SFDC_Error_Code__c='502';
                mr.SFDC_Error__c='SOURCE_CONTACT_BLANK_OR_NULL';
                updatemergelist.add(mr);
            }
            else if(tcmid.containskey(mr.Source_Number__c)){
                mr.SFDC_Status__c='MERGE_REQ_SFDC_STATUS_FAILED_VALIDATION';
                mr.SFDC_Error_Code__c='501';
                mr.SFDC_Error__c='SOURCE_CONTACT_BLANK_OR_NULL'; 
                updatemergelist.add(mr);
            }
            else if(tcmid.containskey(mr.Target_Number__c)){
                mr.SFDC_Status__c='MERGE_REQ_SFDC_STATUS_FAILED_VALIDATION';
                mr.SFDC_Error_Code__c='502';
                mr.SFDC_Error__c='SOURCE_CONTACT_BLANK_OR_NULL';
                updatemergelist.add(mr);
            }
            else if(mr.Source_Number__c!=null && tcmid.get(mr.Source_Number__c).Merge_Status__c!='Merged'){
                mr.SFDC_Status__c='MERGE_REQ_SFDC_STATUS_FAILED_VALIDATION';
                mr.SFDC_Error_Code__c='';
                mr.SFDC_Error__c='SFDC_ERROR_E2E_MERGE_NOT_COMPLETE_IN_TCM_AND_ASSOCIATED_SYSTEMS';
                updatemergelist.add(mr);
            }
            else{
                mr.SFDC_Status__c='Validated';
                mr.SFDC_Error_Code__c='';
                mr.SFDC_Error__c='';
                updatemergelist.add(mr);
            }
        }
        if(!updatemergelist.isempty()){
            update updatemergelist;
        }
        
        }
        return srcnm;
    }
}