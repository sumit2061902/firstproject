public with sharing class CaseContactController {
    public static boolean hasExecuted = false;
    public static boolean isExecuting;
    public static integer batchSize;
    public CaseContactController( boolean TisExecuting,integer TbatchSize) {
        isExecuting=TisExecuting;
        batchSize=TbatchSize;
        
    }
    public void onBeforeInsert(Case [] oCase) {
        System.debug('isExecuting>>>>'+isExecuting);
        System.debug('batchSize>>>>'+batchSize);
        for(case cs:oCase) {
            if(string.isNotBlank(cs.Nominated_Contact_Emai__c)){
                cs.SuppliedEmail=cs.Nominated_Contact_Emai__c;
            }
            if(string.isNotBlank(cs.Nominated_Contact_Name__c)) {
                cs.SuppliedName=cs.Nominated_Contact_Name__c;
            }
            if(string.isNotBlank(cs.Nominated_Contact_Phone__c)) {
                cs.SuppliedPhone=cs.Nominated_Contact_Phone__c;
            }
        }
    }
    public void onBeforeUpdate(map<id,case>caseMap,map<id,case>oldCaseMap) {
        OnAccConRel(caseMap.values());
        map<id,task> taskMap=new map<id,task>();
        list<task> taskList=new list<task>([select id,WhatId  from task where IsClosed=:false and  whatid in:caseMap.keyset() ]);
        for(task tsk:taskList) {
            taskMap.put(tsk.whatid,tsk);
        }
        for(case cs:caseMap.values()) {
            case oldcase=oldCaseMap.get(cs.id);
            Boolean isOldMapCaseClosed = oldCaseMap.get(cs.Id).IsClosed;  
            
            if((taskMap.containsKey(cs.Id) && cs.Status=='Closed') && !isOldMapCaseClosed ){
                cs.adderror('This case is having open task.Hence you cannot close this closed.'); 
            }
        }
        
    }
    
    public void OnAccConRel(case [] caseList) {
        set<string>AccIds=new set<string>();
        set<string>ConIds=new set<string>();
        set<string>accConStr=new Set<string>();
        map<string,date>acrEndDate=new map<string,date>();
        for(case c:caseList) {
            if(string.isNotBlank(c.contactId) && string.isNotBlank(c.accountId)) {
                AccIds.add(c.accountId);
                ConIds.add(c.ContactId);
            }
        }
        for(AccountContactRelation acr:[select id,enddate,AccountId,contactId from AccountContactRelation where contactid in:ConIds and accountid in:AccIds]) {
            accConStr.add(string.valueOf(acr.accountId)+ string.valueOf(acr.contactId));
            
        }
        
        list<contact>conlist=[select id,vlocity_cmt_Status__c from contact where id in:conIds];
        map<id,string>conmap=new map<id,string>();
        for(contact con:conlist) {
            conmap.put(con.id,con.vlocity_cmt_Status__c);
        }
        
        for(case c:caseList) {
            string temp=string.valueOf(c.accountid)+string.valueOf(c.contactid);
            if(accConStr.contains(temp)) {
                c.addError('It is having refernece in acc Relatonship');
            }
            if(conmap.get(c.contactid)!='Active') {
                c.addError('COntact Cont be a now');
            }
        }
        
    }
    public void addCaseComment(case [] caseList) {
        Set<Id> accountIds = new Set<Id>();
        Map<id,id>mapCaseIds=new map<id,id>();
        map<id,map<string,string>> valuesByAccountIdMap=new map<id,map<string,string>>();
        for (case cs:caseList) {
            accountIds.add(cs.accountId);
            mapCaseIds.put(cs.AccountId,cs.id);
            valuesByAccountIdMap.put(cs.accountId,new map<string,string>());
        }
        System.debug('accountIds'+accountIds);
        System.debug('mapCaseIds'+mapCaseIds);
        System.debug('valuesByAccountIdMap'+valuesByAccountIdMap);
        for(account acnt:[select id,Name,(select id,CaseNumber from cases where type='Electrical') from account where id in :accountIds]) {
            
            integer count=0;
            List<String> caseNumList = new List<String>();
            for (case cs:acnt.cases) {
                caseNumList.add(cs.caseNumber);
                count++;
            }
            string openCaseCount='0';
            String recOpencasCaseNumber = '0';
            if(count > 0) {
                opencaseCount = String.valueof(count-1);
                recOpencasCaseNumber = count >=2 ? caseNumList.get(count-2) : '0';
                valuesByAccountIdMap.get(acnt.id).put('AccountName',acnt.Name);
                valuesByAccountIdMap.get(acnt.Id).put('openCaseCount',opencaseCount);
                
            }
            
        }
        casecomment[] csemnt=new List<CaseComment>();
        for(id acids:mapCaseIds.keyset()) {
            //id accountid=acids;
            id accountid=mapCaseIds.get(acids);
            System.debug('accountid'+accountid);
            integer opencases=integer.valueOf(valuesByAccountIdMap.get(acids).get('openCaseCount'));
            String accname=valuesByAccountIdMap.get(acids).get('AccountName');
            string msg;
            if(opencases >0) {
                 msg='Hi There are ' +' '+opencases+' '+'cases are open'+'With Account'+' '+accname;
            }else 
                msg='No open case for this account';
            casecomment cs=new casecomment(ParentId=accountid,commentbody=msg);
            csemnt.add(cs);
        }
        system.debug('csemnt>>>>>'+csemnt);
        insert csemnt;
    }
}