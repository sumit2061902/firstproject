global class CustomerDetailsCntlr {
 
    @AuraEnabled
    public static List<Account> getAccountData(String cac)
    {
        String comLgnUser = UserInfo.getUserName();
		String idCAC = getCACid(comLgnUser);
        List<Account> Act = new List<Account>();
        if(idCAC != null){
       	  Act=[select Name,Id,Phone,Type,Accountnumber,vlocity_cmt_Status__c from Account where id= :idCAC];
        }
        return Act;
    }
    @AuraEnabled
    public static List<contact> getListofContacts(String cac)
    {
        String comLgnUser = UserInfo.getUserName();
		String idCAC = getCACid(comLgnUser);
       List<contact> cnts = new List<contact>();  
         if(idCAC != null){
         cnts=[Select FirstName,LastName,Email,Phone from Contact where AccountId= :idCAC]; 
         }
        return cnts;
    }
    
    @AuraEnabled
    public static contact saveContact(Contact con)
    {
        contact cont = con;
     
        try{
            update cont;
           
        }catch(exception e){
            System.debug('The following error has occured: ' +e.getMessage());
            
        }
        return cont;
    }
	 @AuraEnabled
    global static Id getCACid(String username) {
		Id idCAC = null;
       List<Contact> contcs =( [select accountid from contact where id=:'00328000001qxJsAAI']);
	   if(contcs.size() > 0)
		{
			idCAC = contcs[0].AccountId;
		}
        system.debug('ID: '+idCAC);
        return idCAC;
    }
}