public class EventRequestJSON {

    public String correlationId;
    //public String resourceURI; removed as part of request format change - @SCSE 1101 - sprint 45
    public String timestamp;
    public cls_Data data;
        public class cls_Data {
        public String customerOrderReference;
        public String status;
        public cls_Data(String customerOrderReference, String status, boolean eomsEvent, string submittedAckTimeStamp){
            this.customerOrderReference = customerOrderReference;
            if(!eomsEvent){
                this.status=status;
                }
            }     
        }
   /* public cls_error[] error;
    public class cls_error {
        public String code;
        public String message;
        
    }*/
    public string createrequestjson(String correlationId,/* String resourceURI,*/ String timestamp, String customerOrderReference, String status, boolean eomsEvent, string submittedAckTimeStamp/*, String code, String message*/){
        EventRequestJSON eventreq=  new EventRequestJSON();
        eventreq.correlationId=correlationId;
        //eventreq.resourceURI=resourceURI;
        eventreq.timestamp=timestamp;  
        eventreq.data=new EventRequestJSON.cls_Data(customerOrderReference, status, eomsEvent, submittedAckTimeStamp);
       /* cls_error[] errors = new cls_error[] {};
        cls_error err = new cls_error();
        err.code=code;
        err.message=message;
        errors.add(err);        
        eventreq.error =errors;*/
        return Json.serialize(eventreq);
    }
}