public class Opporder {
    public static Boolean isFirstTime = true;
    Public static void OrderItem(list<order>ordlist){
        set<id>oppids=new set<id>();
        List<Order> orderList = new List<Order>();
    for(order ord:ordlist){
        oppids.add(ord.accountid);
        
    }
    
    Map<id,opportunity> oppLineItem=new Map<id,opportunity>([select id,(SELECT Description,Id,ListPrice,Name,OpportunityId,Product2Id,ProductCode,Quantity,TotalPrice,UnitPrice FROM OpportunityLineItems) from opportunity WHERE account.Id IN :oppids]);
	System.debug('oppLineItem::::'+oppLineItem);
    List<OrderItem> orderItemsForInsert = new List<OrderItem>();
    for(Order o : ordlist)
        {
            // For each order get the related opportunity and line items, loop through the line items and add a new order line item to the order line item list for each matching opportunity line item
            Opportunity oppWithLineItem = oppLineItem.get(o.Opportunity__c);
            for(OpportunityLineItem oli : oppWithLineItem.OpportunityLineItems)
            {
                orderItemsForInsert.add(new OrderItem(quantity=2,UnitPrice=200,pricebookentryid='01u28000007nphIAAQ',product2id ='01t28000004l4TBAAY',OrderId=o.Id));
            }
        }
        // If we have order line items, insert data
        if(orderItemsForInsert.size() > 0)
        {
            insert orderItemsForInsert;
        }
    }
}