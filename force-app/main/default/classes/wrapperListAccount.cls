public class wrapperListAccount 
{
    public list<wrapperAcc> wrappAccList{get;set;}
    public list<Account> getAccList = new list<Account>();
    public list<Contact> conList{get;set;}
    public boolean selectedCheck{get;set;}
    
  public wrapperListAccount()
    {   selectedCheck = false;
        wrappAccList = new list<wrapperAcc>();
        for(Account aa : [SELECT id,name,phone from Account WHERE name != null])
        {
            system.debug('aa---'+aa);
            wrappAccList.add(new wrapperAcc(false,aa));
            system.debug('wrappAccList---'+wrappAccList);
        }
    }
    
    public pagereference displayAcc()
    {    
        selectedCheck = true;
        conList = new list<Contact>();
        for(wrapperAcc aaa : wrappAccList)
        {
             system.debug('aaa---'+aaa);
            if(aaa.checked == true)
                getAccList.add(aaa.acc);
             system.debug('getAccList---'+getAccList);
              system.debug('@@@@@@@@@@@@@@@'+aaa.acc.name);
        }

        for(Contact c : [SELECT id,lastname,firstname,accountid,account.name from contact WHERE accountid IN: getAccList])
        {

            conList.add(c);
             system.debug('conList---'+conList);
        }
        
        return null;
    }
    
public class wrapperAcc
{
    public Account acc{get;set;}
    public Boolean checked{get;set;}
    
    Public wrapperAcc(Boolean bool,Account aa)
    {
        acc = aa;
        checked = bool;
    }
}
}