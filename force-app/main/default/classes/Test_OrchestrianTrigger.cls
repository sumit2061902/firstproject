@istest
public class Test_OrchestrianTrigger {
    
    public testmethod static  void  TestOrc(){
        Parent_Orchestration_Process__c pn=new Parent_Orchestration_Process__c(name='testO');
        insert pn;
        List<CSPOFA_Orchestration_Process__c> cslist=new list<CSPOFA_Orchestration_Process__c>();
        CSPOFA_Orchestration_Process__c cf;
        cf=new CSPOFA_Orchestration_Process__c(name='test1p',CSPOFA_Process_Type__c='New provide',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
        cf=new CSPOFA_Orchestration_Process__c(name='test1p1',CSPOFA_Process_Type__c='New provide',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
        cf=new CSPOFA_Orchestration_Process__c(name='test1p2',CSPOFA_Process_Type__c='New provide',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
       test.startTest();
        insert cslist;
        update cslist;
        test.stopTest();
        system.assertEquals(cslist.size(),3);
    }
    public testmethod static  void  TestOrcneg(){
        Parent_Orchestration_Process__c pn=new Parent_Orchestration_Process__c(name='testO');
        insert pn;
        List<CSPOFA_Orchestration_Process__c> cslist=new list<CSPOFA_Orchestration_Process__c>();
        CSPOFA_Orchestration_Process__c cf;
        cf=new CSPOFA_Orchestration_Process__c(name='test1p',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
        cf=new CSPOFA_Orchestration_Process__c(name='test1p1',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
        cf=new CSPOFA_Orchestration_Process__c(name='test1p2',Parent_Orchestration_Process__c=pn.Id);
        cslist.add(cf);
        insert cslist;
        update cslist;
        
    }
}