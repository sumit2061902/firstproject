public with sharing class  ProductCountryChangeController {
	public void OnAfterInsert(list <Product_Gap__c> newRecords)
	{
    	updateprimaryCountry(newRecords);
	}
    	public void OnAfterUpdate(list<Product_Gap__c> oldrecords,list <Product_Gap__c> updaterecords,map<id,Product_Gap__c> newmap,map<id,Product_Gap__c> oldmap)
	{
    	updateprimaryCountry(updaterecords);
	}
    public void updateprimaryCountry(list <Product_Gap__c> newRecords){
      map<id,string>  prdmap=new map<id,string>();	
        for(product_gap__c prd:newRecords) {
            if(prd.primary__c) {
                prdmap.put(prd.Implementations__c,prd.Purchase_country__c);
            }
        }
            list <implementation__c> imp=new list<implementation__c>([select id,primary_country__c from implementation__c 
                                                                    where id in:prdmap.keySet()]);
        for(implementation__c im:imp) {
            im.primary_country__c=prdmap.get(im.Id);
        }
        update imp;
    }
}