public with sharing class wrapCustomObj {
public Integer limitSize=5;
public Integer OffsetSize=0;
public Integer totalRecs=0;
public List<employee__c> selectedEmp;


public List<employee__c> getSelectedEmp()
{
    selectedEmp=new List<employee__c>();
    if(selectedEmp.size()>0)
    return selectedEmp;
    else
    return null;
}
public void lPage() {
   OffsetSize=totalrecs-math.mod(totalRecs,limitSize);

}
public void fPage() {
    OffsetSize=0;
}


public void next() {
    OffsetSize=OffsetSize+limitSize;

}


public void previous() {
     OffsetSize=OffsetSize-limitSize;
}


public PageReference DisplayDetails() {

    PageReference reRend = new PageReference('/apex/wrapperCustomObjpage2');
    reRend.setRedirect(false);
    return reRend;

}

public List<wrapObj> wrpList{get;set;}
public List<wrapObj> selectedwrpList{get;set;}

public List<wrapObj> getEmpDetails() {
    wrpList=new List<wrapObj>();
    List<Employee__c> emps=[select id,name from employee__c  limit:limitSize Offset:OffsetSize];
    totalRecs=[select count() from Employee__c];
    for(Employee__c e:emps)
    {
        wrpList.add(new wrapObj(e,false));
    }
    return wrpList;
}
public List<wrapObj> getSelectEmpDetails()
{
    selectedwrpList=new List<wrapObj>();
    for(wrapObj w:getEmpDetails())
    {
        if(w.selected==true)
        selectedwrpList.add(w);
    }
    return selectedwrpList;
}
public class wrapObj
{
    public Employee__c empDet{get;set;}
    public Boolean selected{get;set;}
    public wrapObj(Employee__c emp,boolean check)
    {
        empDet=emp;
        selected=check;
    }
}

}