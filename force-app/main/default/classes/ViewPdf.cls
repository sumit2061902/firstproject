public class ViewPdf {
public ViewPdf(ApexPages.StandardController controlle)
{
}
    public document att {
        get {
            if (att == null) {
                att = [SELECT Body, ContentType, Name FROM Document WHERE ID = '01528000000YWVD'];
            }
            return att;
        }
        private set;
    }
    public String pdf {
        get {
            return EncodingUtil.Base64Encode(att.body);
        }
    }
}