@istest
public class Test_UpdateStatusCsOrder {

    @testsetup
    static void createtestData(){
        cs_order__c csord=new cs_order__c(name='test',Fullfilment_Status__c='Completed');
        insert csord;
        cs_order_line_item__c csordlineItem=new cs_order_line_item__c(name='Test',cs_order__c=csord.id,billing_status__c='provisioned');
    	insert csordlineItem;
        //csord.Fullfilment_Status__c='provisioned';
        //update csord;
    }
    public static testmethod void AssertHelp1(){
        test.startTest();
        List<cs_order__c> csdord=new list<cs_order__c>([select id ,Fullfilment_Status__c from cs_order__c]);
        for(cs_order__c cs: csdord){
            cs.Fullfilment_Status__c='Provisioned';
        }
        update csdord;
        cs_order__c sub = [select Id, name,Cs_Order2__c from cs_order__c limit 1];
        system.assert(sub.Cs_Order2__c=='Completed');
    }  
    
}