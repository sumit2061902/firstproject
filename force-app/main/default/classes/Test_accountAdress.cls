@istest
public class Test_accountAdress {
    
    static testMethod void testShareTerritories() {
        List<user> users=new list<user>();
        for(integer i=0;i<20;i++){
            user u=new user();
            u.firstname='Elsa';
            u.lastname='Kalin';
            u.Email='elsa'+i+'@calin.com';
            u.alias='eCalin';
            u.username='elsaCalin'+i+'@disny.com';
             u.LocaleSidKey      = 'en_US';
      u.TimeZoneSidKey    = 'GMT';
      u.ProfileID         = '00e28000000XL3l';
      u.LanguageLocaleKey = 'en_US';
      u.EmailEncodingKey  = 'UTF-8';
            users.add(u);
        }
        insert users;
        
        list<territory__C>terrs=new list<territory__c>();
        for(integer i=0;i<200;i++){
            territory__c t=new territory__c();
            t.Name='Test'+i;
            t.zip__c=string.valueOf(1000+i);
            terrs.add(t);
        }
        insert terrs;
        List<Territory_Members__c> tms = 
        new List<Territory_Members__c>();
    for (Integer i = 0; i < 200; i++) {
      // Here we "cast", aka transform the Double into an Integer
      Integer count = Integer.valueOf(Math.random() * 20);
      for (Integer j = 0; j < count; j++) {
        Territory_Members__c tm = new Territory_Members__c();
        tm.Territory__c        = terrs[i].Id;
        tm.User__c             = users[j].Id;
        tms.add(tm);
      }
    }
    insert tms;
        List<Account> accs = new List<Account>();
    for (Integer i = 0; i < 200; i++) {
      Account a           = new Account();
      a.Name              = 'Disney'; 
        a.SLA__c='212';
        a.Active__c='Yes';
        a.SLASerialNumber__c='2121';
      String zip          = String.valueOf(1000 + i);
      a.BillingPostalCode = zip;
      accs.add(a);
    }
    insert accs;
            accs = [SELECT Id, BillingPostalCode,
           (SELECT Id, AccountId,
                   AccountAccessLevel,
                   OpportunityAccessLevel,
                   UserOrGroupId
              FROM Shares
             WHERE RowCause = 'Manual')
              FROM Account];
        Map<String, Territory__c> terrMap = 
        new Map<String, Territory__c>();                        
    terrs = [SELECT Id, Zip__c,
            (SELECT Id, User__c
               FROM Territory_Members__r)
               FROM Territory__c];
    for (Territory__c t : terrs) {
      terrMap.put(t.Zip__c, t);
    }  
        for (Account a : accs) {
      Territory__c t      = terrMap.get(a.BillingPostalCode);
      Integer shareCount  = a.Shares.size();
      Integer memberCount = t.Territory_Members__r.size();
      System.assertEquals(shareCount, memberCount);
    }               
    
    }


    public static testmethod void  CheckAdd(){
        list<account>acclist=new list<account>();
        account acc=new account();
        acc.Name='TestName';
        acc.SLA__c='Gold';
        acc.SLASerialNumber__c='122';
        acc.Active__c='Yes';
        test.startTest();
        //insert acc;
        acclist.add(acc);
        insert acclist;
         contact c=new contact();
        c.LastName='testcontact';
        c.AccountId=acc.id;
        insert c;
        acc.BillingCity='Pune';
        acc.BillingCountry='India';
        acc.BillingStreet='Dalci road';
       update acclist;
       test.stopTest();
      Accountaddress.updateaddress(acclist);
       string address=[select mailingcity from contact where id =:c.id limit 1].mailingcity;
        System.debug('address>>>'+address+c.MailingCity	);
        system.assertEquals(address,acc.BillingCity);
        
    }
}