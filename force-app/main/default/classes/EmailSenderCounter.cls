global class EmailSenderCounter implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
String query = ' SELECT Name, GeoGraphy__c,ownerID,Employee_Id__c  ,Due_Date_Counter__c,Reminder_Notification__c,employee_Name__c, employee_Name__r.name,Accepted_By__c, status__c FROM Sales_Compensation_Plan__c where status__c=\'Pending\'';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Sales_Compensation_Plan__c> scope)
    {
    
         for(Sales_Compensation_Plan__c sc : scope)
         {  system.debug( 'sc.Due_Date_Counter__c before --->'+sc.Due_Date_Counter__c);
         system.debug('date-->'+date.today());
               if(math.MOD(Date.today().daysBetween(sc.Accepted_By__c),7) ==0) 
                   {
             sc.Due_Date_Counter__c= sc.Due_Date_Counter__c+1;      
             system.debug( ' sc.Due_Date_Counter__c after--->'+sc.Due_Date_Counter__c);
                 }      
         }
         update scope;
         system.debug('scope-->'+scope);
    }   
    global void finish(Database.BatchableContext BC)
    {
    }
}