public class SelectOptionExample {

   // String[] countries = new String[]{};
list<string>countries=new list<string>();
    public PageReference test() {
        return null;
    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('US','US'));
        options.add(new SelectOption('CANADA','Canada'));
        options.add(new SelectOption('MEXICO','Mexico'));
        return options;
    }

    public list<string> getCountries() {
        return countries;
    }

    public void setCountries(list<string> countries) {
        this.countries = countries;
    }
    

}