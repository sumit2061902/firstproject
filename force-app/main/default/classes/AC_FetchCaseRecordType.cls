/*
Class: AC_FetchCaseRecordType
Description: This controller class is used in AC_CaseRecordTypeSelector lightning component
Created By: 
*/
public with sharing class AC_FetchCaseRecordType {

    /* This function will fetch the available RecordTypes of
     * provided object and will return a map of
     * recordTypeId and recordTypeNames
     * it excludes 'master' record type*/
    @AuraEnabled
    public static Map<Id, String> fetchRecordTypeValues() {
        
        Map<Id, String> recordTypeMap = new Map<Id, String>();
        for(RecordTypeInfo info: Case.SObjectType.getDescribe().getRecordTypeInfos()) {
            if(info.isAvailable()) {
                if(info.getName() != 'Master')
                recordTypeMap.put(info.getRecordTypeId(), info.getName());
            }
        }
        return recordTypeMap;  
    }
}