public with sharing class TestTreeView
{
    //Wrapper class to contain the parent nodes and their child nodes
    public class WCTreeView
    {   
        public List<contact> Child {get; set;}
        Public Account Parent {get;set;}
         
        public WCTreeView(Account objAC, List<contact> objCList)
        {
            Child = objCList;
            Parent = objAC;
        }
    }
 
    Public List<wctreeview> TreeView;
     
    Public List<wctreeview> GetNodes()
    {
        TreeView = new List<wctreeview>();
        List<account> objACList = [Select Id, Name from Account];
        for (Integer i = 0; i < objACList.size(); i++)
        {
            List<contact> objCList = [Select Id, FirstName, LastName from Contact where AccountId = :objACList[i].Id];
            TreeView.add(new WCTreeView(objACList[i], objCList));
        }  
        return TreeView;
    }  
}