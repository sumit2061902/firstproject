public class AccountEvalRel {
    public void evalRelWithAccount(List<Account> acclists){
         map<id,account> accountmap=new map<id,account>();
    list<string> accNameList=new list<string>();
    for(account acc:acclists){
        if(acc.Active__c=='yes'){
            accountmap.put(acc.id,acc);
            System.debug('accountmap>>>'+accountmap);
            accNameList.add(acc.name);
        }
    }
    list<Eval__c> evalInsert=new list<eval__c>();
    Map<string,eval__c> evalmap=new map<string,eval__c>();
    list<eval__C> evalist=new list<eval__c>([select name,OwnerId from eval__c] );
    System.debug('evalist>>>'+evalist);
    if(evalist.size()>0){
        for(account acc:accountmap.values()){
            integer count=0;
            for(eval__c ev:evalist){
                if(acc.name==ev.name){
                    evalmap.put(ev.name,ev);
                    System.debug('evalist>>>'+evalist);
                    count++;
                }
                
                acc.Status_Days__c=count;
                if(acc.Status_Days__c==0){
                    acc.Description='No same name in EVAL object';
                    
                }
                else if(count>=1)
                    acc.Description='Same name in EVAL object found';
            }
            
        }
        for(account acc:accountmap.values()){
            if(acc.Status_Days__c==0){
                eval__c ev=new eval__c(name=acc.Name,account__c=acc.id);
                evalInsert.add(ev);
                
            }
        }
        insert evalInsert;
        list<id> userlist=new list<id>();
        list<eval__c> evalList=new list<eval__c>();
        System.debug('evalMap>>>'+evalmap);
        for(account ac:accountmap.values()){
            for(eval__c ev:evalmap.values()){
                if(evalmap.containsKey(ac.name)){
                    System.debug('Insinde If of Eval>>>'+evalmap);
                    userlist.add(ev.OwnerId);
                    ev.Account__c=ac.id;
                    evalList.add(ev) ;
                }
            }
        }
        update evalList;
        
        map<id,user> userMap=new map<id,user>([select name from user where id in :userlist]);
        
        for(account ac:accountmap.values()){
            for(eval__c ev:evalist){
                if(ac.name ==ev.name ){
                    ac.Account_Manager__c=usermap.get(ev.OwnerId).id;
                }
            }
        }
    }
        
    }
}