public class UpdateEmployee {
    public static void UpdateEmp(list<sev_form__c>sevList,list<sev_form__c>sevListOld){
        set<string> grpList=new set<string>();
        for(sev_Form__C sf: sevList){
            if(sf.Group__c!=null){
                grpList.add(sf.Group__c);
            }
        }
        for(sev_Form__C sf: sevListOld){
            if(sf.Group__c!=null){
                grpList.add(sf.Group__c);
            }
        }
        if(grpList.size()>0){
            map<id,Group_Severance__C> GsMap=new Map<id,Group_Severance__C>([select Total_employees_in_Group__c from Group_Severance__C where id in :grplist]);
            for(Group_Severance__C gp : gsmap.values()){
                gsMap.get(gp.Id);
                gsMap.put(gp.id,gp);
            }
            Map<id,integer>gpcount=new Map<id,integer>();
            List<Group_Severance__C > gpUpdateList=new List<Group_Severance__C >();
            for(aggregateresult agr :[select count(id) numsev,group__c gp from sev_form__c where group__c in:grpList group by group__C])
            {
                gpcount.put((Id)agr.get('gp'),(integer)agr.get('numsev')); 
            }
            System.debug('gpcount>>>'+gpcount);
            if(gpcount.keyset().size()>0){
                for(id gid:gpcount.keyset()){
                    gpUpdateList.add(new Group_Severance__C(id=gid,Total_employees_in_Group__c=gpcount.get(gid)));
                    
                }
            }
            if(gpUpdateList.size()>0){
                  System.debug('gpUpdateList>>>'+gpUpdateList);
                update gpUpdateList;
            }
        }
    }
}