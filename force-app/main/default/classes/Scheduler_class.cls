global class Scheduler_class implements Schedulable{
    global void execute (SchedulableContext SC){
       SCAPEmailAlert  b1 = new  SCAPEmailAlert ();
       database.executeBatch(b1);
      // string sch = '0 0 0 1 4 ?';
       string sch = '0 1 1 * * ?';
       system.schedule ('Batch', sch, new Scheduler_class());
    }
}