global class NotifyContactJson{
    public cls_Contact Contact;
    global class cls_Contact {
        public String ContactUUID;  //
        public String SendDateTime; //
        public String cscrmExternalID;  //
        public String Salutation ;
          public String Title ; //
        public String Firstname;    //
        public String MiddleName;   //
        public String Lastname; //
        public String BirthDate;    //
        public String Email;    //
        public String HomePhone;    //
        public String MobilePhone;  //
        public String OtherPhone;   //
        public String Phone;    //
        public String WorkEmail;    //
    }
    public static NotifyContactJson parse(String json){
        return (NotifyContactJson) System.JSON.deserialize(json, NotifyContactJson.class);
    }
}