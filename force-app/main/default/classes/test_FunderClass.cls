/*********************************************************
 Class-FunderClass
 Created by-SfdcDemo
 Description-
 *********************************************************/    
@istest
public class test_FunderClass {
   
  //Covering the Positive scenario
    public static testmethod void Funderpos(){
        
        list<account>acclist=new list<account>();
        for(integer i=1;i<=10;i++){
            account acc=new account(name='Test'+i,
                                    AnnualRevenue=2121+i,
                                    Active__c='Yes',
                                    SLASerialNumber__c='200000',
                                    SLA__c='Gold');
            acclist.add(acc);
        }
        test.startTest();
        insert acclist;
        test.stopTest();
        list<funder__c> funlist=new list<funder__c>([select name,id,account__c from funder__c]);
        System.assertEquals(acclist.size(), funlist.size());
    }
    
    //Covering the Negative scenario
    public static testmethod void FunderNeg(){
        account acc=new account(name='Test',
                                AnnualRevenue=2121,
                                Active__c='Yes',
                                SLASerialNumber__c='200000',
                                SLA__c='Gold');
        insert acc;
        acc.name='Testupdated';
        list<funder__c> funlist=new list<funder__c>([select name,id,account__c from funder__c]);
        System.assertEquals(1, funlist.size());
        
    }
}