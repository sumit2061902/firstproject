public class EventRequestJSON_EOMSEvent {

    public String correlationId;
    public String timestamp;
    public cls_Data data;
        public class cls_Data {
        public String customerOrderReference;
        public String submittedAckTimeStamp;
        public cls_Data(String customerOrderReference, String status, boolean eomsEvent, string submittedAckTimeStamp){
            this.customerOrderReference = customerOrderReference;
                if(eomsEvent){
                    this.submittedAckTimeStamp=submittedAckTimeStamp;
                }
            }     
        }

    public string createrequestjson_EOMSEvent(String correlationId, String timestamp, String customerOrderReference, String status, boolean eomsEvent, string submittedAckTimeStamp){
        EventRequestJSON_EOMSEvent eventreq=  new EventRequestJSON_EOMSEvent();
        eventreq.correlationId=correlationId;
        eventreq.timestamp=timestamp;  
        eventreq.data=new EventRequestJSON_EOMSEvent.cls_Data(customerOrderReference, status, eomsEvent, submittedAckTimeStamp);
        return Json.serialize(eventreq);
    }
}