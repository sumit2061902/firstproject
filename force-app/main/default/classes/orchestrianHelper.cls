public class orchestrianHelper {

public static void rollUpCancellation(List<CSPOFA_Orchestration_Process__c> oldrecords,
			  Map<Id,CSPOFA_Orchestration_Process__C>OldMap,
			  List<CSPOFA_Orchestration_Process__c>newrecords,
			  Map<Id,CSPOFA_Orchestration_Process__C>NewMap){
				  
				  List<CSPOFA_Orchestration_Process__C> orchestrianList=new list<CSPOFA_Orchestration_Process__C>(); 
				  List<Id>parentList=new list<Id>();
				  for(CSPOFA_Orchestration_Process__c cop:oldrecords){
					  if(cop.CSPOFA_Process_Type__c!=null && cop.Parent_Orchestration_Process__c!=null){
						  orchestrianList.add(cop);
						  parentList.add(cop.Parent_Orchestration_Process__c);
						  
					  }
				  }
				  System.debug('parentList'+parentList);
				  if(parentlist.isEmpty()){
					  return ;
				  }
				  
				  List<CSPOFA_Orchestration_Process__C> children=[select Parent_Orchestration_Process__c,CSPOFA_Process_Type__c,State__c,Status__c from 
																  CSPOFA_Orchestration_Process__C where Parent_Orchestration_Process__r.id in:parentlist];                      
				  Map<string,List<CSPOFA_Orchestration_Process__C>> childmap=new map<string,List<CSPOFA_Orchestration_Process__C>>();   
				  System.debug('children'+children);
				  for(CSPOFA_Orchestration_Process__C cs:children){
					  if(childmap.containsKey(cs.Parent_Orchestration_Process__c)){
						  list<CSPOFA_Orchestration_Process__C> childlist=childmap.get(cs.Parent_Orchestration_Process__c);
						  childlist.add(cs);
						  System.debug('childlistget'+childlist);
					  }
					  else
					  {
						  list<CSPOFA_Orchestration_Process__C> childlist=new list<CSPOFA_Orchestration_Process__C>();
						  childlist.add(cs);
						  childmap.put(cs.Parent_Orchestration_Process__c,childlist);
					  }
				  }
                  System.debug('childmap'+childmap);
				  
			  }
}