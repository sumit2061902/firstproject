@isTest
public class OddEvenController_test {
    
    public static testmethod void CheckNum(){
        
        
        OddEvenController oec=new OddEvenController();
        oec.input=21;
        
        oec.checkNumber();
        system.assertEquals('Number is Even','Number is Even');
        oec.input=22;
        oec.checkNumber();
        system.assertEquals('number is Odd','number is Odd');
        PageReference pageRef = Page.oddevenidentifier; 
        Test.setCurrentPage(pageRef);
        oec.regester();
        
    }
}