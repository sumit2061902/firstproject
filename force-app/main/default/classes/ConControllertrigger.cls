public class ConControllertrigger {
   @TestVisible private static boolean firstRun = true;
    public static boolean isFirstRun() {
        return firstRun;
    }
    public static void setFirstRunFalse(){
        firstRun = false;
    }
    public void ContactDeletePer(list<contact>oldcontactlist,Map<id,contact>oldcontactMap){
        System.debug('oldcontactlist>>>>>'+oldcontactlist);
        System.debug('oldcontactMap>>>>>'+oldcontactMap);
        if(oldcontactMap.values()!=null){
            for(profile prof:[select id,profile.name from profile where name='System Administrator']){
                for(contact con:oldcontactlist){
                    try{
                        string uid=userinfo.getprofileId();
                        if(uid==prof.id){
                            con.adderror('<span color="red"><b>You cannot delete this contact dude</b></span>');
                        }
                    }
                    catch(exception e){
                        
                    }
                }
            }
        }
        
    }
    
    public void ValidatePrimarycontacts(List<Contact> contacts){
        Map<id,integer>Primaryconsize=new map<id,integer>();
        Map<Id,Contact> conPrimaryMap = new Map<Id,Contact>();
        Set<id>accountids=new set<id>();
        Integer duperrorcount = 0;
        for(contact con:contacts){
            if(con.primary_contact__c==true){
                if(!Primaryconsize.containskey(con.accountid)){
                    Primaryconsize.put(con.AccountId,1);
                }
 
                else
                {
                    integer count=Primaryconsize.get(con.AccountId);
                    count=count+1;
                    Primaryconsize.put(con.AccountId,count);
                }
            }
        }
        for(contact con:contacts){
            if(Primaryconsize.get(con.accountid)>1){
                con.addError('There are Duplicate Primary Contacts in the List Provided for this Account');
                duperrorcount++;
            }
            else
            {
                if(Primaryconsize.get(con.accountid)==1){
                    conPrimaryMap.put(con.accountid,con); 
                }
            }
        }
        for(id ids:conPrimaryMap.keySet()){
            accountids.add(ids);
        }
        list<contact>allContact=[select id,name,AccountId,primary_contact__c from contact where accountid in:accountids];
        for(contact con:allContact){
            if(con.Primary_Contact__c==true){
                if(conPrimaryMap.get(con.AccountId)!=null){
                    if(conPrimaryMap.get(con.AccountId).id!=con.id){
                        conPrimaryMap.get(con.AccountId).adderror('Primary contact is already available with this account');
                    }
                }  
            }
        } 
    }
    
    Public void BillProfile(List<contact>conList,map<id,contact>conOldMap){
        System.debug('Inside Method>>>>>');
        List<Id> conLst = new List<Id>(); 
        List<BillProfile__c> billList = new List<BillProfile__c>();
        List<contact>cons=new list<contact>();
        for(contact con:conList){
            conLst.add(con.id);
            if((con.isOppUpdate__c==true && conOldMap.get(con.id).isOppUpdate__c==true) &&
               (con.Sent_to_Tibco__c==true && conOldMap.get(con.Id).Sent_to_Tibco__c==true)){
                   contact conss= new contact(Is_updated__c=true,id=con.Id);
                   cons.add(conss);
               }
        }
        System.debug('cons>>>><<<>>'+cons);
 
        cons.clear();
        Map<id,BillProfile__c>	BillMap=new Map<id,BillProfile__c>([Select id,Company__c,Status__c,Contact__c from BillProfile__c where Contact__c IN: conLst]);
        for(BillProfile__c bp:billMap.values()){
            BillMap.put(bp.Contact__c,bp);
        }
        for(contact con:conlist){
            if(billmap.containskey(con.id)){
                contact conss= new contact(Description ='Got the Bill Profile as per the Attached documents',id=con.Id);
                cons.add(conss);
            }
            else{
                contact conss=new contact(Description ='No Bill profile is available for the same',id=con.Id);
                cons.add(conss);
            } 
        } 
        System.debug('cons>>>><<<>>'+cons);
      
        billList = [select Id, Contact__c, company__c, Status__c from BillProfile__c where Contact__c in :conLst ];
        for(contact con:conList){
            for(billprofile__c bp:billList){
                if(BillMap.get(con.id).Company__c == Null && BillMap.get(con.id).Status__c == 'Active')
                {
                    con.adderror('Bill Profile associated with contact dont have company and status');
                }
            }
        }
    }  
    public void populateManager(List<contact>conList){
        
        Set<id>UserIds=new set<id>();
        for(contact con:conList){
            userIds.add(con.ownerid);
        } 
        Map<id,user>UsrMap=new Map<id,User>([select id,Name,ManagerId,Manager.name,Manager.Manager.name from user where id in:Userids]);
        for(contact con:conList){
            //con.Regional_Sales_Manager__c=UsrMap.get(con.OwnerId).Manager.name;
            //con.Sales_Director_Name__c=usrMap.get(con.OwnerId).Manager.Manager.Name;
            
            if(con.Country_ISO_Code__c=='US' && con.Postal_Code__c!=null){
                if(!pattern.matches('[0-9]{5} [0-9]{4}', con.Postal_Code__c)){
                    con.Postal_Code__c.adderror('Postal Code should be in correct format(ex:-123456789 || 12345-1234');
                }
            }
        }
    }
}