/*************************************************************************************************************
    Name : DuplicateUserValidationController 
    Project : Safe harbor
    Custom Setting : CustomerSupport__c
    Purpose : This is the controller class for trigger 'DuplicateUserValidation'. It checks for User's Email for all 
                active Internal Users and prevents duplicate user records. However, certain users are allowed to by-pass 
                this rule which are under the associated custom setting 'CustomerSupport__c'. 
                Error message is stored under the custom setting : EmailErrorMessage__c 
    
**************************************************************************************************************/
    

public class DuplicateUserValidationController{
    
    public void checkEmailLimitOnInsert(List<User> newUserList){
    List<User> userList =  new List<User>();
        System.debug('newUserList--'+newUserList);
    List<String> userEmailList =  new List<String>();
    Map<String,Integer> userCountMap =  new Map<String,Integer>();
    for(User user : newUserList)
        userEmailList.add(user.email);
        
    System.debug('userEmailList--'+userEmailList);    
    userList =  [Select id, email from User where email in : userEmailList];
        
    //userList.addAll(newUserList);
        System.debug('userList--'+userList);
        
    for(User user : userList) {
        integer temp = 0;
        if(! userCountMap.isEmpty() && userCountMap.get(user.Email) != null ) {
            temp = userCountMap.get(user.Email);
            temp++;
            userCountMap.put(user.Email, temp);
        }
        else {
            temp++;
            userCountMap.put(user.Email, temp);
        }
            
    }
	System.debug('userCountMap--'+userCountMap);        
    for(User u:newUserList) {      

        //UserEmccount=[select count() from user where email =:u.email];
        
        if((CustomerSupport__c.getInstance(u.email) != null && userCountMap.get(u.Email) >= CustomerSupport__c.getInstance(u.email).UserCount__c) || (CustomerSupport__c.getInstance(u.email)== null && userCountMap.get(u.Email) > 1) )     

            u.adderror('User Email Already exist.You cannot insert user');
        
     }
    }
    
    public void checkEmailLimitOnUpdate(List<User> newUserList){
    List<User> userList =  new List<User>();
    List<String> userEmailList =  new List<String>();
    Map<String,Integer> userCountMap =  new Map<String,Integer>();
    for(User user : newUserList)
        userEmailList.add(user.email);
        
    userList =  [Select id, email from User where email in : userEmailList];
    for(User user : userList) {
        integer temp = 0;
        if(! userCountMap.isEmpty() && userCountMap.get(user.Email) != null ) {
            temp = userCountMap.get(user.Email);
            temp++;
            userCountMap.put(user.Email, temp);
        }
        else {
            temp++;
            userCountMap.put(user.Email, temp);
        }
            
    }
    for(User u:newUserList) {      

        //UserEmccount=[select count() from user where email =:u.email];
        
        if((CustomerSupport__c.getInstance(u.email) != null && userCountMap.get(u.Email) >= CustomerSupport__c.getInstance(u.email).UserCount__c) || (CustomerSupport__c.getInstance(u.email)== null && userCountMap.get(u.Email) >= 1) )     

            u.adderror('User Email Already exist.You cannot insert user');
        
     }
    }
    
   
}