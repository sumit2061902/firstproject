public class LeadTriggerHandler {
    
    public static void onBeforeInsert(list<Lead> leadlist){
        SetOriginalOwner(leadlist);
        setupPrefferedMethod(leadList);
        setCustomerAccountOnLead(leadlist,null);
        
    }
    
    public static void SetOriginalOwner(list<lead> leaddata){
        for(lead l:leaddata) {
            l.OriginalOwner__c=l.ownerid;
        }       
    }
    
    public static void setupPrefferedMethod(list<lead> leaddata){
        for(lead leadrecord:leaddata)  {
            if(leadrecord.LeadType__c=='EAI'){
                leadrecord.PreferredContactMethod__c='Email';
            }
            else if (leadrecord.phone!=null && leadrecord.phone.length()>2 && leadrecord.phone.substring(0,2).contains('04')){
                leadrecord.PreferredContactMethod__c='SMS';
            }
            else if(leadrecord.Email!=''){
                leadrecord.PreferredContactMethod__c='Email';
                
            }
            else
                leadrecord.PreferredContactMethod__c=null;
        }
    }
    
    public static void setCustomerAccountOnLead(list<lead>leadData,map<id,lead>oldLeadMap){
        set<id>leadCustomerIdSet=new set<id>();
        
        for(lead l:leadData){
            if(l.customer_id__c!=null && l.customer_id__c!=oldLeadMap.get(l.Id).customer_id__c){ 
                leadCustomerIdSet.add(l.customer_id__c);
            }
        }
        
        list<account>acclist=new list<account>([select id,customer_id__c from account where customer_id__c  in:leadCustomerIdSet]);
        map<id,account> accmap=new map<id,account> ();
        
        for(account ac:acclist){
            accmap.put(ac.customer_Id__c,ac);
        }
        for(lead l:leaddata){
            if(accmap.containskey(l.customer_id__c)){
                l.CustomerAccount__c=accmap.get(l.customer_id__c).id;
            }           
        }
    }
    
    public static void setOwner(list<lead>leaddata){
        for(lead l:leaddata){
            if(isperson(l.OwnerId)){
                l.State='MH';
            }
            }	
    }
    public static boolean isperson(Id ownerid){
        if(string.valueOf(ownerid).startswith('005')){
            return true;
        }
        return false;
    }
}