@isTest
public class AccountServiceTest {
    static testMethod void testGetAllAccounts(){
        List<Account> accounts = new List<Account>();
        for(Integer i=0;i < 10;i++){
            accounts.add(new Account(Name = 'Test Acc'));
        }
        insert accounts;
        
        Test.startTest();
        List<Account> accountsFromService = AccountService.getAllAccounts();
        Test.stopTest();
        
        System.assertEquals(10, accountsFromService.size(), 'There should be 10 accounts');
    }
    
    static testMethod void testGetOpportunitiesOnAccount(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        List<Opportunity> opportunities = new List<Opportunity>();
        for(Integer i=0;i < 10;i++){
            opportunities.add(new Opportunity(
                Name = 'Test Opp', 
                AccountId = acc.Id,
                StageName = 'Closed Won',
                CloseDate = Date.today()
            ));
        }
        insert opportunities;
        
        Test.startTest();
        List<Opportunity> opportunitiesFromService = AccountService.getOpportunitiesOnAccount(acc.Id);
        Test.stopTest();
        
        System.assertEquals(10, opportunitiesFromService.size(), 'There are 10 opportunities');
    }
    
    static testMethod void updateAccountName(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Test.startTest();
        AccountService.updateAccountName(acc, 'Updated Account');
        Test.stopTest();
        
        Account accFromDb = [SELECT Id, Name FROM Account WHERE Id = :acc.Id];
        System.assertEquals('Updated Account', accFromDb.Name, 'The account name has been updated');
    }
}