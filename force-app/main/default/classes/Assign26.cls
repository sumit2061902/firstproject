public class Assign26
{ 
public List<Opportunity> opportunityRecords{get;set;}
public List<opportunityLineItem> opportunityproductRecords{get;set;}
//public String oppId = ApexPages.currentPage().getParameters().get('Id');
public Assign26(ApexPages.StandardController controller) 
{ 
opportunityRecords = new List<Opportunity>();

opportunityRecords = [select name,accountId,Account.name,Type,StageName from Opportunity ];
opportunityProductRecords = new List<opportunityLineItem>();
opportunityProductRecords =[Select ServiceDate, discount, ListPrice,ProductCode, Quantity,UnitPrice, Subtotal,TotalPrice from opportunityLineItem ];
} 
}