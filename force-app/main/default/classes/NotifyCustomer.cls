@restResource(urlmapping='/NotifyCustomerSdata/*')
global without sharing class NotifyCustomer 
{
    static String requiredFields = '';    
    static Boolean validateFlag = false;
    static Savepoint sp; 
    static String correlationId;
    @HttpPost
    global static void upsertCustomerRecords(){
        
        RestRequest req;
        RestResponse res;
        String sendDate = ''; 
        String customerId ='';
        String operation; 
        DateTime sendDateTime=null; 
        String jsonResponse = '';
        // String correlationId;
        Integer retryCount;
        String mStatus='' ;
        String  updExternalId='';
        String isExternalId ='';
        String corelId = '';
        //String trklogID='';
        String isSourceSystem='';
        String sresponsevent='';
        List<Account> accList; 
        
        try{
            Account accountRec=new account();
            req=restcontext.request;
            res=restcontext.response;
            res.addHeader('CONTENT_TYPE', 'APPLICATION_JSON');
            
            System.debug('req:::'+req);
            System.debug('res:::'+res);
            
            string jsonVal=req.requestBody.tostring();
            NotifyCusJSON obj=NotifyCusJSON.parse(jsonval);
            System.debug('jsonVal:::'+jsonVal);
            System.debug('obj:::'+obj);
       
                 
                NotifyCusJSON.cls_account ncJson = new NotifyCusJSON.cls_account();
                ncJson = obj.Account;  
                System.debug('ncJson '+ncJson);
                customerId = ncJson.CustomerId;
                isExternalId = ncJson.cscrmExternalID;

                
                accList = [select id, Name,Customer_Id__c,Send_Date_Time__c,Title__c,Firstname__c,Lastname__c,Birthdate__c,Phone,Fax,Email__c,Website,
                           Preferred_Communication_Mode__c,Merge_Status__c,csm_External_ID__c
                           from Account where Customer_ID__c =: customerId LIMIT 1]; 
                
                try{
                    if(accList!=null && accList.size()>0)
                    {
                        accountRec = accList.get(0);
                        operation = 'Update'; 
                        mapAccountJSON(accountRec,ncJson);
                        // added as part of SCSE1384 to check when the Merge Status is "Merged" donot accept any updates 
                    }
                    else
                    {
                        accountRec = new Account();  
                        operation = NBNConstants.INSERTOperation; 
                        mapAccountJSON(accountRec,ncJson);
                    }
                    If(operation == NBNConstants.INSERTOperation){
                        
                        insert accountRec;  
                    }
                    else
                    { 
                        update accountRec; 
                    }
                }
                catch (Exception e) {                     
                    
                }        
        }
            catch (Exception e) {                     
                    
                }
    }

    public static void mapAccountJSON(Account accRec2, NotifyCusJSON.cls_account csReq2) {
        System.debug('Calling method :::::');
        accRec2.Name = csReq2.firstName + NBNConstants.VALUE_BLANK + csReq2.lastname;
        accRec2.Customer_Id__c = csReq2.CustomerId; 
        accRec2.Title__c  = csReq2.title;  
        accRec2.Firstname__c = csReq2.firstName;             
        accRec2.csm_External_ID__c = csReq2.cscrmExternalID;     
        accRec2.Phone  = csReq2.Phone; 
        accRec2.Fax  = csReq2.fax; 
        //System.debug('Email'+csReq2.email);
        accRec2.Email__c = csReq2.email; 
        accRec2.Website= csReq2.website;  
        //accRec2.TCMUUID__c = csReq2.cscrmExternalID;//Replaced cscrm__External_ID__c with TCMUUID__c 
        
    }
    
}