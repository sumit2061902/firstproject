/**********************************************************
Created by: Accenture
Created Date: 08/18/2015
Last Modified Date: 08/21/2015
Description:SAL-420: AM - Link Primary Admin from IM object to OE
/**********************************************************/
public class ImplementationTriggerHandler1 {
    public void handleAfterInsertEvent(List<Implementation__c> iList) {
        Set<ID> accId = new Set<ID>();
        for(Implementation__c i: iList) {
            accId.add(i.Account__c);
            system.debug('set accid----'+accId);
        }
        
        List<Open_Enrollment__c > OEList = new List<Open_Enrollment__c>([Select ID,Account__c,Primary_Admin__c from Open_Enrollment__c where Account__c IN: accID ]);
        system.debug('List OEList ----'+OEList);
        if(!OEList.isEmpty()) {
            Map<ID, List<Open_Enrollment__c >> AccMap = new Map<ID, List<Open_Enrollment__c >>();
            for(Open_Enrollment__c oe: OEList) {
                AccMap.put(oe.Account__c, OEList);
                system.debug('Map AccMap----'+AccMap);
            }
            for(Implementation__c i: iList) {
                if(i.Primary_Admin__c != null) {
                    List<Open_Enrollment__c> oe = AccMap.get(i.Account__c);
                     system.debug('List 2 oe ----'+oe);
                    for(Open_Enrollment__c oenroll: oe) {
                        oenroll.Primary_Admin__c = i.Primary_Admin__c;
                    }                   
                    AccMap.put(i.Account__c,oe);
                    system.debug('Map2 AccMap ----'+AccMap);
                }
            }
            update OEList;
        }          
    }
    public void handleAfterUpdateEvent(List<Implementation__c> iListNew) {
        handleAfterInsertEvent(iListNew);
    }
}