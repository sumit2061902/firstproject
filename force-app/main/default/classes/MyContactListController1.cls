public class MyContactListController1 {
@AuraEnabled
public static List<Contact> getContacts1() {
    return [Select Id, Name, Email, Title, Phone From Contact];
}
}