global class batchToDeleteRecords implements Database.Batchable<sobject>{
    global String Query;
    global string finalstr;
    global string header;
    global  string recordString;
    global database.QueryLocator start(database.BatchableContext BC){
        Query='Select id,name,CreatedDate,LastModifiedDate FROM Account where createddate<LAST_N_WEEKS:152';
        system.debug('Query1----'+Query);
        return database.getQueryLocator(Query);
    }
    global 	void execute(database.BatchableContext BC,list<Account> acclist){
        header = 'Record Id, Name , Created Date, Modified Date \n';
        System.debug('header:::::'+header);
        finalstr = header ;
        System.debug('finalstr:::::'+finalstr);
        for(Account a: acclist)
        {
            recordString = '"'+a.id+'","'+a.Name+'","'+a.CreatedDate+'","'+a.LastModifiedDate +'"\n';
            finalstr = finalstr +recordString;
            System.debug('finalstr>>>>>'+finalstr);
            
        }
        delete acclist; 
    }
    global void finish(database.BatchableContext BC){
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        System.debug('csvBlob>>>>>'+csvBlob);
        string csvname= 'Account.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<string> {'sumit.bhagat01@infosys.com'};
            String subject ='Account CSV';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setPlainTextBody('Account CSV ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }    
    
}