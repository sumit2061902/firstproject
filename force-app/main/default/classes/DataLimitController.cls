global class DataLimitController{
    String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm(); 
    global  void execute() {
        //Make sure your Salesforce instance URL is added in remote site settings
       // String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm(); 
        String restAPIURL = sfdcURL + '/services/data/v29.0/limits';  
        
        HttpRequest httpRequest = new HttpRequest();  
        httpRequest.setMethod('GET');   
        httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());        
        httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID()); 
        httpRequest.setEndpoint(restAPIURL);  
        String response = '';
        try {  
            Http http = new Http();   
            HttpResponse httpResponse = http.send(httpRequest);  
            if (httpResponse.getStatusCode() == 200 ) {  
                Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(httpResponse.getBody());
                Map<String, Object>  m2=(Map<String, Object>)m.get('DataStorageMB');
                Integer max = Integer.valueOf(m2.get('Max'));
                integer remaining = Integer.valueOf(m2.get('Remaining'));
                Integer Percent =(1-(remaining/max))*100;
                System.debug('Percent--->'+percent);
                if(Percent>=integer.valueof(System.label.StoragePercentage))
                 sendEmail(Percent);
                //DeleteRecords.DeleteRecords();
            } else {  
                System.debug(' httpResponse ' + httpResponse.getBody() );  
                throw new CalloutException( httpResponse.getBody() );  
            }   
        } catch( System.Exception e) {  
            System.debug('ERROR: '+ e);  
            throw e;  
        } 
        
        //System.debug(' ** response ** : ' + values );  
    }
    public void sendEmail(integer percent) {
        string  email=[select email from user where profile.name='system administrator' limit 1].email;
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

        message.toAddresses = new String[]  {System.Label.StorageLimitNotifierEmail} ;
            message.ccaddresses =new string[]{email};
            message.subject = 'Data Limit Notification for Org'+''+sfdcURL;
           String body= 'Org is about to exceed the limit'+sfdcurl;
       body+= 'Current Percent of data used:'+percent;
        System.debug('body---->'+body);
        message.setHtmlBody(body);
		
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }   
}