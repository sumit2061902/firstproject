public class TaskTriggerHandler {
    //This method is used to restricte New Task 
    public static void restrictTaskCreation (List<task>taskList) {
        set<id>caseId=new set<id>();
        if(taskList.size()>0 && !taskList.isempty()) {
            for(task t:taskList) {
                if(t.whatid!=null) {
                    caseId.add(t.whatid);
                }
            }
            
            map<id,case>caseMap=new map<id,case>([select id,status  from case where id in:caseId]);
           
            if(!caseId.isEmpty() && caseId.size()>0){
                for(task t:taskList){
                     system.debug('SObj1'+t.WhatId.getsObjectType()+'---'+case.getsobjectType());
                    if(caseMap.containsKey(t.whatId) && casemap.get(t.whatId).status=='Closed') {
                        t.adderror('task cannot be created on closed case');
                    }
                }
            }
        }
    }
}