public class disableInputField {
 
    public Opportunity opp {get; set;}
    public Boolean disableInput {get; set;}
 
    public disableInputField(){
        opp = new Opportunity();
    }
 
    public void disableCloseDateInput(){
        disableInput = true;
    }
 
    public void enableCloseDateInput(){
        disableInput = false;
    }
}