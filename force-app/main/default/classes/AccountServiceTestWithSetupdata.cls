@isTest
public class AccountServiceTestWithSetupdata {
    @testSetup static void setupTestData1(){
        List<Account> accounts = new List<Account>();
        for(Integer i=0;i < 10;i++){
            accounts.add(new Account(Name = 'Test Acc'));
        }
        insert accounts;
        List<Opportunity> opportunities = new List<Opportunity>();
        for(Account acc:accounts){
            for(Integer i=0;i < 10;i++){
                opportunities.add(new Opportunity(
                    Name = 'Test Opp', 
                    AccountId = acc.Id,
                    StageName = 'Closed Won',
                    CloseDate = Date.today()
                ));
            }
        }
        insert opportunities;
    }
    
    static testMethod void testGetAllAccounts(){
        Test.startTest();
        List<Account> accountsFromService = AccountService.getAllAccounts();
        Test.stopTest();
        
        System.assertEquals(10, accountsFromService.size(), 'There should be 10 accounts');
    }
    
    static testMethod void testGetOpportunitiesOnAccount(){
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Acc' LIMIT 1];
        
        Test.startTest();
        List<Opportunity> opportunitiesFromService = AccountService.getOpportunitiesOnAccount(acc.Id);
        Test.stopTest();
        
        System.assertEquals(10, opportunitiesFromService.size(), 'There are 10 opportunities');
    }
    
    static testMethod void updateAccountName(){
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Acc' LIMIT 1];
        
        Test.startTest();
        AccountService.updateAccountName(acc, 'Updated Account');
        Test.stopTest();
        
        Account accFromDb = [SELECT Id, Name FROM Account WHERE Id = :acc.Id];
        System.assertEquals('Updated Account', accFromDb.Name, 'The account name has been updated');
    }
}