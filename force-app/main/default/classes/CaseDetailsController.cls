/************************************************************
     This class is used for showing all the case details on page using Controller and Action Methods.
     VF Page:VfExample1
     Date-1/6/2017
 ************************************************************/
public class CaseDetailsController {
   public list<case> caselist{get;set;}
    public list<case> caseDisp{get;set;}
   public Integer casecount{get;set;}
    public Integer casecount1{get;set;}
    public CaseDetailsController(apexpages.StandardController stdcon){
        caselist=[select casenumber,Status,Subject,Type,Priority,ContactPhone,ContactEmail,Reason,Origin from case];
         casecount=caselist.size();
    }
    public void ShowCaseDetails(){
        caseDisp=[select casenumber,Status,Subject,Type,Priority,ContactPhone,ContactEmail,Reason,Origin from case ];
         casecount1=caseDisp.size();
    }

}