public class AccountService {
    public static List<Account> getAllAccounts(){
        return [SELECT Id, Name FROM Account LIMIT 500];
    }
    
    public static List<Opportunity> getOpportunitiesOnAccount(Id accountId){
        return [SELECT Id, Name FROM Opportunity WHERE AccountId = :accountId];
    }
    
    public static void updateAccountName(Account account, String accountName){
        account.Name = accountName;
        update account;
    }
}