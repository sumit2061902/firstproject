public class AccountConRel {
    public static Map<string,integer> accMap = new Map<string,integer>();
    public static map<string,integer> accountmap(list<accountcontactrelation> act) {
        set<id>accountIds = new set<id>();
        for(accountcontactrelation acr:act) {
            accountIds.add(acr.accountid);
        }
        list<sObject>acrList =([select accountid,count(contactid) contct from accountcontactRelation where accountid in:accountIds group by accountid]);
        
        for(sObject ac:acrList) {
            integer count=0;
            count =	integer.valueOf(ac.get('contct'));
            accMap.put(ac.get('accountid')+'',count);
        }
        return accmap;
    }
    public static void updateTypeCheck(List<accountcontactRelation> rel,Map<id,account>accountDetails,Map<id,contact>conDetails) {
        
        for(accountContactRelation acr:rel) {
            account acc=accountDetails.get(acr.accountid);
            contact con=conDetails.get(acr.ContactId);   
            if(acc.Industry =='Energy') {
                acr.addError('Energy Customer cannot be end Dated');
            }
            else if(acc.Customer_Status__c =='IM Sales' && acr.EndDate<=date.today()) {
                acr.EndDate=null;
                acr.Roles='null';
                acr.IsActive=true;
            }
        }  
    }
}