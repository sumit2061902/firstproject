public class AddFunderClass {
    public static void InsertFunder(list<account>acclisttrig){
        Map<id,account> accMap=new Map<id,Account>([select id,(select id from funder__r),(select id from projects1__r ) from account where id in :acclisttrig]);
    List<funder__c>insertFunderList=new List<funder__C>();
  
        for(account acc:acclisttrig){
            if(accmap.containsKey(acc.id)){
                if(accmap.get(acc.id).funder__r.isempty()){
                    funder__c fund=new funder__c(name=acc.name,account__c=acc.id);
                    insertFunderList.add(fund);
                }
            }
                }
        if(!insertFunderList.isEmpty()){
            insert insertFunderList;
        }
    
    }
}