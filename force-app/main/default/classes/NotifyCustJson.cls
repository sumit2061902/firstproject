public class NotifyCustJson {
  //Customer fails
  
     
  //insert senario
 public static String customerWithAddressPositive()
 {
       String json= '{'+
        '"Account":{'+
        '"ABN" : "",'+
        '"CustomerId": "5932100003",'+
        '"CIDN": "",'+
        '"cscrmExternalID": "CNT11234",'+
        '"AccountUUID": "ACUID00003",'+
        '"FirstName": "Thomas",'+
        '"LastName": "James",'+
        '"Phone": "299500003",'+
        '"Fax": "289700003",'+
        '"Email": "thomas@james.com",'+
        '"BirthDate": "1983-09-03",'+
        '"Title": "",'+
        '"Type": "",'+
        '"ActualRetiredDate": "",'+
        '"BusinessUnitCode": "B",'+
        '"BusinessUniqueKey": "",'+
        '"ConcessionAuthority": "",'+
        '"ConcessionNumber": "",'+
        '"ConcessionType": "Pensioner",'+
        '"AustralianDriversLicenseNumber": "BM1003",'+
        '"AustralianDriversLicenseIssueState": "NT",'+
        '"Website": "www.thomasjames.com",'+
        '"LanguagePreference": "",'+
        '"ManagedIndicator": "False",'+
        '"CustomerType": "",'+
        '"MarketSegmentCode": "Independents",'+
        '"PreferredCommunicationMode": "",'+
        '"RetiredUserId": "",'+
        '"RetirementReason": "",'+
        '"ServicingOrganisationalUnit": "RESSTD",'+
        '"PriorityAssist": "False",'+
        '"Createddate": "",'+
        '"SendDateTime": "2016-10-25T19:25:00.356",'+
        '"AccountNumber": "AN65430003",'+
        '"RevenueOrganisationalUnit": "",'+
        '"OnlineFlag": "True",'+
        '"CustomerPotentialRevenue": "",'+
        '"GeographicLocationCode": "03",'+
        '"SourceSystem": "",'+
        '"PaymentType": "",'+
        '"PaymentStatus": "",'+
        '"ValidConcessionFLag": "",'+
        '"LastConcessionValidatedDate": "",'+
        '"BypassConcessionValidation": "",'+
        '"ConcessionValidationResultOverride": "",'+
        '"ConcessionValidationCustomerConsent": "",'+
        '"ConcessionValidationConsentRequestDate": "",'+
        '"Sic": "",'+
        '"AccountAddressRelationship":['+
        '{'+
        '"Status": "",'+
        '"cscrmAddressType": "",'+
        '"cscrmBuildingname": "Chifley Tower",'+
        '"cscrmCountry": "Australia",'+
        '"StreetNumberEndSuffix": "",'+
        '"cscrmLotNumberStart": "",'+
        '"cscrmExternalAddressId": "AADDID0001",'+
        '"cscrmZipPostalCode": "2000",'+
        '"cscrmPrivateStreetName": "",'+
        '"PrivateStreetType": "",'+
        '"cscrmIsPrimary": "",'+
        '"cscrmPropertyName": "Chifley Square",'+
        '"cscrmPlanNumber": "22",'+
        '"StreetNumberStartSuffix": "",'+
        '"cscrmStateProvince": "Sydney",'+
        '"cscrmStreetNumberEnd": "400",'+
        '"cscrmStreetNumberStart": "201",'+
        '"cscrmStreetType": "Avenue",'+
        '"StreetTypeSuffix": "Chifley Street",'+
        '"cscrmBuildingNumber": "2",'+
        '"cscrmBuildingType": "",'+
        '"cscrmLevelNumber": "11",'+
        '"cscrmLevelType": "",'+
        '"cscrmLotNumberEnd": "",'+
        '"cscrmDescriptor": "",'+
        '"cscrmInternationalPostcode": "",'+
        '"cscrmInternationalState": "",'+
        '"cscrmCity": "Chifley",'+
        '"cscrmStreetName": "Hunter Street",'+
        '"AddressLine1": "Street No.11",'+
        '"AddressLine2": "Chifley Street Road",'+
        '"AddressLine3": "Sydney",'+
        '"cscrmDPId": "",'+
        '"cscrmExchange": ""'+
        '}'+
        ']'+
        '}'+
        '}';
    
     return json; 
 }
    
  public static String customerAddressWithFail()
  {
     String json=        '{'+
        '"Account":{'+
        '"ABN" : "",'+
        '"CustomerId": "5932100003",'+
        '"CIDN": "",'+
        '"cscrmExternalID": "",'+
        '"AccountUUID": "ACUID00003",'+
        '"FirstName": "",'+
        '"LastName": "",'+
        '"Phone": "299500003",'+
        '"Fax": "289700003",'+
        '"Email": "thomas@james.com",'+
        '"BirthDate": "1983-09-03",'+
        '"Title": "Sir",'+
        '"Type": "",'+
        '"ActualRetiredDate": "",'+
        '"BusinessUnitCode": "B",'+
        '"BusinessUniqueKey": "",'+
        '"ConcessionAuthority": "",'+
        '"ConcessionNumber": "",'+
        '"ConcessionType": "Pensioner",'+
        '"AustralianDriversLicenseNumber": "BM1003",'+
        '"AustralianDriversLicenseIssueState": "NT",'+
        '"Website": "www.thomasjames.com",'+
        '"LanguagePreference": "",'+
        '"ManagedIndicator": "False",'+
        '"CustomerType": "",'+
        '"MarketSegmentCode": "Independents",'+
        '"PreferredCommunicationMode": "",'+
        '"RetiredUserId": "",'+
        '"RetirementReason": "",'+
        '"ServicingOrganisationalUnit": "RESSTD",'+
        '"PriorityAssist": "False",'+
        '"Createddate": "",'+
        '"SendDateTime": "2016-10-28T19:25:00.356",'+
        '"AccountNumber": "AN65430003",'+
        '"RevenueOrganisationalUnit": "",'+
        '"OnlineFlag": "True",'+
        '"CustomerPotentialRevenue": "",'+
        '"GeographicLocationCode": "03",'+
        '"SourceSystem": "",'+
        '"PaymentType": "",'+
        '"PaymentStatus": "",'+
        '"ValidConcessionFLag": "",'+
        '"LastConcessionValidatedDate": "",'+
        '"BypassConcessionValidation": "",'+
        '"ConcessionValidationResultOverride": "",'+
        '"ConcessionValidationCustomerConsent": "",'+
        '"ConcessionValidationConsentRequestDate": "",'+
        '"Sic": "",'+
        '"AccountAddressRelationship":['+
        '{'+
        '"Status": "",'+
        '"cscrmAddressType": "Street",'+
        '"cscrmBuildingname": "Chifley Tower",'+
        '"cscrmCountry": "Australia",'+
        '"StreetNumberEndSuffix": "",'+
        '"cscrmLotNumberStart": "",'+
        '"cscrmExternalAddressId": "AADDID0001",'+
        '"cscrmZipPostalCode": "2000",'+
        '"cscrmPrivateStreetName": "",'+
        '"PrivateStreetType": "",'+
        '"cscrmIsPrimary": "",'+
        '"cscrmPropertyName": "Chifley Square",'+
        '"cscrmPlanNumber": "22",'+
        '"StreetNumberStartSuffix": "",'+
        '"cscrmStateProvince": "Sydney",'+
        '"cscrmStreetNumberEnd": "400",'+
        '"cscrmStreetNumberStart": "201",'+
        '"cscrmStreetType": "Avenue",'+
        '"StreetTypeSuffix": "Chifley Street",'+
        '"cscrmBuildingNumber": "2",'+
        '"cscrmBuildingType": "Building",'+
        '"cscrmLevelNumber": "11",'+
        '"cscrmLevelType": "Floor",'+
        '"cscrmLotNumberEnd": "",'+
        '"cscrmDescriptor": "",'+
        '"cscrmInternationalPostcode": "",'+
        '"cscrmInternationalState": "",'+
        '"cscrmCity": "Chifley",'+
        '"cscrmStreetName": "Hunter Street",'+
        '"AddressLine1": "Street No.11",'+
        '"AddressLine2": "Chifley Street Road",'+
        '"AddressLine3": "Sydney",'+
        '"cscrmDPId": "",'+
        '"cscrmExchange": ""'+
        '}'+
        ']'+
        '}'+
        '}';
    
      return json;
  }
  
}