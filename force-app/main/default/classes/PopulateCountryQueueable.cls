public class PopulateCountryQueueable implements Queueable,Database.AllowsCallouts{
    public List<Contact> contacts; 
    public PopulateCountryQueueable(List<Contact> conList){
        contacts = conList ;
        
    }
    public void execute(QueueableContext qc){
        System.debug('contacts'+contacts[0]);
        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setMethod('GET');
        httpReq.setHeader('Content-Type','application/json'); 
        httpReq.setEndpoint('http://countryapi.gear.host/v1/Country/getCountries');
        HttpResponse res = h.send(httpReq);
        System.debug('res--->'+res);
        JsonApex jsonP = (JsonApex)JSON.deserialize(res.getBody(),JsonApex.class);
        List<cls_Response> countries = jsonP.Response;
        system.debug('countries--->'+countries);
        for(Contact con :contacts ){
            for(cls_Response cntry : countries){
                if(cntry.Alpha3Code ==con.MailingCountry){
                    con.CurrencyCode__c = cntry.CurrencyCode ; 
                }
                
            }
            
        }
        system.debug('contacts---->'+contacts);
        update contacts;
        
    }
    
    public class JsonApex{
        public boolean IsSuccess{get;set;}
        public cls_UserMessage UserMessage{get;set;}
        public cls_TechnicalMessage TechnicalMessage{get;set;}
        public Integer TotalCount{get;set;}
        public cls_Response[] Response{get;set;}
    }
    public class cls_UserMessage {
    }
    class cls_TechnicalMessage {
    }
    public class cls_Response {
        public String Name {get;set;}
        public String Alpha2Code{get;set;}
        public String Alpha3Code{get;set;}
        public String NativeName{get;set;}
        public String Region{get;set;}
        public String SubRegion{get;set;}
        public String Latitude{get;set;}
        public String Longitude{get;set;}
        public Integer Area{get;set;}
        public Integer NumericCode{get;set;}
        public String NativeLanguage{get;set;}
        public String CurrencyCode{get;set;}
        public String CurrencyName{get;set;}
        public String CurrencySymbol{get;set;}
        public String Flag{get;set;}
        public String FlagPng{get;set;}
    }  
}