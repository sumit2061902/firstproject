/*--------------------------------------------------
Class Name: InsertCaseHelper
CreateDate: 03/05/2018
Description:created to insert a case in salesforce
Version: 1.0
---------------------------------------------------*/

@RestResource(urlMapping='/v1/cases/*')
global with sharing class InsertCaseHelper {
    global static string  responsePayLoad;
    
    @HTTPPOST  
    global static void insertCase(){
        RestRequest req;
        RestResponse res;
        String jsonPayLoad;  
        try{ 
            
            req = RestContext.request;
            res = RestContext.response;
            jsonPayLoad=req.requestBody.toString();
            responsePayLoad=jsonPayLoad;
            CaseJsonParser obj=CaseJsonParser.parse(jsonPayLoad);
            
           //check json is blank 

                     
                        
                        case  Newcase=new case();                
                        System.debug('inside IF::');
                        Newcase.status=obj.cse.Status;
                        Newcase.Origin=obj.cse.Origin;
                        Newcase.For_a_Business_or_Residential_Customer__c=obj.cse.customerType;
                        Newcase.Reason_for_Completing_this_AIM__c=obj.cse.reasonForCompletingAIM;
                        Newcase.Service_Id__c=obj.cse.serviceId;

                                Insert Newcase; 
                }
        catch(Exception e){    
    }      
    }
    
    
    public class CaseJsonParser {
    
    public  cls_case cse;
    }
    public class cls_case {
        public string status;
        public string Origin;
        public string recTypeName;
        public string customerType;
        public string reasonForCompletingAIM;
        public string serviceId;
        public string enquiryType;
        public boolean nbnService;
        public boolean paCustomer;
        public string emailAddress;
        public boolean existingTelstraCustomer;
        public boolean callbackOnWeekned;
        public string companyType;
        public string changeModifyType;
        public string contactNumber;
        
    }
    
    Public static CaseJsonParser parse(string json){
        return (CaseJsonParser) System.JSON.deserialize(json, CaseJsonParser.class); 
    }
}