@RestResource(urlMapping='/GetSalesOrder/*')
global without sharing class GetSalesOrder {
    private class cls_SalesOrder {
        public string customerOrderReference;
        public string createdDate;
        public string status;
        public string accountUUID;
        public string billingAccountUUID;
        public string id;
        public double totalOneOffCharge;
        public double totalRecurringCharge;
        public string activationDate;
        
        List<cls_Services> items;
        cls_SalesOrder(csord_Subscription__c ord) {
            id = ord.CS_Order__r.Id;
            status = ord.CS_Order__r.csord_Status__c;
            customerOrderReference = ord.CS_Order__r.csord_Customer_Order_Number__c;
            accountUUID = ord.Account__r.AccountUUID__c;
            billingAccountUUID = ord.Billing_Account_UUID__c;
            createdDate = String.valueOf(ord.CS_Order__r.CreatedDate);
            activationDate = String.valueOf(ord.Activation_Date__c);
            totalOneOffCharge = ord.csord_Total_One_Off_Charges__c;
            totalRecurringCharge = ord.csord_Total_Recurring_Charges__c;
            
            items = new List<cls_Services>();
            for(csord_Service__c svc:ord.csord_Services__r) {
                items.add(new cls_Services(svc, ord));
            }
            System.debug('items>>>>'+items);
        }
    }
    
    private class cls_Services {
        public string name;
        public string id;
        public string status;
        public string activationDate;
        public string serviceIdentifier;
        public string addressId;
        public string nbnAppointmentId;
        public string parentItemId;
        public double oneOffCharge;
        public double recurringCharge;
        public string productType;
        public string productSubType;
        public string uuid;
        public string parentUUID;
        public string cancellationInitiatedData;
        public string cancellationCompletionData;
        public string cancellationReason;
        
        cls_Services(csord_Service__c svc, csord_Subscription__c ord) {
            name = svc.name;
            status = svc.csord_Status__c;
           // activationDate = String.valueOf(svc.csord_Activation_Date__c);
			serviceIdentifier = svc.ServiceIdentifer__c;
            addressId = svc.csord_Service_Address__c;
            nbnAppointmentId = svc.Appointment_ID__c;
            parentItemId = svc.csord_Service__c;
            id = svc.Id;
            productType = svc.ProductType__c;
         //   productSubType = svc.ProductSubType__c;
            oneOffCharge = svc.csord_Total_One_Off_Charges__c;
            recurringCharge = svc.csord_Total_Recurring_Charges__c;
            parentUUID = svc.Parent_Service_UUID__c;
            uuid = svc.UUID__c;
            cancellationInitiatedData = String.valueOf(ord.Cancellation_Date__c);
            cancellationCompletionData = String.valueOf(ord.Cancellation_Completion_date__c);
            cancellationReason = ord.Cancellation_Reason__c;
        }
    }
    
    private class cls_response {
        public string code;
        public string message;
        cls_response(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }
    
    
    @HttpGet
    global static void GetSalesOrder()
    {
        String orderNumber = RestContext.request.params.get('orderNumber');
        System.debug(orderNumber);
        RestContext.response.addHeader('Content-Type', 'application/json');
       // csord_Order__c ord = [SELECT Id, csord_Status2__c, csord_Account__r.AccountUUID__c from csord_Order__c where Id = 'a1Y0l0000008qZO'];
        csord_Subscription__c[] ords = 
            [SELECT 
             Account__r.AccountUUID__c, 
             CS_Order__r.Id, 
             CS_Order__r.csord_Status__c,
             CS_Order__r.CreatedDate,
             CS_Order__r.csord_Customer_Order_Number__c,
             Cancellation_Completion_date__c,
             Cancellation_Date__c,
             Cancellation_Reason__c,
             Activation_Date__c,
             csord_Total_One_Off_Charges__c,
             csord_Total_Recurring_Charges__c,
             Billing_Account_UUID__c,
             (SELECT 
              Id,
              name,
              ServiceIdentifer__c,
              csord_Service__c,
              csord_Status__c,
              csord_Service_Address__c,
              Appointment_ID__c,
              csord_Total_One_Off_Charges__c,
              csord_Total_Recurring_Charges__c,
              ProductType__c,
              Parent_Service_UUID__c,
              UUID__c
              from csord_Services__r
             )
             from csord_Subscription__c where CS_Order__r.csord_Customer_Order_Number__c = :orderNumber LIMIT 1];
        if(ords.size() > 0) {
            System.debug('Inside Ords if>>>>');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new cls_SalesOrder(ords[0])));
            RestContext.response.statusCode = 200;   
        } else {
            System.debug('Inside Ords Else>>>>');
            RestContext.response.statusCode = 404;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new cls_response('OrderNotFound', 'Order Not found')));
        }
    }
}