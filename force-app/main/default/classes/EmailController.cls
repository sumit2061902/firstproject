public class EmailController {
     String baseId {get;set;}
    list<Candidate__c> emailcheck{get;set;}
    public Boolean rend{get;set;}
       public EmailController(ApexPages.StandardController stdcon){
        sendingEmail();              
    }
    
    public void  sendingEmail(){
        
        
   		 baseID = ApexPages.currentPage().getParameters().get('id');
        system.debug('baseID---'+baseID);
           emailcheck=[select SenEmail__c from Candidate__c where id=:baseid and SenEmail__c=true];  
        system.debug('emailcheck---'+emailcheck);
        if(emailcheck.size()>0)
        {
        messaging.SingleEmailMessage seemail=new messaging.SingleEmailMessage();
       String[] sendingTo = new String[]{'sumit.rajaram.bhagat@accenture.com'};

         seemail.setToAddresses(sendingTo);  
        seemail.setSubject('Test Email Setup');
        seemail.setPlainTextBody('Hello this is the test email');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {seemail});
            
            rend=true;
        }
        else{
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No need to send mail'));
			rend=false;
        }
    }
        

}