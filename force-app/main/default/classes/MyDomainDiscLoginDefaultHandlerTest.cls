@isTest
public class MyDomainDiscLoginDefaultHandlerTest {
    @testSetup 
    public static void setup() { 
        try {
            User sysAdminUser = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                LastName = 'last',
                Email = 'testUser2FA@test.com',
                Username = 'testUser2FA@test.com',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'tuser',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US'
            );
            insert sysAdminUser;
            
            User restrictedUser = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Restricted'].Id,
                LastName = 'last',
                Email = 'testUser2FA@test.com',
                Username = 'TelstraCustomPerm@test.com',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'Permtest',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                FederationIdentifier='R00100' 
            );
            insert restrictedUser;   
            
            // Create PermissionSet with Custom Permission and assign to test user
            PermissionSet ps = new PermissionSet();
            ps.Name = 'Test';
            ps.Label = 'Test';
            insert ps;
            
            SetupEntityAccess sea = new SetupEntityAccess();
            sea.ParentId = ps.Id;
            sea.SetupEntityId = [select Id from CustomPermission where DeveloperName = 'Telstra_Administrator'][0].Id;
            insert sea;
            
            System.debug('sea>>>>'+sea);
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.AssigneeId = restrictedUser.id;
            psa.PermissionSetId = ps.Id;
            insert psa;
            
            User usr = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'CDM Admin'].Id,
                LastName = 'last',
                Email = 'testUser2FA@test.com',
                Username = 'nocustompermission@test.com',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                FederationIdentifier='C00100' 
            );
            insert usr; 
            
            User partnerUser = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Partner Stores'].Id,
                LastName = 'last',
                Email = 'testUser2FA@test.com',
                Username = 'partnerProfile@test.com',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                FederationIdentifier='P00100' 
            );
            insert partnerUser; 
        }
        catch(Exception e) {
        }      
    }
    
    public static testmethod void CustomPermValidation(){
        
        try {  
            user u=[select id,FederationIdentifier,Username from user where username='TelstraCustomPerm@test.com'];
            Map<String, String> requestAttributes = new Map<String, String>();
            String startUrl = '';
            MyDomainDiscLoginDefaultHandler sa=new MyDomainDiscLoginDefaultHandler();
            PageReference  pageReference =  sa.login(u.FederationIdentifier,startUrl,requestAttributes);
            System.assertNotEquals(null, pageReference, 'Page reference was not returned');
        }
        catch(Exception e){
        }
    }   
    
    public static testmethod void noCustomPermValidation(){
        
        try {
            user u=[select id,FederationIdentifier from user where username='nocustompermission@test.com' limit 1];
            Map<String, String> requestAttributes = new Map<String, String>();
            String startUrl = '';
            MyDomainDiscLoginDefaultHandler sa=new MyDomainDiscLoginDefaultHandler();
            PageReference  pageReference =  sa.login(u.FederationIdentifier,startUrl,requestAttributes);
            System.assertNotEquals(null, pageReference, 'Page reference was not returned');
        }
        catch(Exception e){
        }
    }
    
    public static testmethod void partnerUserValidation(){
        
        try {
            user u=[select id,profile.name,FederationIdentifier from user where username='partnerProfile@test.com' limit 1];
            Map<String, String> requestAttributes = new Map<String, String>();
            system.debug('upro---->'+u.profile.name);
            String startUrl = '';
            MyDomainDiscLoginDefaultHandler sa=new MyDomainDiscLoginDefaultHandler();
            PageReference  pageReference =  sa.login(u.FederationIdentifier,startUrl,requestAttributes);
            System.assertNotEquals(null, pageReference, 'Page reference was not returned');
        }
        catch(Exception e){
        }
    }
    /*public static testmethod void testcaal(){
user u=[select id,FederationIdentifier,Username from user where username='testUser2FA@test.com'];
test.startTest();
Map<String, String> requestAttributes = new Map<String, String>();
String startUrl = '';
MyDomainDiscLoginDefaultHandler sa=new MyDomainDiscLoginDefaultHandler();
PageReference  pageReference = sa.login(u.username,startUrl,requestAttributes);

}*/
    @isTest static void testLoginWithInvalidUser() {
        try {
            Map<String, String> requestAttributes = new Map<String, String>();
            String startUrl = '';
            String fedId = 'AETERYD';
            MyDomainDiscLoginDefaultHandler myDomainDiscLoginDefaultHandler = new MyDomainDiscLoginDefaultHandler();
            // Invoke login method from handler with non-existing user
            myDomainDiscLoginDefaultHandler.login(fedId, startUrl, requestAttributes);
        }catch (Auth.LoginDiscoveryException loginDiscoveryException) {
            // Assert exception message
            System.assert(loginDiscoveryException.getMessage().contains('No unique user found'), 'message=' + loginDiscoveryException.getMessage());
        }
    }
}