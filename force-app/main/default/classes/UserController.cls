public class UserController {
	public static map<string,string> getUserStatus ( string accId) {
		
		String openCaseComplaints;
		Account acc=[select id,rating,type,owner.name,(select id from cases where status='open') from account where id =:accId limit 1];
		if(acc.cases.size()>0) {
			openCaseComplaints='Yes';
		}
		else {
			openCaseComplaints='No';
		}
		Map<string,string>statusMap=new map<string,string>();
		statusMap.put('Account Type',acc.type);
		statusMap.put('Account OwnerName',acc.owner.name);
		statusMap.put('Account Rating',acc.rating);
        statusMap.put('open Case Complaints',openCaseComplaints);
        
        statusMap.put(acc.owner.name,acc.rating);
        statusMap.put(acc.owner.name,openCaseComplaints);
		System.debug('Status Map------>'+statusMap);
         
		return statusMap;
	}
}