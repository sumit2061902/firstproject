/****************************************
 Test class:
description

 ****************************************/
@istest
public class Test_GetSomeTrail {
    
    public static testmethod void checkGetSomeTrail(){
        list <account> acclist=new list<account>();
        list<id>ids=new list<id>();
        
        for(integer i=1;i<=10;i++) {
            account acc=new account(name='test'+i,rating='warm', Active__c='Yes', SLA__c='gold');
            acclist.add(acc);
        }
        insert acclist;
        
        list<account>aclist=[select id from account where id in :acclist];
        System.debug('aclist::::'+aclist);
        
        for(account ac:aclist){
            ids.add(ac.id);
        }
        System.debug('ids::::'+ids);
        for(integer i=1;i<10;i++){
            system.debug('issaa:::'+ids[i]);
            
            GetSomeTrail.Getparams(ids[i],'hot');
        }
        list<account> ac=[select id,rating from account where rating='hot'];
        for(account a:ac){
            system.assertEquals(ac[0].Rating, 'Hot');
        }
    }
}