public class AccountManagerPopulate {
    public static void AccManagerPop(list<account>acclist,Map<id,Account>oldAccountMap){
        set<id>ownerids=new set<id>();
        for(account acc:acclist){
            ownerids.add(acc.ownerid);
        }
        
        Map<id,user>Usermap=new Map<id,User>([select id ,firstname,lastname from user where id in :ownerids]);
        
        for(account acc:acclist){
            if(oldAccountMap==null || oldAccountMap.get(acc.id).ownerid!=acc.OwnerId){
                acc.Accounts_Manager__c=acc.OwnerId;
            }
            
            if(usermap.containsKey(acc.OwnerId)){
                acc.description=usermap.get(acc.OwnerId).firstname;
            }
        }
    }
}