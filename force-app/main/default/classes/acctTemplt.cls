@RestResource(urlMapping='/getAcc/*')
global without sharing class acctTemplt
{
    @HTTPPOST
    global static void getData(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        NotifyCust obj = NotifyCust.parse(req.requestBody.toString());
        AccountData data = obj.data;
        system.debug('obj>>>>'+obj);
        system.debug('data>>>>'+data);
    }
    public class NotifyCust {
        public AccountData data;    
    }
    public class AccountData {
        public String eventAction;
        public String TCMSendDateTime;
        public String abn;
        public String customerAccountId;
        public String accountUuid;
        public String accountStatus;
        public String acn;
        public String actualRetiredDate;
        public String businessUnit;
    }
public static NotifyCust parse(String json){
    return (NotifyCust) System.JSON.deserialize(json, NotifyCust.class);
}
}