/***********************************************
@Author: Yasdeep Sana
@name: DelegatedAuthServiceTest
@CreateDate:15/Nov/2018
@Description: Class to implement Delegated Authentication
@Version: 1.0
************************************************/
@RestResource(urlMapping='/delegatedauth/*')
global with sharing class DelegatedAuthService {
    
    static final Blob AUTH_TRUE = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><AuthenticateResponse xmlns="urn:authentication.soap.sforce.com"><Authenticated>true</Authenticated></AuthenticateResponse></soapenv:Body></soapenv:Envelope>');
    static final Blob AUTH_FALSE = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><AuthenticateResponse xmlns="urn:authentication.soap.sforce.com"><Authenticated>false</Authenticated></AuthenticateResponse></soapenv:Body></soapenv:Envelope>');
    static String IDENTITY_PROVIDER;
    static final String OrgUrl=system.URL.getSalesforceBaseUrl().toExternalForm();
    
    @HttpPost
    global static void doPost() {
        
        if(OrgUrl.contains('SIT01'))
            IDENTITY_PROVIDER='https://sa.auth.thyra.telstra.com/SecureAuth265/';
        else if(OrgUrl.contains(System.Label.OrgUrl))
            IDENTITY_PROVIDER = System.Label.IdentityProviderURL;	  	
        else if (OrgUrl.contains('PREPROD'))
            IDENTITY_PROVIDER='https://sa.auth.thyra.telstra.com/SecureAuth287/';
        else
            IDENTITY_PROVIDER='https://sa.auth.thyra.telstra.com/SecureAuth227/';
        Blob responseBody = AUTH_FALSE;
        
        RestRequest req = RestContext.request;
        Dom.Document doc = new Dom.Document();
        doc.load(req.requestBody.toString());
        Dom.XMLNode authRequest = doc.getRootElement();
        
        Dom.XMLNode body = authRequest.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
        Dom.XMLNode authenticate = body.getChildElement('Authenticate', 'urn:authentication.soap.sforce.com');
        String username = authenticate.getChildElement('username', 'urn:authentication.soap.sforce.com').getText();
        String password = authenticate.getChildElement('password', 'urn:authentication.soap.sforce.com').getText();
        String sourceIp = authenticate.getChildElement('sourceIp', 'urn:authentication.soap.sforce.com').getText();
        
        Http h = new Http();
        HttpRequest hr = new HttpRequest();
        hr.setEndpoint(IDENTITY_PROVIDER);
        hr.setHeader('X-OpenIDM-Username',username);
        hr.setHeader('X-OpenIDM-Password',password);
        hr.setMethod('GET');
        HttpResponse hrs = h.send(hr);
        if (hrs.getStatusCode() == 200)  {
            responseBody = AUTH_TRUE;
        } 
        
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/soap+xml');
        res.statusCode = 200;
        res.responseBody = responseBody;
        
    }
    
    
}