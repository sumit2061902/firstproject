@isTest
public class Test_Util {
    public static User sysAdministrator(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser1CS@email.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1CS@email.com');
        return u;
    }
    
    public static User salesRep(){
        Profile p = [SELECT Id FROM Profile WHERE Name='BC Sales User']; 
        User u = new User(Alias = 'standt', Email='standarduser1CS@email.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1CS@email.com');
        return u;
    }
    
    public static User salesManagement(){
        Profile p = [SELECT Id FROM Profile WHERE Name='BC Sales Management']; 
        User u = new User(Alias = 'standt', Email='standarduser1CS@email.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1CS@email.com');
        return u;
    }
    
    
    public static List<Account> createAccounts(Integer count){
        List<Account> actList = new List<Account>();
        for(Integer i =0; i<count; i++){
            Account acc1 = new Account();
            acc1.name = 'test';
            acc1.BillingCity = 'Test City';
           
            acc1.BillingCountry='US';
           
                acc1.Active__C ='No';
                acc1.SLASerialNumber__c = '1231';
                acc1.SLAExpirationDate__c =system.today()-2;
                acc1.SLA__c = 'Gold';
       
            actList.add(acc1);
        }
        return actList;
    }
}