@IsTest
public class PLR_UtilTest {
    	@isTest
	static void getSessionIdTest() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new PLR_UtilMock(200));
		String sessionId = PLR_Util.getSessionId();
		System.assertEquals(sessionId, 'sfdcSessionId', 'Error: Unexpected SessionId value');
		Test.stopTest();
	}
    	@isTest
	static void getSessionIdTestFail() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new PLR_UtilMock(401));
		String sessionId = PLR_Util.getSessionId();
		//System.assertEquals(sessionId, 'sfdcSessionId', 'Error: Unexpected SessionId value');
		Test.stopTest();
	}
 	@isTest
	static void getSessionIdTestFail2() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new PLR_UtilMock());
		String sessionId = PLR_Util.getSessionId();
		//System.assertEquals(sessionId, 'sfdcSessionId', 'Error: Unexpected SessionId value');
		Test.stopTest();
	}


}