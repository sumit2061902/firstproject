public class EventRequestJSON_EOMSEventFailure {

    public String correlationId; //test1234
    public String timestamp; //2017-07-10 04:09:30
    public cls_Data data;
    public List<cls_Errors> errors;
        
    public class cls_Data {
        public String customerOrderReference; //039872342398   
        public cls_Data(String customerOrderReference) {
            this.customerOrderReference = customerOrderReference;
        }
    }
    
    public class cls_Errors {
        public String code; //Fred
        public String message;  //Bloggs
            
        public cls_Errors(string code, string message)
        {            
            this.code = code;
            this.message = message;            
        }
    }    
    
    public string createrequestjson_EOMSEventFailure(String correlationId, String timestamp, String customerOrderReference, String errorCode, String errorMessage)
    {
        EventRequestJSON_EOMSEventFailure eventreq = new EventRequestJSON_EOMSEventFailure();
        
        List<cls_Errors> errorCodeMessage = new List<cls_Errors>();

        cls_Errors clsrerror = new cls_Errors(errorCode, errorMessage);
        errorCodeMessage.add(clsrerror);
        
        system.debug('Vishal Test>>>'+errorCodeMessage);
                
        eventreq.correlationId=correlationId;
        eventreq.timestamp=timestamp;  
        eventreq.data=new EventRequestJSON_EOMSEventFailure.cls_Data(customerOrderReference);        
        eventreq.errors = errorCodeMessage;
        return Json.serialize(eventreq);
    }
}