public class CreateServiceProduct {
    
    public static void CreateServiceProductOpp(list<OpportunityLineItem> OpplineItems){
        Id priceBookId;
         boolean bMatchFound;
        System.debug('OpplineItems>><><><>'+OpplineItems);
        list<OpportunityLineItem> newOppLineitems = new list<OpportunityLineItem>();
        priceBookId=[select ID,pricebook2id from opportunity where id=:OpplineItems[0].opportunityid][0].pricebook2ID;
        System.debug('priceBookId>>>>>>'+priceBookId);
        set<string> productName=new set<String>();
        set<ID>productID=new set<ID>();
        Map<id,id>oppMap=new Map<Id,Id>();
        for(OpportunityLineItem opp:OpplineItems){
            productName.add(opp.SLA__c);
            System.debug('productName>>>'+productName);
            oppMap.put(opp.opportunityId,opp.opportunityId); 
        }
          System.debug('oppMap>>>>>>'+oppMap);
       	list<product2> prdList=[select id,Name from product2 where name=:ProductName];
          System.debug('prdList>>>>>>'+prdList);
        for(product2 prd2:prdList){
            productID.add(prd2.id);
            System.debug('productID>>>>'+productID);
        }
         List<PricebookEntry> pbe1 = [SELECT Id, PriceBookEntry.product2Id, Pricebook2Id, PriceBookEntry.product2.name, PriceBookEntry.unitprice, ProductCode
                                     FROM PriceBookEntry 
                                     WHERE PriceBookEntry.product2Id ='01t280000036tBUAAY'];
        System.debug('pbe1>>>>>>'+pbe1);
   Map<String,pricebookEntry>pbeMap=new map<String,pricebookEntry>();
    
        for(pricebookEntry pbe:pbe1){
            pbeMap.put(pbe.product2.name,pbe);
        }
        System.debug('pbeMap>>>>>>'+pbeMap);
        for(opportunitylineitem opp:OpplineItems){
            bMatchFound = false;
            if(bMatchFound==false){
                System.debug('INSIDE FOR>>>>>>>>>>>>>'+opp.SLA__c);
                bMatchFound=true;
                OpportunityLineItem temp = new OpportunityLineItem ();
                temp.OpportunityId=oppMap.get(opp.OpportunityId);
                temp.PricebookEntryId=pbemap.get(opp.SLA__c).id;
                 temp.quantity=opp.Quantity; 
                    temp.SLA__c = ''; 
                temp.TotalPrice=2000;
                newOppLineitems.add(temp);
                 System.debug('newOppLineitems>>>>>>'+newOppLineitems);
            }
             if(bMatchFound==false)
            {
                opp.addError('No Service Part Found for this SLA in the selected Pricebook.Contact system administrator or Modify the SLA');
            }     
        }
        if(newOppLineitems.size() >0){
                insert newOppLineitems;
                system.debug('*** number of new lines added' + newOppLineitems.size());
            } 
    }

}