import { LightningElement,track } from 'lwc';

export default class HelloExpression extends LightningElement {

@track fullname = {firstname:'',lastname:'' };
    handleChange(event) {
        const field = event.target.name;
        console.log('field=='+field);
        if(field === 'FirstName') {
            this.fullname.firstname = event.target.value;
        }
        if(field === 'LastName') {
            this.fullname.lastname = event.target.value;
        }
    }

    get upperCaseFullName() {
        return `${this.fullname.firstname} ${this.fullname.lastname}`.trim().toUpperCase();
    }
}