import { LightningElement ,wire } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import STATUS_FIELD from '@salesforce/schema/Account.Industry';
import APPLIESTO_FIELD from '@salesforce/schema/Account.CustomerType__c';
import { fireEvent } from '../pubsub/pubsub';
import {currentPageReference} from 'lightning/navigation';
export default class IncidentSearch extends LightningElement {
    statusValue="";
    appliesValue="";
    incNum="";
    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: STATUS_FIELD })
    statusValues;
    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: APPLIESTO_FIELD })
    appliesToValues;
 
    handleIncNum(event){
        this.incNum=event.target.value;

    }
    handleChange(event) {
        if(event.target.name === status) {
            this.statusValue = event.target.value;
        }
        if(event.target.name === appliesTo) {
            this.appliesValue = event.target.value;
        }
    }
    handleClick(event) {
        let eventData = {"incNo" : this.incNum,
                        "statusValue" : this.statusValue,
                        "appliesValue" :this.appliesValue
                        };
        fireEvent(this.pageRef,'searchKey',eventData);
    }
}