import { LightningElement } from 'lwc';
import { registerListener } from '../pubsub/pubsub';

export default class SearchResult extends LightningElement {
    incvalue="";
    statusValue="";
    appliesValue="";
    connectedCallback(){
        registerListener('searchKey',this.handleSearchKeyChange,this);
    }
    dissconnectedCallback(){

    }
    handleSearchKeyChange(searchKey) {
this.incvalue = searchKey.incNo;
this.statusValue = searchKey.statusValue;
this.appliesValue = searchKey.appliesValue;
    }
}